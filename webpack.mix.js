let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.autoload({
   jquery: ['$', 'window.jQuery', "jQuery", "window.$", "jquery", "window.jquery"]
}).js([
   'resources/assets/js/app.js',
], 'public/js').extract([
   'vue',
   'jquery',
   'datatables.net',
   'datatables.net-bs4',
   'datatables.net-responsive-bs4'
]);

mix.styles([
    'node_modules/bootstrap/dist/css/bootstrap.min.css',
    'node_modules/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css',
    'node_modules/@fortawesome/fontawesome-free/css/all.css',
    'node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css',
    'node_modules/datatables.net-fixedcolumns-bs4/css/fixedColumns.bootstrap4.min.css',
    'node_modules/select2/dist/css/select2.min.css',
    'node_modules/selectize/dist/css/selectize.css',
    'node_modules/select2-bootstrap-theme/dist/css/select2-bootstrap.min.css',
    'node_modules/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css',
    'node_modules/bootstrap-year-calendar/css/bootstrap-year-calendar.min.css',
    'resources/assets/css/dashboard.css'
], 'public/css/app.css');

mix.copy([
   'node_modules/@fortawesome/fontawesome-free/webfonts',
], 'public/webfonts');

mix.copy([
   'node_modules/feather-icons/dist/icons',
], 'public/js/icons');