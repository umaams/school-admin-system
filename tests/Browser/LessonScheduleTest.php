<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Models\Master\User;

class LessonScheduleTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testView()
    {
        $this->browse(function ($first) {
            $first->loginAs(User::find(1))
            ->visit('/lesson_schedules/1/schedules')
            ->assertRouteIs('lesson_schedules.show', ['id' => 1]);
        });
    }

    public function testCreate()
    {
        $this->browse(function ($first) {
            $first->loginAs(User::find(1))
            ->visit('/lesson_schedules/1/schedules/create')
            ->select('day_week_id')
            ->select('school_time_id')
            ->select('lesson_id')
            ->select('teacher_id')
            ->click('@btn-store')
            ->assertRouteIs('lesson_schedules.show', ['id' => 1]);
        });
    }

    public function testUpdate()
    {
        $this->browse(function ($first) {
            $first->loginAs(User::find(1))
            ->visit('/lesson_schedules/1')
            ->select('day_week_id')
            ->select('school_time_id')
            ->select('lesson_id')
            ->select('teacher_id')
            ->click('@btn-update')
            ->assertRouteIs('lesson_schedules.show', ['id' => 1]);
        });
    }
}
