<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Models\Master\User;

class CalendarTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testView()
    {
        $this->browse(function ($first) {
            $first->loginAs(User::find(1))
            ->visit('/calendar_academic')
            ->assertRouteIs('calendar.index');
        });
    }

    public function testUpdate()
    {
        $this->browse(function ($first) {
            $first->maximize()->loginAs(User::find(1))
            ->visit('/calendar_academic')
            ->assertRouteIs('calendar.index')
            ->visit('/calendar_academic/setting')
            ->assertRouteIs('calendar.edit')
            ->keys('input[name=semester_1_date_start]', '2018-06-16')
            ->keys('input[name=semester_1_date_end]', '2018-12-31')
            ->keys('input[name=semester_2_date_start]', '2019-01-01')
            ->keys('input[name=semester_2_date_end]', '2019-06-30')
            ->click('@btn-calendar-update')
            ->assertRouteIs('calendar.index');
        });
    }
}
