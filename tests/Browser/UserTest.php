<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Models\Master\User;

class UserTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testView()
    {
        $this->browse(function ($first) {
            $first->loginAs(User::find(1))
            ->visit('/users')
            ->assertRouteIs('users.index');
        });
    }

    public function testCreate()
    {
        $this->browse(function ($first) {
            $first->loginAs(User::find(1))
            ->visit('/users/create')
            ->assertRouteIs('users.create');
        });
    }
}
