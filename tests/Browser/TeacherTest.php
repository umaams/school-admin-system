<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Models\Master\User;

class TeacherTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testCreate()
    {
        $this->browse(function ($first) {
            $first->loginAs(User::find(1))
            ->visit('/teachers/create')
            ->assertRouteIs('teachers.create')
            ->type('nuptk', '12345')
            ->select('employee_id')
            ->type('code', 'KUM')
            ->click('@btn-store')
            ->assertRouteIs('teachers.index');
        });
    }
}
