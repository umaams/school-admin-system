@extends('templates.data.tabler.app')

@section('title')
Kurikulum Detail
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('curriculum detail', $curriculum) }}
@endsection

@section('content')
<div class="row" id="baseapp">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<ul class="nav nav-tabs card-header-tabs" role="tablist">
					@foreach($grades as $i => $grade)
					<li class="nav-item"><a class="nav-link @if ($i == 0 && !session('grade_id')) active  @endif @if (session('grade_id') == $grade->id) active @endif" data-toggle="tab" href="#tab-{{ $grade->id }}" aria-expanded="@if ($i == 0 && !session('grade_id')) true @else false @endif @if (session('grade_id') == $grade->id) true @endif"> Kelas {{ $grade->name }}</a></li>
					@endforeach
				</ul>
			</div>
			<div class="card-body">
				<div class="tab-content">
					@foreach($grades as $i => $grade)
					<div id="tab-{{ $grade->id }}" class="tab-pane @if ($i == 0 && !session('grade_id')) active @endif @if (session('grade_id') == $grade->id) active @endif">
						<div class="row">
							<div class="col-md-12 text-right">
								<a href="{{ route('curriculums.details.add', ['id' => $curriculum->id, 'grade_id' => $grade->id]) }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</a>
								<a href="{{ route('curriculums.index') }}" class="btn btn-default"><i class="fa fa-back"></i> Kembali</a>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table id="table" class="table table-hovered table-striped">
										<thead class="thead-light">
											<tr>
												<th class="text-center">Kode</th>
												<th class="text-center">Mata Pelajaran</th>
												<th class="text-center">Alokasi Jam</th>
												<th class="text-center">KKM</th>
												<th class="text-center"></th>
											</tr>
										</thead>
										<tbody>
											@foreach($grade->lessons as $lesson)
											<tr>
												<td class="text-center">{{ $lesson->code }}</td>
												<td>{{ $lesson->name }}</td>
												<form class="form-inline" method="POST" action="{{ route('curriculums.details.update', ['id' => $curriculum->id, 'grade_id' => $grade->id]) }}">
												<td class="text-center">
													@csrf
													@method('PUT')
													<input type="number" class="form-control form-control-sm" style="width:100px;" name="allocation" value="{{ intval($lesson->pivot->allocation) }}"/>
												</td>
												<td>
													<input type="hidden" name="lesson_id" value="{{ $lesson->id }}"/>
													<div class="input-group" style="width: 150px;">
														<input type="number" class="form-control-sm form-control" name="kkm" value="{{ intval($lesson->pivot->kkm) }}">
														<span class="input-group-btn">
															<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Update</button>
														</span>
													</div>
												</td>
												</form>
												<td class="text-center">
													<form class="form-horizontal" method="POST" action="{{ route('curriculums.details.destroy', ['id' => $curriculum->id, 'grade_id' => $grade->id]) }}">
														@csrf
							 							@method('PUT')
														<input type="hidden" name="lesson_id" value="{{ $lesson->id }}"/>
														<button class="btn btn-sm btn-danger" type="submit"><i class="fas fa-trash"></i> Hapus</button>
													</form>
												</td>
											</tr>
											@endforeach
											@if ($grade->lessons->count() == 0)
											<tr>
												<td class="text-center" colspan="4">Tidak ada data</td>
											</tr>
											@endif
										</tbody>
										<tfoot>
											@if ($grade->lessons->count() > 0)
											<tr>
												<th class="text-center" colspan="2">Total Alokasi Jam</th>
												<th class="text-center">{{ $grade->lessons->sum('pivot.allocation') }}</th>
												<th colspan="2"></th>
											</tr>
											@endif
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
					@endforeach
				</div>	
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
@endsection