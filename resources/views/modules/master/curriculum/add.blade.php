@extends('templates.data.tabler.app')

@section('title')
Tambah Mata Pelajaran
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('curriculum detail add', $curriculum, $grade) }}
@endsection

@section('content')
<div class="row" id="baseapp">
	<div class="col-md-8">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<form action="{{ route('curriculums.details.store', ['id' => $curriculum->id, 'grade_id' => $grade->id]) }}" method="POST" class="form-horizontal" role="form">
							 @csrf
							 @method('PUT')
							<div class="form-group row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table id="table" class="table table-hovered table-striped">
											<thead class="thead-light">
												<tr>
													<th class="text-center"><input type="checkbox"></th>
													<th class="text-center">Code</th>
													<th class="text-center">Mata Pelajaran</th>
												</tr>
											</thead>
											<tbody>
												@foreach($lessons as $i => $lesson)
												<tr>
													<td class="text-center">
														<input type="checkbox" name="lesson_id[]" value="{{ $lesson->id }}"/>
													</td>
													<td class="text-center">{{ $lesson->code }}</td>
													<td class="text-left">{{ $lesson->name }}</td>
												</tr>
												@endforeach
												@if ($lessons->count() == 0)
												<tr>
													<td class="text-center" colspan="3">Tidak ada data</td>
												</tr>
												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Tambahkan</button>
                                    <a href="{{ route('curriculums.show', ['id' => $curriculum->id]) }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
</script>
@endsection