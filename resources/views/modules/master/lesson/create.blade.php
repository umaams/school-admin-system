@extends('templates.data.tabler.app')

@section('title')
Tambah Mata Pelajaran
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('lesson create') }}
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
            <div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('lessons.store') }}" method="POST" class="form-horizontal" role="form">
                            @csrf
                            <div class="form-group row @if ($errors->has('code')) has-error @endif">
                                <label class="form-label col-md-2">Kode</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="code" placeholder="Kode Mata Pelajaran" required>
                                    @if ($errors->has('code')) <div class="invalid-feedback">{{ $errors->first('code') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('name')) has-error @endif">
                                <label class="form-label col-md-2">Nama</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" placeholder="Nama Mata Pelajaran" required>
                                    @if ($errors->has('name')) <div class="invalid-feedback">{{ $errors->first('name') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('lesson_category_id')) has-error @endif">
                                <label class="form-label col-md-2">Kelompok</label>
                                <div class="col-md-3">
                                    <select class="form-control selectize" name="lesson_category_id" required>
                                        <option value="">Pilih Kelompok...</option>
                                        @foreach ($lesson_categories as $lesson_category)
                                        <option value="{{ $lesson_category->id }}">{{ $lesson_category->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('lesson_category_id')) <div class="invalid-feedback">{{ $errors->first('lesson_category_id') }}</div> @endif
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
$('.selectize').selectize({
    create: true,
    sortField: 'text'
});
</script>
@endsection