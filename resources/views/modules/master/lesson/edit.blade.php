@extends('templates.data.tabler.app')

@section('title')
Edit Mata Pelajaran
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('lesson edit', $lesson) }}
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
            <div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('lessons.update', $lesson->id) }}" method="POST" class="form-horizontal" role="form">
                            @csrf
                            @method('PUT')
                            <div class="form-group row @if ($errors->has('code')) has-error @endif">
                                <label class="form-label col-md-2">Kode</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="code" placeholder="Kode Mata Pelajaran" value="{{ $lesson->code }}" required>
                                    @if ($errors->has('code')) <div class="invalid-feedback">{{ $errors->first('code') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('name')) has-error @endif">
                                <label class="form-label col-md-2">Nama</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" placeholder="Nama Mata Pelajaran" value="{{ $lesson->name }}" required>
                                    @if ($errors->has('name')) <div class="invalid-feedback">{{ $errors->first('name') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('lesson_category_id')) has-error @endif">
                                <label class="form-label col-md-2">Kelompok</label>
                                <div class="col-md-3">
                                    <select class="form-control selectize" name="lesson_category_id" required>
                                        <option value="">Pilih Kelompok...</option>
                                        @foreach ($lesson_categories as $lesson_category)
                                        <option value="{{ $lesson_category->id }}" @if ($lesson_category->id == $lesson->lesson_category_id) selected @endif>{{ $lesson_category->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('lesson_category_id')) <div class="invalid-feedback">{{ $errors->first('lesson_category_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('active')) has-error @endif">
                                <label class="form-label col-md-2">Status</label>
                                <div class="col-md-6">
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" class="custom-control-input" id="inlineCheckbox1" name="active" value="1" @if ($lesson->active == '1') checked @endif required>
                                            <span class="custom-control-label" for="inlineCheckbox1">Aktif</span>
                                        </label>
                                        <label class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="inlineCheckbox2" class="custom-control-input" name="active" value="0" @if ($lesson->active == '0') checked @endif required>
                                            <span class="custom-control-label" for="inlineCheckbox2">Tidak Aktif</span>
                                        </label>
                                    </div>
                                    @if ($errors->has('active')) <div class="invalid-feedback">{{ $errors->first('active') }}</div> @endif
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
$('.selectize').selectize({
    create: true,
    sortField: 'text'
});
</script>
@endsection