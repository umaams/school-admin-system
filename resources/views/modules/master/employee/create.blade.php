@extends('templates.data.tabler.app')

@section('title')
Tambah Pegawai
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('employee create') }}
@endsection

@section('content')
<div id="baseapp">
    <form action="{{ route('employees.store') }}" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
        <div class="col-md-12">
            <div class="card">
                <div class="card-status bg-blue"></div>
                <div class="card-header">Informasi Pegawai</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            @csrf
                            <div class="form-group row @if ($errors->has('nip')) has-error @endif">
                                <label class="form-label col-md-2">NIP</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="nip" placeholder="No. Induk Pegawai" value="{{ old('nip') }}" required>
                                    @if ($errors->has('nip')) <div class="invalid-feedback">{{ $errors->first('nip') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('name')) has-error @endif">
                                <label class="form-label col-md-2">Nama</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" placeholder="Nama Lengkap" value="{{ old('name') }}" required>
                                    @if ($errors->has('name')) <div class="invalid-feedback">{{ $errors->first('name') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('sex')) has-error @endif">
                                <label class="form-label col-md-2">Jenis Kelamin</label>
                                <div class="col-md-6">
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-radio custom-control-inline">
                                            <input class="custom-control-input" id="inlineCheckbox1" type="radio" name="sex" value="M" @if (old('sex') == 'M') checked @endif required>
                                            <span class="custom-control-label" for="inlineCheckbox1">Laki-Laki</span>
                                        </label>
                                        <label class="custom-control custom-radio custom-control-inline">
                                            <input class="custom-control-input" id="inlineCheckbox2" type="radio" name="sex" value="F" @if (old('sex') == 'F') checked @endif required>
                                            <span class="custom-control-label" for="inlineCheckbox2">Perempuan</span>
                                        </label>
                                    </div>
                                    @if ($errors->has('sex')) <div class="invalid-feedback">{{ $errors->first('sex') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('birth_place')) has-error @endif">
                                <label class="form-label col-md-2">Tempat & Tgl. Lahir</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="birth_place" placeholder="Tempat Lahir" value="{{ old('birth_place') }}" required>
                                    @if ($errors->has('birth_place')) <div class="invalid-feedback">{{ $errors->first('birth_place') }}</div> @endif
                                </div>
                                <div class="col-md-2">
                                    <div class="input-group date" id="birth_date" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" name="birth_date" placeholder="Tgl Lahir" value="{{ old('birth_date') }}" data-target="#birth_date" required>
                                        <div class="input-group-append" data-target="#birth_date" data-toggle="datetimepicker"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                    </div>
                                    @if ($errors->has('birth_date')) <div class="invalid-feedback">{{ $errors->first('birth_date') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('address')) has-error @endif">
                                <label class="form-label col-md-2">Alamat</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" name="address" required>{{ old('address') }}</textarea>
                                    @if ($errors->has('address')) <div class="invalid-feedback">{{ $errors->first('address') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('province_id')) has-error @endif">
                                <label class="form-label col-md-2">Provinsi</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="province_id" v-model="province_id" required>
                                        <option value="">Pilih Provinsi...</option>
                                        <option v-for="province in provinces" v-bind:value="province.id">@{{ province.name }}<option>
                                    </select>
                                    @if ($errors->has('province_id')) <div class="invalid-feedback">{{ $errors->first('province_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('city_id')) has-error @endif">
                                <label class="form-label col-md-2">Kabupaten/Kota</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="city_id" v-model="city_id" required>
                                        <option value="">Pilih Kabupaten/Kota...</option>
                                        <option v-for="city in cities" v-bind:value="city.id">@{{ city.name }}<option>
                                    </select>
                                    @if ($errors->has('city_id')) <div class="invalid-feedback">{{ $errors->first('city_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('district_id')) has-error @endif">
                                <label class="form-label col-md-2">Kecamatan</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="district_id" v-model="district_id" required>
                                        <option value="">Pilih Kecamatan...</option>
                                        <option v-for="district in districts" v-bind:value="district.id">@{{ district.name }}<option>
                                    </select>
                                    @if ($errors->has('district_id')) <div class="invalid-feedback">{{ $errors->first('district_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('village_id')) has-error @endif">
                                <label class="form-label col-md-2">Desa</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="village_id" v-model="village_id" required>
                                        <option value="">Pilih Desa/Kelurahan...</option>
                                        <option v-for="village in villages" v-bind:value="village.id">@{{ village.name }}<option>
                                    </select>
                                    @if ($errors->has('village_id')) <div class="invalid-feedback">{{ $errors->first('village_id') }}</div> @endif
                                </div>
                            </div>
                            <input type="hidden" name="zip_code" value="61353">
                            <div class="form-group row @if ($errors->has('religion_id')) has-error @endif">
                                <label class="form-label col-md-2">Agama</label>
                                <div class="col-md-3">
                                    <select class="form-control selectize" name="religion_id" required>
                                        <option value="">Pilih Agama...</option>
                                        @foreach($religions as $religion)
                                        <option value="{{ $religion->id }}" @if (old('religion_id') == $religion->id) selected @endif>{{ $religion->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('religion_id')) <div class="invalid-feedback">{{ $errors->first('religion_id') }}</div> @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-status bg-blue"></div>
                <div class="card-header">Riwayat Pendidikan</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success" v-on:click="employee_educations.push('i')"><i class="fa fa-plus-circle"></i> Tambah Pendidikan</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead class="thead-light">
                                        <tr>
                                            <th class="text-center" rowspan="2" style="vertical-align:middle;">No.</th>
                                            <th class="text-center" rowspan="2" style="vertical-align:middle;">Tingkat</th>
                                            <th class="text-center" rowspan="2" style="vertical-align:middle;">Institusi</th>
                                            <th class="text-center" rowspan="2" style="vertical-align:middle;">Jurusan</th>
                                            <th class="text-center" colspan="2">Tahun</th>
                                            <th class="text-center" rowspan="2" style="vertical-align:middle;">No. Ijazah</th>
                                            <th class="text-center" rowspan="2" style="vertical-align:middle;">GPA</th>
                                            <th class="text-center" rowspan="2" style="vertical-align:middle;"></th>
                                        </tr>
                                        <tr>
                                            <th class="text-center">Mulai</th>
                                            <th class="text-center">Akhir</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(employee_education, i) in employee_educations">
                                            <td class="text-center">@{{ (i+1) }}</td>
                                            <td>
                                                <select class="form-control input-sm" name="employee_educations[]['education_id']" required>
                                                    <option value="">Pilih...</option>
                                                    <option v-for="education in educations" v-bind:value="education.id">@{{ education.name }}</option>
                                                </select>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control input-sm" name="employee_educations[]['place']" required>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control input-sm" name="employee_educations[]['department']">
                                            </td>
                                            <td class="text-center" style="width: 80px;">
                                                <input type="text" class="form-control input-sm" name="employee_educations[]['year_start']"  required>
                                            </td>
                                            <td class="text-center" style="width: 80px;">
                                                <input type="text" class="form-control input-sm" name="employee_educations[]['year_end']" required>
                                            </td>
                                            <td class="text-center" style="width: 120px;">
                                                <input type="text" class="form-control input-sm" name="employee_educations[]['certificate_number']" required>
                                            </td>
                                            <td class="text-center" style="width: 80px;">
                                                <input type="number" class="form-control input-sm" name="employee_educations[]['gpa']" required>
                                            </td>
                                            <td class="text-center">
                                                <button type="button" class="btn btn-danger btn-sm" v-on:click="employee_educations.splice(i, 1)"><i class="fa fa-trash-o"></i> Hapus</button>
                                            </td>
                                        </tr>
                                        <tr v-if="employee_educations.length == 0">
                                            <td class="text-center" colspan="9">Tidak ada data</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-status bg-blue"></div>
                <div class="card-header">Informasi Lain</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row @if ($errors->has('registered_at')) has-error @endif">
                                <label class="form-label col-md-2">Terdaftar Tanggal</label>
                                <div class="col-md-2">
                                    <div class='input-group date' id="registered_at" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" name="registered_at" placeholder="Tgl. Terdaftar" value="{{ old('registered_at') }}" data-target="#registered_at" required>
                                        <span class="input-group-append" data-target="#registered_at" data-toggle="datetimepicker">
                                            <span class="input-group-text"><span class="fa fa-calendar"></span></span>
                                        </span>
                                    </div>
                                    @if ($errors->has('registered_at')) <div class="invalid-feedback">{{ $errors->first('registered_at') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('image_file')) has-error @endif">
                                <label class="form-label col-md-2">Image File</label>
                                <div class="col-md-6">
                                    <input type="file" class="form-control" name="image_file" placeholder="Image File" accept="image/*">
                                    @if ($errors->has('image_file')) <div class="invalid-feedback">{{ $errors->first('image_file') }}</div> @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('scripts')
<script>
    const vm = new Vue({
        el: "#baseapp",
        data: {
            provinces: [],
            cities: [],
            districts: [],
            villages: [],
            educations: [],
            province_id: '{{ old('province_id') }}',
            city_id: '{{ old('city_id') }}',
            district_id: '{{ old('district_id') }}',
            village_id: '{{ old('village_id') }}',
            employee_educations: []
        },
        watch: {
            province_id: function (value) {
                this.cities = [];
                this.city_id = '';
                this.getCities(value);
            },
            city_id: function (value) {
                this.districts = [];
                this.district_id = '';
                this.getDistricts(value);
            },
            district_id: function (value) {
                this.villages = [];
                this.village_id = '';
                this.getVillages(value);
            }
        },
        methods: {
            getEducations: function () {
                axios.get('/educations').then(response => {
                    this.educations = response.data.educations;
                });
            },
            getProvinces: function () {
                axios.get('/provinces').then(response => {
                    this.provinces = response.data.provinces;
                });
            },
            getCities: function (province_id) {
                axios.get('/provinces/' + province_id + '/cities').then(response => {
                    this.cities = response.data.cities;
                });
            },
            getDistricts: function (city_id) {
                axios.get('/cities/' + city_id + '/districts').then(response => {
                    this.districts = response.data.districts;
                });
            },
            getVillages: function (district_id) {
                axios.get('/districts/' + district_id + '/villages').then(response => {
                    this.villages = response.data.villages;
                });
            }
        },
        mounted () {
            this.getEducations();
            this.getProvinces();
            if (this.province_id != '') this.getCities(this.province_id);
            if (this.city_id != '') this.getDistricts(this.city_id);
            if (this.district_id != '') this.getVillages(this.district_id);
            $('.selectize').selectize({
                create: true,
                sortField: 'text'
            });
            $('#birth_date').datetimepicker({
                locale: 'id',
                format: 'YYYY-MM-DD'
            });
            $('#registered_at').datetimepicker({
                locale: 'id',
                format: 'YYYY-MM-DD'
            });
        }
    });
</script>
@endsection