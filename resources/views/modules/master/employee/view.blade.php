@extends('templates.data.tabler.app')

@section('title')
Pegawai
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('employee') }}
@endsection

@section('content')
<div class="row" id="baseapp">
	<div class="col-md-12">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="card-header">
				<div class="card-options">
					<a href="{{ route('employees.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</a>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<table id="table" class="table table-hovered table-striped" width="100%">
							<thead class="thead-light">
								<tr>
									<th class="text-center"></th>
									<th class="text-center">NIP</th>
									<th class="text-center">Nama Lengkap</th>
									<th class="text-center">JK</th>
									<th class="text-center">Tgl Lahir</th>
									<th class="text-center">Terdaftar pada</th>
									<th class="text-center">Status</th>
									<th class="text-center">Alamat Desa</th>
									<th class="text-center" style="width: 150px;"></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
var vm = new Vue({
	el: '#baseapp',
	data: {
		table: null,
	},
	methods: {
		initDatatable: function () {
		    this.table = $('#table').DataTable({
		        processing: true,
		        serverSide: true,
		        responsive: true,
		        ajax: '{{ route('employees.datatable') }}',
		        columns: [
		        	{ data: 'image_path', name: 'image_path', sortable: false, searchable: false },
		            { data: 'nip', name: 'nip', className: 'text-center' },
		            { data: 'name', name: 'name' },
		            { data: 'sex', name: 'sex', className: 'text-center' },
		            { data: 'birth_date', name: 'birth_date', className: 'text-center' },
		            { data: 'registered_at', name: 'registered_at', className: 'text-center' },
		            { data: 'active', name: 'active', className: 'text-center' },
		            { data: 'village.name', name: 'village.name' },
		            { data: 'action', name: 'action', className: 'text-center', sortable: false, searchable: false }
		        ],
		        language: { 
			       "processing": "<span class='text text-info'><b><i class='fa fa-spinner fa-spin'></i> Loading...</b></span>"
			    }
		    });
		}
	},
	mounted(){
		this.initDatatable();
	}
})
</script>
@endsection

