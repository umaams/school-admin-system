@extends('templates.data.tabler.app')

@section('title')
Extrakurikuler
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('lesson_category') }}
@endsection

@section('content')
<div class="row" id="baseapp">
	<div class="col-md-12">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="card-header">
				<div class="card-options">
					<a href="{{ route('extracurriculars.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</a>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<table id="table" class="table table-hovered table-striped" width="100%">
							<thead class="thead-light">
								<tr>
									<th class="text-center">Nama</th>
									<th class="text-center">Pembina</th>
									<th class="text-center">Waktu Kegiatan</th>
									<th class="text-center" style="width: 150px;"></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
var vm = new Vue({
	el: '#baseapp',
	data: {
		table: null,
	},
	methods: {
		initDatatable: function () {
		    this.table = $('#table').DataTable({
		        processing: true,
		        serverSide: true,
		        responsive: true,
		        ajax: '{{ route('extracurriculars.datatable') }}',
		        columns: [
		            { data: 'name', name: 'name' },
		            { data: 'coach', name: 'coach' },
		            { data: 'event_time', name: 'event_time' },
		            { data: 'action', name: 'action', className: 'text-center', sortable: false, searchable: false }
		        ],
		        language: { 
			       "processing": "<span class='text text-info'><b><i class='fa fa-spinner fa-spin'></i> Loading...</b></span>"
			    }
		    });
		}
	},
	mounted(){
		this.initDatatable();
	}
})
</script>
@endsection