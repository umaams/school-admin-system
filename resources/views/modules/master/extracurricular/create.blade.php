@extends('templates.data.tabler.app')

@section('title')
Tambah Ekstrakurikuler
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('lesson_category create') }}
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
            <div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('extracurriculars.store') }}" method="POST" class="form-horizontal" role="form">
                            @csrf
                            <div class="form-group row @if ($errors->has('name')) has-error @endif">
                                <label class="form-label col-md-2">Nama</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" placeholder="Nama Extrakurikuler" value="{{ old('name') }}" required>
                                    @if ($errors->has('name')) <div class="invalid-feedback">{{ $errors->first('name') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('coach')) has-error @endif">
                                <label class="form-label col-md-2">Pembina</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="coach" placeholder="Nama Pembina" value="{{ old('coach') }}" required>
                                    @if ($errors->has('coach')) <div class="invalid-feedback">{{ $errors->first('coach') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('event_time')) has-error @endif">
                                <label class="form-label col-md-2">Waktu Kegiatan</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="event_time" placeholder="Waktu Kegiatan" value="{{ old('event_time') }}" required>
                                    @if ($errors->has('event_time')) <div class="invalid-feedback">{{ $errors->first('event_time') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('description')) has-error @endif">
                                <label class="form-label col-md-2">Deskripsi</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" name="description" placeholder="Deskripsi Extrakurikuler" required>{{ old('description') }}</textarea>
                                    @if ($errors->has('description')) <div class="invalid-feedback">{{ $errors->first('description') }}</div> @endif
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection