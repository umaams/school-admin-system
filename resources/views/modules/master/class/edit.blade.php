@extends('templates.data.tabler.app')

@section('title')
Edit Kelas
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('class edit', $class) }}
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
            <div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('classes.update', $class->id) }}" method="POST" class="form-horizontal" role="form">
                            @csrf
                            @method('PUT')
                            <div class="form-group row @if ($errors->has('name')) has-error @endif">
                                <label class="form-label col-md-2">Nama</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" placeholder="Nama Kelas" value="{{ $class->name }}" required>
                                    @if ($errors->has('name')) <div class="invalid-feedback">{{ $errors->first('name') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('grade_id')) has-error @endif">
                                <label class="form-label col-md-2">Tingkat</label>
                                <div class="col-md-3">
                                    <select class="form-control selectize" name="grade_id">
                                        <option value="">Pilih Tingkat...</option>
                                        @foreach ($grades as $grade)
                                        <option value="{{ $grade->id }}" @if ($class->grade_id == $grade->id) selected @endif>{{ $grade->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('grade_id')) <div class="invalid-feedback">{{ $errors->first('grade_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('active')) has-error @endif">
                                <label class="form-label col-md-2">Status</label>
                                <div class="col-md-6">
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-radio custom-control-inline">
                                            <input class="custom-control-input" type="radio" name="active" value="1" @if ($class->active == '1') checked @endif required>
                                            <span class="custom-control-label">Aktif</span>
                                        </label>
                                        <label class="custom-control custom-radio custom-control-inline">
                                            <input class="custom-control-input" type="radio" name="active" value="0" @if ($class->active == '0') checked @endif required>
                                            <span class="custom-control-label">Tidak Aktif</span>
                                        </label>
                                    </div>
                                    @if ($errors->has('active')) <div class="invalid-feedback">{{ $errors->first('active') }}</div> @endif
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
$('.selectize').selectize({
    create: true,
    sortField: 'text'
});
</script>
@endsection

