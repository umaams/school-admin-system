@extends('templates.data.tabler.app')

@section('title')
Tambah Jam Pelajaran
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('school_time create') }}
@endsection

@section('content')
<div class="row" id="baseapp">
	<div class="col-md-12">
		<div class="card">
            <div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('school_times.store') }}" method="POST" class="form-horizontal" role="form">
                            @csrf
                            <div class="form-group row @if ($errors->has('id')) has-error @endif">
                                <label class="form-label col-md-2">Index Jam</label>
                                <div class="col-md-2">
                                    <input type="number" class="form-control" name="id" placeholder="Index" required>
                                    @if ($errors->has('id')) <div class="invalid-feedback">{{ $errors->first('id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('name')) has-error @endif">
                                <label class="form-label col-md-2">Rentang Jam</label>
                                <div class="col-md-2">
                                    <div class="input-group timepicker">
                                        <input type="text" class="form-control" name="time_start" placeholder="Waktu Mulai" required>
                                        <div class="input-group-append"><i class="fa fa-clock-o"></i></div>
                                    </div>
                                    @if ($errors->has('time_start')) <div class="invalid-feedback">{{ $errors->first('time_start') }}</div> @endif
                                </div>
                                <div class="col-md-2">
                                    <div class="input-group timepicker">
                                        <input type="text" class="form-control" name="time_end" placeholder="Waktu Akhir" required>
                                        <div class="input-group-append"><i class="fa fa-clock-o"></i></div>
                                    </div>
                                    @if ($errors->has('time_end')) <div class="invalid-feedback">{{ $errors->first('time_end') }}</div> @endif
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
var vm = new Vue({
	el: '#baseapp',
	data: {
		table: null,
	},
	methods: {
	},
	mounted(){
        $('.timepicker').datetimepicker({
            format: 'HH:mm'
        });
	}
})
</script>
@endsection