@extends('templates.data.tabler.app')

@section('title')
Tambah Siswa
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('student create') }}
@endsection

@section('content')
<div id="baseapp">
    <form action="{{ route('students.store') }}" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
        <div class="col-md-12">
            <div class="card">
                <div class="card-status bg-blue"></div>
                <div class="card-header">Informasi Siswa</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            @csrf
                            <div class="form-group row @if ($errors->has('nis')) has-error @endif">
                                <label class="form-label col-md-2">No. Induk Siswa</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="nis" placeholder="No. Induk" value="{{ old('nis') }}" required>
                                    @if ($errors->has('nis')) <div class="invalid-feedback">{{ $errors->first('nis') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('name')) has-error @endif">
                                <label class="form-label col-md-2">Nama</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" placeholder="Nama Lengkap" value="{{ old('name') }}" required>
                                    @if ($errors->has('name')) <div class="invalid-feedback">{{ $errors->first('name') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('sex')) has-error @endif">
                                <label class="form-label col-md-2">Jenis Kelamin</label>
                                <div class="col-md-6">
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-radio custom-control-inline">
                                            <input class="custom-control-input" id="inlineCheckbox1" type="radio" name="sex" value="M" @if (old('sex') == 'M') checked @endif required>
                                            <span class="custom-control-label" for="inlineCheckbox1">Laki-Laki</span>
                                        </label>
                                        <label class="custom-control custom-radio custom-control-inline">
                                            <input class="custom-control-input" id="inlineCheckbox2" type="radio" name="sex" value="F" @if (old('sex') == 'F') checked @endif required>
                                            <span class="custom-control-label" for="inlineCheckbox2">Perempuan</span>
                                        </label>
                                    </div>
                                    @if ($errors->has('sex')) <div class="invalid-feedback">{{ $errors->first('sex') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('birth_place')) has-error @endif">
                                <label class="form-label col-md-2">Tempat & Tgl. Lahir</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="birth_place" placeholder="Tempat Lahir" value="{{ old('birth_place') }}" required>
                                    @if ($errors->has('birth_place')) <div class="invalid-feedback">{{ $errors->first('birth_place') }}</div> @endif
                                </div>
                                <div class="col-md-2">
                                    <div class="input-group date" id="birth_date" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" name="birth_date" placeholder="Tgl Lahir" value="{{ old('birth_date') }}" data-target="#birth_date" required>
                                        <div class="input-group-append" data-target="#birth_date" data-toggle="datetimepicker"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                    </div>
                                    @if ($errors->has('birth_date')) <div class="invalid-feedback">{{ $errors->first('birth_date') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('address')) has-error @endif">
                                <label class="form-label col-md-2">Alamat</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" name="address" required>{{ old('address') }}</textarea>
                                    @if ($errors->has('address')) <div class="invalid-feedback">{{ $errors->first('address') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('province_id')) has-error @endif">
                                <label class="form-label col-md-2">Provinsi</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="province_id" v-model="province_id" required>
                                        <option value="">Pilih Provinsi...</option>
                                        <option v-for="province in provinces" v-bind:value="province.id">@{{ province.name }}<option>
                                    </select>
                                    @if ($errors->has('province_id')) <div class="invalid-feedback">{{ $errors->first('province_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('city_id')) has-error @endif">
                                <label class="form-label col-md-2">Kabupaten/Kota</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="city_id" v-model="city_id" required>
                                        <option value="">Pilih Kabupaten/Kota...</option>
                                        <option v-for="city in cities" v-bind:value="city.id">@{{ city.name }}<option>
                                    </select>
                                    @if ($errors->has('city_id')) <div class="invalid-feedback">{{ $errors->first('city_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('district_id')) has-error @endif">
                                <label class="form-label col-md-2">Kecamatan</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="district_id" v-model="district_id" required>
                                        <option value="">Pilih Kecamatan...</option>
                                        <option v-for="district in districts" v-bind:value="district.id">@{{ district.name }}<option>
                                    </select>
                                    @if ($errors->has('district_id')) <div class="invalid-feedback">{{ $errors->first('district_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('village_id')) has-error @endif">
                                <label class="form-label col-md-2">Desa</label>
                                <div class="col-md-4">
                                    <select class="form-control" name="village_id" v-model="village_id" required>
                                        <option value="">Pilih Desa/Kelurahan...</option>
                                        <option v-for="village in villages" v-bind:value="village.id">@{{ village.name }}<option>
                                    </select>
                                    @if ($errors->has('village_id')) <div class="invalid-feedback">{{ $errors->first('village_id') }}</div> @endif
                                </div>
                            </div>
                            <input type="hidden" name="zip_code" value="61353">
                            <div class="form-group row @if ($errors->has('religion_id')) has-error @endif">
                                <label class="form-label col-md-2">Agama</label>
                                <div class="col-md-3">
                                    <select class="form-control selectize" name="religion_id" required>
                                        <option value="">Pilih Agama...</option>
                                        @foreach($religions as $religion)
                                        <option value="{{ $religion->id }}" @if (old('religion_id') == $religion->id) selected @endif>{{ $religion->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('religion_id')) <div class="invalid-feedback">{{ $errors->first('religion_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('family_order')) has-error @endif">
                                <label class="form-label col-md-2">Anak Ke-</label>
                                <div class="col-md-1">
                                    <input type="text" name="family_order" class="form-control" value="{{ old('family_order') }}" required>
                                    @if ($errors->has('family_order')) <div class="invalid-feedback">{{ $errors->first('family_order') }}</div> @endif
                                </div>
                                <div class="col-md-1">
                                    <input type="text" name="family_number" class="form-control" value="{{ old('family_number') }}" required>
                                    @if ($errors->has('family_number')) <div class="invalid-feedback">{{ $errors->first('family_number') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('blood_type_id')) has-error @endif">
                                <label class="form-label col-md-2">Golongan Darah</label>
                                <div class="col-md-3">
                                    <select class="form-control selectize" name="blood_type_id" required>
                                        <option value="">Pilih golongan darah...</option>
                                        @foreach($blood_types as $blood_type)
                                        <option value="{{ $blood_type->id }}" @if (old('blood_type_id') == $blood_type->id) selected @endif>{{ $blood_type->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('blood_type_id')) <div class="invalid-feedback">{{ $errors->first('blood_type_id') }}</div> @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-status bg-blue"></div>
                <div class="card-header">Informasi Ayah</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row @if ($errors->has('father_name')) has-error @endif">
                                <label class="form-label col-md-2">Nama Ayah</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="father_name" placeholder="Nama Lengkap Ayah" value="{{ old('father_name') }}" required>
                                    @if ($errors->has('father_name')) <div class="invalid-feedback">{{ $errors->first('father_name') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('father_alive')) has-error @endif">
                                <label class="form-label col-md-2">Masih Hidup</label>
                                <div class="col-md-6">
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-radio custom-control-inline">
                                            <input class="custom-control-input" id="inlineCheckbox1" type="radio" name="father_alive" value="1" @if (old('father_alive') == '1') checked @endif required>
                                            <span class="custom-control-label" for="inlineCheckbox1">Ya</span>
                                        </label>
                                        <label class="custom-control custom-radio custom-control-inline">
                                            <input class="custom-control-input" id="inlineCheckbox2" type="radio" name="father_alive" value="0" @if (old('father_alive') == '0') checked @endif required>
                                            <span class="custom-control-label" for="inlineCheckbox2">Tidak</span>
                                        </label>
                                    </div>
                                    @if ($errors->has('father_alive')) <div class="invalid-feedback">{{ $errors->first('father_alive') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('father_address')) has-error @endif">
                                <label class="form-label col-md-2">Alamat Ayah</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" name="father_address" required>{{ old('father_address') }}</textarea>
                                    @if ($errors->has('father_address')) <div class="invalid-feedback">{{ $errors->first('father_address') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('father_occupation_id')) has-error @endif">
                                <label class="form-label col-md-2">Pekerjaan Ayah</label>
                                <div class="col-md-4">
                                    <select class="form-control selectize" name="father_occupation_id" required>
                                        <option value="">Pilih Pekerjaan...</option>
                                        @foreach($occupations as $occupation)
                                        <option value="{{ $occupation->id }}" @if (old('father_occupation_id') == $occupation->id) selected @endif>{{ $occupation->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('father_occupation_id')) <div class="invalid-feedback">{{ $errors->first('father_occupation_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('father_phone')) has-error @endif">
                                <label class="form-label col-md-2">Telp. Ayah</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="father_phone" placeholder="Nama Telp. Ayah" value="{{ old('father_phone') }}">
                                    @if ($errors->has('father_phone')) <div class="invalid-feedback">{{ $errors->first('father_phone') }}</div> @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-status bg-blue"></div>
                <div class="card-header">Informasi Ibu</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row @if ($errors->has('mother_name')) has-error @endif">
                                <label class="form-label col-md-2">Nama Ibu</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="mother_name" placeholder="Nama Lengkap Ibu" value="{{ old('mother_name') }}" required>
                                    @if ($errors->has('mother_name')) <div class="invalid-feedback">{{ $errors->first('mother_name') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('mother_alive')) has-error @endif">
                                <label class="form-label col-md-2">Masih Hidup</label>
                                <div class="col-md-6">
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-radio custom-control-inline">
                                            <input class="custom-control-input" id="inlineCheckbox1" type="radio" name="mother_alive" value="1" @if (old('mother_alive') == '1') checked @endif required>
                                            <span class="custom-control-label" for="inlineCheckbox1">Ya</span>
                                        </label>
                                        <label class="custom-control custom-radio custom-control-inline">
                                            <input class="custom-control-input" id="inlineCheckbox2" type="radio" name="mother_alive" value="0" @if (old('mother_alive') == '0') checked @endif required>
                                            <span class="custom-control-label" for="inlineCheckbox2">Tidak</span>
                                        </label>
                                    </div>
                                    @if ($errors->has('mother_alive')) <div class="invalid-feedback">{{ $errors->first('mother_alive') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('mother_address')) has-error @endif">
                                <label class="form-label col-md-2">Alamat Ibu</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" name="mother_address" required>{{ old('mother_address') }}</textarea>
                                    @if ($errors->has('mother_address')) <div class="invalid-feedback">{{ $errors->first('mother_address') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('mother_occupation_id')) has-error @endif">
                                <label class="form-label col-md-2">Pekerjaan Ibu</label>
                                <div class="col-md-4">
                                    <select class="form-control selectize" name="mother_occupation_id" required>
                                        <option value="">Pilih Pekerjaan...</option>
                                        @foreach($occupations as $occupation)
                                        <option value="{{ $occupation->id }}" @if (old('mother_occupation_id') == $occupation->id) selected @endif>{{ $occupation->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('mother_occupation_id')) <div class="invalid-feedback">{{ $errors->first('mother_occupation_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('mother_phone')) has-error @endif">
                                <label class="form-label col-md-2">Telp Ibu</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="mother_phone" placeholder="Nama Telp. Ibu" value="{{ old('mother_phone') }}">
                                    @if ($errors->has('mother_phone')) <div class="invalid-feedback">{{ $errors->first('mother_phone') }}</div> @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-status bg-blue"></div>
                <div class="card-header">Informasi Wali Siswa</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row @if ($errors->has('guardian_name')) has-error @endif">
                                <label class="form-label col-md-2">Nama Wali</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="guardian_name" placeholder="Nama Lengkap Wali" value="{{ old('guardian_name') }}" required>
                                    @if ($errors->has('guardian_name')) <div class="invalid-feedback">{{ $errors->first('guardian_name') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('guardian_address')) has-error @endif">
                                <label class="form-label col-md-2">Alamat Ayah</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" name="guardian_address" required>{{ old('guardian_address') }}</textarea>
                                    @if ($errors->has('guardian_address')) <div class="invalid-feedback">{{ $errors->first('guardian_address') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('guardian_occupation_id')) has-error @endif">
                                <label class="form-label col-md-2">Pekerjaan Wali</label>
                                <div class="col-md-4">
                                    <select class="form-control selectize" name="guardian_occupation_id" required>
                                        <option value="">Pilih Pekerjaan...</option>
                                        @foreach($occupations as $occupation)
                                        <option value="{{ $occupation->id }}" @if (old('guardian_occupation_id') == $occupation->id) selected @endif>{{ $occupation->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('guardian_occupation_id')) <div class="invalid-feedback">{{ $errors->first('guardian_occupation_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('guardian_phone')) has-error @endif">
                                <label class="form-label col-md-2">Telp Wali</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="guardian_phone" placeholder="Nama Telp. Wali" value="{{ old('guardian_phone') }}">
                                    @if ($errors->has('guardian_phone')) <div class="invalid-feedback">{{ $errors->first('guardian_phone') }}</div> @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-status bg-blue"></div>
                <div class="card-header">Informasi Lain</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row @if ($errors->has('poor_family')) has-error @endif">
                                <label class="form-label col-md-2">Keluarga Kurang Mampu</label>
                                <div class="col-md-6">
                                    <div class="custom-controls-stacked">
                                        <label class="custom-control custom-radio custom-control-inline">
                                            <input class="custom-control-input" id="inlineCheckbox1" type="radio" name="poor_family" value="1" @if (old('poor_family') == '1') checked @endif required>
                                            <span class="custom-control-label" for="inlineCheckbox1">Ya</span>
                                        </label>
                                        <label class="custom-control custom-radio custom-control-inline">
                                            <input class="custom-control-input" id="inlineCheckbox2" type="radio" name="poor_family" value="0" @if (old('poor_family') == '0') checked @endif required>
                                            <span class="custom-control-label" for="inlineCheckbox2">Tidak</span>
                                        </label>
                                    </div>
                                    @if ($errors->has('poor_family')) <div class="invalid-feedback">{{ $errors->first('poor_family') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('registered_at')) has-error @endif">
                                <label class="form-label col-md-2">Terdaftar Tanggal</label>
                                <div class="col-md-2">
                                    <div class='input-group date' id="registered_at" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" name="registered_at" placeholder="Tgl. Terdaftar" value="{{ old('registered_at') }}" data-target="#registered_at" required>
                                        <span class="input-group-append" data-target="#registered_at" data-toggle="datetimepicker">
                                            <span class="input-group-text"><span class="fa fa-calendar"></span></span>
                                        </span>
                                    </div>
                                    @if ($errors->has('registered_at')) <div class="invalid-feedback">{{ $errors->first('registered_at') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('registered_school_year_id')) has-error @endif">
                                <label class="form-label col-md-2">Terdaftar Pada Tahun Ajaran</label>
                                <div class="col-md-3">
                                    <select class="form-control selectize" name="registered_school_year_id" required>
                                        <option value="">Pilih Tahun Ajaran...</option>
                                        @foreach($school_years as $school_year)
                                        <option value="{{ $school_year->id }}" @if (old('registered_school_year_id') == $school_year->id) selected @endif>{{ $school_year->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('registered_school_year_id')) <div class="invalid-feedback">{{ $errors->first('registered_school_year_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('registered_grade_id')) has-error @endif">
                                <label class="form-label col-md-2">Terdaftar pada Kelas</label>
                                <div class="col-md-3">
                                    <select class="form-control selectize" name="registered_grade_id" required>
                                        <option value="">Pilih Kelas...</option>
                                        @foreach($grades as $grade)
                                        <option value="{{ $grade->id }}" @if (old('registered_grade_id') == $grade->id) selected @endif>Kelas {{ $grade->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('registered_grade_id')) <div class="invalid-feedback">{{ $errors->first('registered_grade_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('old_school')) has-error @endif">
                                <label class="form-label col-md-2">Nama Sekolah Sebelumnya</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="old_school" placeholder="Nama Telp. Wali" value="{{ old('old_school') }}">
                                    @if ($errors->has('old_school')) <div class="invalid-feedback">{{ $errors->first('old_school') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('image_file')) has-error @endif">
                                <label class="form-label col-md-2">Image File</label>
                                <div class="col-md-6">
                                    <input type="file" class="form-control" name="image_file" placeholder="Image File" accept="image/*">
                                    @if ($errors->has('image_file')) <div class="invalid-feedback">{{ $errors->first('image_file') }}</div> @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('scripts')
<script>
    const vm = new Vue({
        el: "#baseapp",
        data: {
            provinces: [],
            cities: [],
            districts: [],
            villages: [],
            province_id: '{{ old('province_id') }}',
            city_id: '{{ old('city_id') }}',
            district_id: '{{ old('district_id') }}',
            village_id: '{{ old('village_id') }}'
        },
        watch: {
            province_id: function (value) {
                this.cities = [];
                this.city_id = '';
                this.getCities(value);
            },
            city_id: function (value) {
                this.districts = [];
                this.district_id = '';
                this.getDistricts(value);
            },
            district_id: function (value) {
                this.villages = [];
                this.village_id = '';
                this.getVillages(value);
            }
        },
        methods: {
            getProvinces: function () {
                axios.get('/provinces').then(response => {
                    this.provinces = response.data.provinces;
                });
            },
            getCities: function (province_id) {
                axios.get('/provinces/' + province_id + '/cities').then(response => {
                    this.cities = response.data.cities;
                });
            },
            getDistricts: function (city_id) {
                axios.get('/cities/' + city_id + '/districts').then(response => {
                    this.districts = response.data.districts;
                });
            },
            getVillages: function (district_id) {
                axios.get('/districts/' + district_id + '/villages').then(response => {
                    this.villages = response.data.villages;
                });
            }
        },
        mounted () {
            this.getProvinces();
            if (this.province_id != '') this.getCities(this.province_id);
            if (this.city_id != '') this.getDistricts(this.city_id);
            if (this.district_id != '') this.getVillages(this.district_id);
            $('.selectize').selectize({
                create: true,
                sortField: 'text'
            });
            $('#birth_date').datetimepicker({
                locale: 'id',
                format: 'YYYY-MM-DD'
            });
            $('#registered_at').datetimepicker({
                locale: 'id',
                format: 'YYYY-MM-DD'
            });
        }
    });
</script>
@endsection