@extends('templates.data.tabler.app')

@section('title')
Lihat Siswa
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('student edit', $student) }}
@endsection

@section('content')
<div id="baseapp" class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-status bg-blue"></div>
            <div class="card-header">Informasi Siswa</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row @if ($errors->has('nis')) has-error @endif">
                            <label class="form-label col-md-2">No. Induk Siswa</label>
                            <div class="col-md-3">
                                <div class="form-control-plaintext">{{ $student->nis }}</div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('name')) has-error @endif">
                            <label class="form-label col-md-2">Nama</label>
                            <div class="col-md-6">
                                <div class="form-control-plaintext">{{ $student->name }}</div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('sex')) has-error @endif">
                            <label class="form-label col-md-2">Jenis Kelamin</label>
                            <div class="col-md-6">
                                <div class="form-control-plaintext">
                                @if ($student->sex == 'M') Laki-Laki @endif
                                @if ($student->sex == 'F') Perempuan @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('birth_place')) has-error @endif">
                            <label class="form-label col-md-2">Tempat & Tgl. Lahir</label>
                            <div class="col-md-5">
                                <div class="form-control-plaintext">{{ $student->birth_place }}, {{ $student->birth_date->format('d F Y') }}</div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('address')) has-error @endif">
                            <label class="form-label col-md-2">Alamat</label>
                            <div class="col-md-6">
                                <div class="form-control-plaintext">{{ $student->address }}</div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('province_id')) has-error @endif">
                            <label class="form-label col-md-2">Provinsi</label>
                            <div class="col-md-4">
                                <div class="form-control-plaintext">{{ $student->village->district->city->province->name }}</div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('city_id')) has-error @endif">
                            <label class="form-label col-md-2">Kabupaten/Kota</label>
                            <div class="col-md-4">
                                <div class="form-control-plaintext">{{ $student->village->district->city->name }}</div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('district_id')) has-error @endif">
                            <label class="form-label col-md-2">Kecamatan</label>
                            <div class="col-md-4">
                                <div class="form-control-plaintext">{{ $student->village->district->name }}</div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('village_id')) has-error @endif">
                            <label class="form-label col-md-2">Desa</label>
                            <div class="col-md-4">
                                <div class="form-control-plaintext">{{ $student->village->name }}</div>
                            </div>
                        </div>
                        <input type="hidden" name="zip_code" value="61353">
                        <div class="form-group row @if ($errors->has('religion_id')) has-error @endif">
                            <label class="form-label col-md-2">Agama</label>
                            <div class="col-md-3">
                                <div class="form-control-plaintext">
                                @if ($student->religion)
                                {{ $student->religion->name }}
                                @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-status bg-blue"></div>
            <div class="card-header">Informasi Ayah</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row @if ($errors->has('father_name')) has-error @endif">
                            <label class="form-label col-md-2">Nama Ayah</label>
                            <div class="col-md-6">
                                <div class="form-control-plaintext">{{ $student->father_name }}</div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('father_alive')) has-error @endif">
                            <label class="form-label col-md-2">Masih Hidup</label>
                            <div class="col-md-6">
                                <div class="form-control-plaintext">
                                @if ($student->father_alive == '1') Ya @endif
                                @if ($student->father_alive == '0') Tidak @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('father_address')) has-error @endif">
                            <label class="form-label col-md-2">Alamat Ayah</label>
                            <div class="col-md-6">
                                <div class="form-control-plaintext">{{ $student->father_address }}</div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('father_occupation_id')) has-error @endif">
                            <label class="form-label col-md-2">Pekerjaan Ayah</label>
                            <div class="col-md-4">
                                <div class="form-control-plaintext">
                                @if ($student->father_occupation)
                                {{ $student->father_occupation->name }}
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('father_phone')) has-error @endif">
                            <label class="form-label col-md-2">Telp. Ayah</label>
                            <div class="col-md-3">
                                <div class="form-control-plaintext">{{ $student->father_phone }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-status bg-blue"></div>
            <div class="card-header">Informasi Ibu</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row @if ($errors->has('mother_name')) has-error @endif">
                            <label class="form-label col-md-2">Nama Ibu</label>
                            <div class="col-md-6">
                                <div class="form-control-plaintext">{{ $student->mother_name }}</div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('mother_alive')) has-error @endif">
                            <label class="form-label col-md-2">Masih Hidup</label>
                            <div class="col-md-6">
                                <div class="form-control-plaintext">
                                @if ($student->mother_alive == '1') Ya @endif
                                @if ($student->mother_alive == '0') Tidak @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('mother_address')) has-error @endif">
                            <label class="form-label col-md-2">Alamat Ayah</label>
                            <div class="col-md-6">
                                <div class="form-control-plaintext">{{ $student->mother_address }}</div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('mother_occupation_id')) has-error @endif">
                            <label class="form-label col-md-2">Pekerjaan Ibu</label>
                            <div class="col-md-4">
                                <div class="form-control-plaintext">
                                @if ($student->mother_occupation)
                                {{ $student->mother_occupation->name }}
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('mother_phone')) has-error @endif">
                            <label class="form-label col-md-2">Telp Ibu</label>
                            <div class="col-md-3">
                                <div class="form-control-plaintext">{{ $student->mother_phone }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-status bg-blue"></div>
            <div class="card-header">Informasi Wali Siswa</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row @if ($errors->has('guardian_name')) has-error @endif">
                            <label class="form-label col-md-2">Nama Wali</label>
                            <div class="col-md-6">
                                <div class="form-control-plaintext">{{ $student->guardian_name }}</div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('guardian_address')) has-error @endif">
                            <label class="form-label col-md-2">Alamat Ayah</label>
                            <div class="col-md-6">
                                <div class="form-control-plaintext">{{ $student->guardian_address }}</div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('guardian_occupation_id')) has-error @endif">
                            <label class="form-label col-md-2">Pekerjaan Wali</label>
                            <div class="col-md-4">
                                <div class="form-control-plaintext">
                                @if ($student->guardian_occupation)
                                {{ $student->guardian_occupation->name }}
                                @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('guardian_phone')) has-error @endif">
                            <label class="form-label col-md-2">Telp Wali</label>
                            <div class="col-md-3">
                                <div class="form-control-plaintext">{{ $student->guardian_phone }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-status bg-blue"></div>
            <div class="card-header">Informasi Lain</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row @if ($errors->has('registered_at')) has-error @endif">
                            <label class="form-label col-md-2">Terdaftar Tanggal</label>
                            <div class="col-md-2">
                                <div class="form-control-plaintext">{{ $student->registered_at->format('d F Y') }}</div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('registered_school_year_id')) has-error @endif">
                            <label class="form-label col-md-2">Terdaftar Pada Tahun Ajaran</label>
                            <div class="col-md-3">
                                <div class="form-control-plaintext">{{ $student->registered_school_year->name }}</div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('registered_grade_id')) has-error @endif">
                            <label class="form-label col-md-2">Terdaftar pada Kelas</label>
                            <div class="col-md-3">
                                <div class="form-control-plaintext">{{ $student->registered_grade->name }}</div>
                            </div>
                        </div>
                        <div class="form-group row @if ($errors->has('old_school')) has-error @endif">
                            <label class="form-label col-md-2">Nama Sekolah Sebelumnya</label>
                            <div class="col-md-6">
                                <div class="form-control-plaintext">{{ $student->old_school }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group row">
            <div class="col-sm-10">
                <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-back"></i> Kembali</a>
            </div>
        </div>
    </div>
</div>
@endsection