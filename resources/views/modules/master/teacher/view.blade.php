@extends('templates.data.tabler.app')

@section('title')
Guru
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('teacher') }}
@endsection

@section('content')
<div class="row" id="baseapp">
	<div class="col-md-12">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="card-header">
				<div class="card-options">
					<a href="{{ route('teachers.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</a>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<table id="table" class="table table-hovered table-striped" width="100%">
							<thead class="thead-light">
								<tr>
									<th class="text-center">NUPTK</th>
									<th class="text-center">Name</th>
									<th class="text-center">Tgl Lahir</th>
									<th class="text-center">Terdaftar pada</th>
									<th class="text-center">Status</th>
									<th class="text-center">Dibuat</th>
									<th class="text-center">Diupdate</th>
									<th class="text-center" style="width: 150px;"></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
var vm = new Vue({
	el: '#baseapp',
	data: {
		table: null,
	},
	methods: {
		initDatatable: function () {
		    this.table = $('#table').DataTable({
		        processing: true,
		        serverSide: true,
		        responsive: true,
		        ajax: '{{ route('teachers.datatable') }}',
		        columns: [
		            { data: 'nuptk', name: 'nuptk', className: 'text-center' },
		            { data: 'employee.name', name: 'employee.name' },
		            { data: 'employee.birth_date', name: 'employee.birth_date', className: 'text-center' },
		            { data: 'employee.registered_at', name: 'employee.registered_at', className: 'text-center' },
		            { data: 'active', name: 'active', className: 'text-center' },
		            { data: 'created_at', name: 'created_at' },
		            { data: 'updated_at', name: 'updated_at', className: 'text-center' },
		            { data: 'action', name: 'action', className: 'text-center', sortable: false, searchable: false }
		        ],
		        language: { 
			       "processing": "<span class='text text-info'><b><i class='fa fa-spinner fa-spin'></i> Loading...</b></span>"
			    }
		    });
		}
	},
	mounted(){
		this.initDatatable();
	}
})
</script>
@endsection

