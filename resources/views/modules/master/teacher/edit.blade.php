@extends('templates.data.tabler.app')

@section('title')
Edit Guru
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('teacher edit', $teacher) }}
@endsection

@section('content')
<div class="row" id="baseapp">
    <form action="{{ route('teachers.update', ['id' => $teacher->id]) }}" method="POST" class="form-horizontal" role="form">
        <div class="col-md-12">
            <div class="card">
                <div class="card-status bg-blue"></div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            @csrf
                            @method('PUT')
                            <div class="form-group row @if ($errors->has('nuptk')) has-error @endif">
                                <label class="form-label col-md-2">NUPTK</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="nuptk" placeholder="No. Induk" value="{{ $teacher->nuptk }}" required>
                                    @if ($errors->has('nuptk')) <div class="invalid-feedback">{{ $errors->first('nuptk') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('code')) has-error @endif">
                                <label class="form-label col-md-2">Kode</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="code" placeholder="Kode Guru" value="{{ $teacher->code }}" required>
                                    @if ($errors->has('code')) <div class="invalid-feedback">{{ $errors->first('code') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('employee_id')) has-error @endif">
                                <label class="form-label col-md-2">Pegawai</label>
                                <div class="col-md-6">
                                    <select class="form-control">
                                        <option value="">Pilih pegawai...</option>
                                        <option></option>
                                    </select>
                                    @if ($errors->has('employee_id')) <div class="invalid-feedback">{{ $errors->first('employee_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary" dusk="btn-update"><i class="fa fa-save"></i> Simpan</button>
                                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('scripts')
<script>
    const vm = new Vue({
        el: "#baseapp",
        data: {  
        },
        watch: { 
        },
        methods: {
        },
        mounted () {
        }
    });
</script>
@endsection