@extends('templates.data.tabler.app')

@section('title')
Tambah Guru
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('teacher create') }}
@endsection

@section('content')
<div id="baseapp">
    <form action="{{ route('teachers.store') }}" method="POST" class="form-horizontal" role="form">
        <div class="col-md-12">
            <div class="card">
                <div class="card-status bg-blue"></div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            @csrf
                            <div class="form-group row @if ($errors->has('nuptk')) has-error @endif">
                                <label class="form-label col-md-2">NUPTK</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="nuptk" placeholder="NUPTK" value="{{ old('nuptk') }}" required>
                                    @if ($errors->has('nuptk')) <div class="invalid-feedback">{{ $errors->first('nuptk') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('code')) has-error @endif">
                                <label class="form-label col-md-2">Kode</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="code" placeholder="Kode Guru" value="{{ old('code') }}" required>
                                    @if ($errors->has('code')) <div class="invalid-feedback">{{ $errors->first('code') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('employee_id')) has-error @endif">
                                <label class="form-label col-md-2">Pegawai</label>
                                <div class="col-md-6">
                                    <select class="form-control selectize" name="employee_id" required>
                                        <option value="">Pilih pegawai...</option>
                                        @foreach ($employees as $employee)
                                        <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('employee_id')) <div class="invalid-feedback">{{ $errors->first('employee_id') }}</div> @endif
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary" dusk="btn-store"><i class="fa fa-save"></i> Simpan</button>
                                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection

@section('scripts')
<script>
    const vm = new Vue({
        el: "#baseapp",
        data: {
        },
        watch: {
        },
        methods: {
        },
        mounted () {
            $('.selectize').selectize({
                create: true,
                sortField: 'text'
            });
        }
    });
</script>
@endsection