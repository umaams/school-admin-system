@extends('templates.data.tabler.app')

@section('title')
Tambah User
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('user create') }}
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
            <div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('users.store') }}" method="POST" class="form-horizontal" role="form">
                            @csrf
                            <div class="form-group row @if ($errors->has('name')) has-error @endif">
                                <label class="form-label col-md-2">Nama</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" placeholder="Nama Lengkap" required>
                                    @if ($errors->has('name')) <div class="invalid-feedback">{{ $errors->first('name') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('email')) has-error @endif">
                                <label class="form-label col-md-2">E-mail</label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" placeholder="Alamat E-mail" required>
                                    @if ($errors->has('email')) <div class="invalid-feedback">{{ $errors->first('email') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('role_name')) has-error @endif">
                                <label class="form-label col-md-2">Type User</label>
                                <div class="col-md-3">
                                    <select class="form-control" name="role_name">
                                        <option value="">Pilih Type...</option>
                                        @foreach ($roles as $role)
                                        <option value="{{ $role->name }}">{{ $role->description }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('role_name')) <div class="invalid-feedback">{{ $errors->first('role_name') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('password')) has-error @endif">
                                <label class="form-label col-md-2">Password</label>
                                <div class="col-md-4">
                                    <input type="password" class="form-control" name="password" placeholder="Password min. 6 karakter" required>
                                    @if ($errors->has('password')) <div class="invalid-feedback">{{ $errors->first('password') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('password_confirmation')) has-error @endif">
                                <label class="form-label col-md-2">Konfirmasi Password</label>
                                <div class="col-md-4">
                                    <input type="password" class="form-control" name="password_confirmation" placeholder="Password min. 6 karakter" required>
                                    @if ($errors->has('password_confirmation')) <div class="invalid-feedback">{{ $errors->first('password_confirmation') }}</div> @endif
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection