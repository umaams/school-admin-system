@extends('templates.data.tabler.app')

@section('title')
Edit Absensi Siswa
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('attendance edit', $kelas) }}
@endsection

@section('content')
<form action="{{ route('attendances.update') }}" method="POST" class="form-horizontal" role="form">
@csrf
<div id="baseapp" class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-2">
						<label class="form-label">Tanggal</label>
						<div class="input-group date">
							<input type="text" class="form-control" v-model="att_date">
							<div class="input-group-append"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
						</div>
                    </div>
                    <div class="col-md-3">
                        <label class="form-label">Kelas</label>
                        <select class="form-control selectize" v-model="class_id">
                            <option value="">Pilih Kelas...</option>
                            @foreach ($classes as $class)
                            <option value="{{ $class->id }}">Kelas {{ $class->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label class="form-label" style="width:100%; visibility: hidden;">Action</label>
                        <button type="button" class="btn btn-primary" v-on:click="editAttendance"><i class="fa fa-edit"></i> Edit Absensi</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="ibox-title">
				<h5>
					Absensi Kelas: {{ $kelas->name }} - Tanggal: {{ date('d F Y', strtotime($att_date)) }}
				</h5>
			</div>
			<div class="card-body">
				<input type="hidden" name="class_id" value="{{ $class_id }}"/>
				<input type="hidden" name="att_date" value="{{ $att_date }}"/>
				<div class="row">
					<div class="col-md-12">
                        <table class="table table-striped table-hovered">
							<thead class="thead-light">
								<tr>
									<th class="text-center" style="width: 50px;">No</th>
									<th class="text-center">NIS</th>
									<th class="text-center">Name</th>
									<th class="text-center">JK</th>
									<th class="text-center"></th>
								</tr>
							</thead>
							<tbody>
								@foreach ($kelas->students as $i => $student)
								<tr>
									<td class="text-center">{{ ($i+1) }}</td>
									<td class="text-center">
										<input type="hidden" name="student_id[]" value="{{ $student->id }}"/>
										{{ $student->nis }}
									</td>
									<td>{{ $student->name }}</td>
									<td class="text-center">
									@if ($student->sex == 'M')
									LK
									@else
									PR
									@endif
									</td>
									<td class="text-center">
										<div class="checkbox">
											<label>
												<input type="radio" name="status[{{$student->id}}]" value="0" @if ($student->attendances->count() == 0) checked @endif/>
												Tidak Hadir
											</label>
											<label>
												<input type="radio" name="status[{{$student->id}}]" value="1" @if ($student->attendances->count() > 0) checked @endif/>
												Hadir
											</label>
										</div>
									</td>
								</tr>
								@endforeach
								@if ($kelas->students->count() == 0)
								<tr>
									<td class="text-center" colspan="5">Tidak ada data</td>
								</tr>
								@endif
							</tbody>
						</table>
                    </div>
				</div>
				@if ($kelas->students->count() > 0)
				<div class="row">
					<div class="col-md-12">
						<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
					</div>
				</div>
				@endif
			</div>
		</div>
	</div>
</div>
</form>
@endsection

@section('scripts')
<script>
var vm = new Vue({
	el: '#baseapp',
	data: {
		att_date: '{{$att_date}}',
		class_id: '{{$class_id}}',
		disableDays: []
	},
	methods: {
		getEvents: function () {
			const $this = this;
			axios.get('{{ route('events.show.school_year.active') }}', { params: { isActiveDay: 0 } }).then(function (response) {
				for (let i = 0; i < response.data.events.length; i++) {
					event = response.data.events[i];
					let date_start = moment(event.date_start);
					let date_end = moment(event.date_end);
					while (date_start.isSame(date_end)) {
						$this.disableDays.push(date_start);
						date_start.add(1, 'days');
					}
				}
				$('.date').data("DateTimePicker").destroy();
				$('.date').datetimepicker({
					locale: 'id',
					format: 'YYYY-MM-DD',
					minDate: moment('{{$calendar->semester_1_date_start}}'),
					maxDate: moment('{{$calendar->semester_2_date_end}}'),
					disabledDates: $this.disableDays
				});
			}).catch(function (error) {
				console.log(error);
			});
		},
		editAttendance: function () {
			window.location = '{{ route('attendances.edit') }}?att_date=' + moment(this.att_date, 'DD-MM-YYYY').format('YYYY-MM-DD') + '&class_id=' + this.class_id;
		}
	},
	mounted(){
		$('.date').datetimepicker({
            locale: 'id',
            format: 'YYYY-MM-DD',
			minDate: moment('{{$calendar->semester_1_date_start}}'),
			maxDate: moment('{{$calendar->semester_2_date_end}}')
		});
		let $this = this;
		$('.date').on('change.datetimepicker', function(e){ 
			$this.att_date = e.date.format('DD-MM-YYYY');
		});
		$('.selectize').selectize({
			create: true,
			sortField: 'text'
		});
		this.getEvents();
	}
})
</script>
@endsection