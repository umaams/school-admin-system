@extends('templates.data.tabler.app')

@section('title')
Edit Ijin Siswa
@endsection

@section('styles')
<style>
.select2-selection__rendered {
	font-size: 14px;
}
.select2-container--default .select2-selection--single {
    border-radius: 0px !important;
}
.select2-container .select2-selection--single {
    height: 34px;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 34px;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
    height: 34px;
}
</style>
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('student_permit edit', $permit) }}
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
            <div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('student_permits.update', $permit->id) }}" method="POST" class="form-horizontal" role="form">
                            @csrf
                            @method('PUT')
                            <div class="form-group row @if ($errors->has('student_id')) has-error @endif">
                                <label class="form-label col-md-2">Nama</label>
                                <div class="col-md-6">
                                    <select name="student_id" id="student_id" class="form-control" required>
                                        <option value="{{$permit->student_id}}" selected="selected">[{{$permit->student->nis}}] {{$permit->student->name}}</option>
                                    </select>
                                    @if ($errors->has('student_id')) <div class="invalid-feedback">{{ $errors->first('student_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('permit_category_id')) has-error @endif">
                                <label class="form-label col-md-2">Jenis Ijin</label>
                                <div class="col-md-3">
                                    <select class="form-control" name="permit_category_id">
                                        <option value="">Pilih Kategori...</option>
                                        @foreach ($permit_categories as $permit_category)
                                        <option value="{{ $permit_category->id }}" @if ($permit->permit_category_id == $permit_category->id) selected @endif>{{ $permit_category->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('permit_category_id')) <div class="invalid-feedback">{{ $errors->first('permit_category_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('date_start') || $errors->has('date_end')) has-error @endif">
                                <label class="form-label col-md-2">Tanggal Ijin</label>
                                <div class="col-md-2">
                                    <div class="input-group date" id="date_start" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" name="date_start" placeholder="Tanggal Mulai" value="{{$permit->date_start}}" data-target="#date_start" required>
                                        <div class="input-group-append" data-target="#date_start" data-toggle="datetimepicker"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                    </div>
                                    @if ($errors->has('date_start')) <div class="invalid-feedback">{{ $errors->first('date_start') }}</div> @endif
                                </div>
                                <div class="col-md-2">
                                    <div class="input-group date" id="date_end" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" name="date_end" placeholder="Tanggal Selesai" value="{{$permit->date_end}}" data-target="#date_end" required>
                                        <div class="input-group-append" data-target="#date_end" data-toggle="datetimepicker"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                    </div>
                                    @if ($errors->has('date_end')) <div class="invalid-feedback">{{ $errors->first('date_end') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('description')) has-error @endif">
                                <label class="form-label col-md-2">Deskripsi</label>
                                <div class="col-md-6">
                                    <textarea name="description" class="form-control">{{$permit->description}}</textarea>
                                    @if ($errors->has('description')) <div class="invalid-feedback">{{ $errors->first('description') }}</div> @endif
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
var vm = new Vue({
    el: '#baseapp',
    data: {
        table: null,
    },
    methods: {
    },
    mounted(){
        $('#date_start').datetimepicker({
            locale: 'id',
            format: 'YYYY-MM-DD'
        });
        $('#date_end').datetimepicker({
            locale: 'id',
            format: 'YYYY-MM-DD',
            useCurrent: false
        });
        $("#date_start").on("change.datetimepicker", function (e) {
            $('#date_end').data("DateTimePicker").minDate(e.date);
        });
        $("#date_end").on("change.datetimepicker", function (e) {
            $('#date_start').data("DateTimePicker").maxDate(e.date);
        });
        $('#student_id').select2({
            ajax: {
                url: '{{ route('students.hasclass') }}',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                  var query = {
                    search: params.term,
                    page: params.page || 1
                  }
                  return query;
                },
                processResults: function (data) {
                    return {
                        results: data.data.students.map(function (item, index) {
                            let data = {};
                            data.id = item.id;
                            data.text = "[" + item.nis + "] " + item.name;
                            return data;
                        })
                    };
                }
            }
        });
    }
})
</script>
@endsection

