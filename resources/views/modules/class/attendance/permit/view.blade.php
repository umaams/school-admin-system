@extends('templates.data.tabler.app')

@section('title')
Ijin Siswa
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('student_permit') }}
@endsection

@section('content')
<div class="row" id="baseapp">
	<div class="col-md-12">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="card-header">
				<div class="card-options">
					<a href="{{ route('student_permits.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</a>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<table id="table" class="table table-hovered table-striped" width="100%">
							<thead class="thead-light">
								<tr>
									<th class="text-center">Nama Siswa</th>
									<th class="text-center">Kategori</th>
									<th class="text-center">Tanggal Mulai</th>
									<th class="text-center">Tanggal Selesai</th>
									<th class="text-center">Dibuat</th>
									<th class="text-center">Diupdate</th>
									<th class="text-center"></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
var vm = new Vue({
	el: '#baseapp',
	data: {
		table: null,
	},
	methods: {
		initDatatable: function () {
		    this.table = $('#table').DataTable({
		        processing: true,
		        serverSide: true,
		        responsive: true,
		        ajax: '{{ route('student_permits.datatable') }}',
		        columns: [
		            { data: 'student.name', name: 'student.name' },
		            { data: 'permit_category.name', name: 'permit_category.name', className: 'text-center' },
		            { data: 'date_start', name: 'date_start', className: 'text-center' },
		            { data: 'date_end', name: 'date_end', className: 'text-center' },
		            { data: 'created_at', name: 'created_at' },
		            { data: 'updated_at', name: 'updated_at', className: 'text-center' },
		            { data: 'action', name: 'action', className: 'text-center', sortable: false, searchable: false }
		        ],
		        language: { 
			       "processing": "<span class='text text-info'><b><i class='fa fa-spinner fa-spin'></i> Loading...</b></span>"
			    }
		    });
		}
	},
	mounted(){
		this.initDatatable();
	}
})
</script>
@endsection