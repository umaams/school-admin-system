@extends('templates.data.tabler.app')

@section('title')
Edit Absensi Siswa
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('attendance') }}
@endsection

@section('content')
<div id="attendance-view" class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-2">
						<label class="form-label">Tanggal</label>
						<div class="input-group date" id="date" data-target-input="nearest">
							<input type="text" class="form-control datetimepicker-input" data-target="#date" v-model="att_date">
							<div class="input-group-append" data-target="#date" data-toggle="datetimepicker"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
						</div>
                    </div>
                    <div class="col-md-3">
                        <label class="form-label">Kelas</label>
                        <select class="form-control selectize" v-model="class_id">
                            <option value="">Pilih Kelas...</option>
                            <option v-for="kelas in classes" v-bind:value="kelas.id">Kelas @{{ kelas.name }}</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label class="form-label" style="width:100%; visibility: hidden;">Action</label>
                        <button type="button" class="btn btn-primary" v-on:click="editAttendance"><i class="fa fa-eye"></i> Tampilkan</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12" v-if="kelas != null">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="card-header">Absensi Kelas: @{{ kelas.name }} - Tanggal: @{{ selected.att_date | dateFormat }}</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
                        <div class="table-responsive">
                        	<table class="table table-striped table-hovered">
								<thead class="thead-light">
									<tr>
										<th class="text-center" style="width: 50px;">No</th>
										<th class="text-center">NIS</th>
										<th class="text-center">Name</th>
										<th class="text-center">JK</th>
										<th class="text-center"></th>
										<th class="text-center"></th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="(student, i) in kelas.students">
										<td class="text-center">@{{ (i+1) }}</td>
										<td class="text-center">
											@{{ student.nis }}
										</td>
										<td>@{{ student.name }}</td>
										<td class="text-center">
											<template v-if="student.sex == 'M'">LK</template>
											<template v-if="student.sex == 'F'">PR</template>
										</td>
										<td class="text-center">
											<div class="form-check form-check-inline">
											  <input class="form-check-input" type="radio" :name="'status-'+i" value="0" v-model="student.att_status">
											  <label class="form-check-label">Tidak Hadir</label>
											</div>
											<div class="form-check form-check-inline">
											  <input class="form-check-input" type="radio" :name="'status-'+i" value="1" v-model="student.att_status">
											  <label class="form-check-label">Hadir</label>
											</div>
										</td>
										<td class="text-center">
											<span class="label-info" v-if="student.student_permits.length > 0">
												<button class="btn btn-sm btn-outline-success" v-on:click="editPermit(student)"><i class="fa fa-info"></i> @{{ student.student_permits[0].permit_category.name }}</button>
											</span>
											<button v-if="student.student_permits.length == 0" class="btn btn-sm btn-success" v-on:click="createPermit(student)"><i class="fa fa-plus-circle"></i> Buat Ijin</button>
										</td>
									</tr>
									<tr v-if="kelas.students.length == 0">
										<td class="text-center" colspan="6">Tidak ada data</td>
									</tr>
								</tbody>
							</table>
                        </div>
                    </div>
				</div>
				<div class="row" v-if="kelas.students.length > 0">
					<div class="col-md-12">
						<button type="button" class="btn btn-primary" v-on:click="updateAttendance"><i class="fa fa-save"></i> Simpan Perubahan</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-create" tabindex="-1" role="dialog">
	  	<div class="modal-dialog" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
			        <h5 class="modal-title">Buat Ijin</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			    </div>
		      	<div class="modal-body" v-if="selected.student != null">
		        	<form>
		        		<div class="form-group">
			            	<label for="message-text" class="form-label">Nama Siswa</label><br>
			            	<label for="message-text" class="form-label"><b>[@{{ selected.student.nis }}] @{{ selected.student.name }}</b></label>
			          	</div>
			          	<div class="form-group row">
			            	<label for="message-text" class="form-label col-md-12">Tanggal Ijin</label><br>
			            	<div class="col-md-6">
                                <div class="input-group date" id="create-date-start" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="create-date-start" placeholder="Tanggal Mulai" v-model="form_permit.date_start" data-target="#create-date-start" required>
                                    <div class="input-group-append" data-target="#create-date-start" data-toggle="datetimepicker"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group date" id="create-date-end" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="create-date-end" placeholder="Tanggal Selesai" v-model="form_permit.date_end" data-target="#create-date-end" required>
                                    <div class="input-group-append" data-target="#create-date-end" data-toggle="datetimepicker"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                </div>
                            </div>
			          	</div>
		        		<div class="form-group">
			            	<label for="message-text" class="form-label">Jenis Ijin</label>
			            	<select class="form-control" v-model="form_permit.permit_category_id" required>
			            		<option value="">Pilih Jenis...</option>
			            		<option v-for="permit_category in permit_categories" v-bind:value="permit_category.id">@{{ permit_category.name }}</option>
			            	</select>
			          	</div>
			          	<div class="form-group">
			            	<label for="message-text" class="form-label">Keterangan</label>
			            	<textarea class="form-control" v-model="form_permit.description"></textarea>
			          	</div>
			        </form>
		      	</div>
		      	<div class="modal-footer">
		        	<button type="button" class="btn btn-primary" v-on:click="storePermit"><i class="fa fa-save"></i> Simpan</button>
		      	</div>
		    </div>
		</div>
	</div>
	<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog">
	  	<div class="modal-dialog" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
			        <h5 class="modal-title">Edit Ijin</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			    </div>
		      	<div class="modal-body" v-if="selected.student != null">
		        	<form>
		        		<div class="form-group">
			            	<label for="message-text" class="form-label">Nama Siswa</label><br>
			            	<label for="message-text" class="form-label"><b>[@{{ selected.student.nis }}] @{{ selected.student.name }}</b></label>
			          	</div>
			          	<div class="form-group row">
			            	<label for="message-text" class="form-label col-md-12">Tanggal Ijin</label><br>
			            	<div class="col-md-6">
                                <div class="input-group date" id="edit-date-start" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="edit-date-start" placeholder="Tanggal Mulai" v-model="form_permit.date_start" data-target="#edit-date-start" required>
                                    <div class="input-group-append" data-target="#edit-date-start" data-toggle="datetimepicker"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group date" id="edit-date-end" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="edit-date-end" placeholder="Tanggal Selesai" v-model="form_permit.date_end" data-target="#edit-date-end" required>
                                    <div class="input-group-append" data-target="#edit-date-end" data-toggle="datetimepicker"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                </div>
                            </div>
			          	</div>
		        		<div class="form-group">
			            	<label for="message-text" class="form-label">Jenis Ijin</label>
			            	<select class="form-control" v-model="form_permit.permit_category_id" required>
			            		<option value="">Pilih Jenis...</option>
			            		<option v-for="permit_category in permit_categories" v-bind:value="permit_category.id">@{{ permit_category.name }}</option>
			            	</select>
			          	</div>
			          	<div class="form-group">
			            	<label for="message-text" class="form-label">Keterangan</label>
			            	<textarea class="form-control" v-model="form_permit.description"></textarea>
			          	</div>
			        </form>
		      	</div>
		      	<div class="modal-footer">
					<button type="button" class="btn btn-danger" v-on:click="deletePermit"><i class="fas fa-trash"></i> Hapus</button>
		        	<button type="button" class="btn btn-primary" v-on:click="updatePermit"><i class="fa fa-save"></i> Simpan</button>
		      	</div>
		    </div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
var vm = new Vue({
	el: '#attendance-view',
	data: {
		att_date: moment().format('YYYY-MM-DD'),
		class_id: '',
		disableDays: [],
		permit_categories: [],
		classes: [],
		kelas: null,
		selected: {
			att_date: null,
			student: null
		},
		form_permit: {
			student_id: '',
			date_start: '',
			date_end: '',
			permit_category_id: '',
			description: ''
		}
	},
	methods: {
		getClasses: function () {
			this.classes = [];
			axios.get('{{ route('classes.active') }}').then(response => {
				this.classes = response.data.classes;
			});
		},
		getPermitCategories: function () {
			this.permit_categories = [];
			axios.get('{{ route('permit_categories.index') }}').then(response => {
				this.permit_categories = response.data.permit_categories;
			});
		},
		getEvents: function () {
			this.disableDays = [];
			axios.get('{{ route('events.show.school_year.active') }}', { params: { isActiveDay: 0 } }).then(response => {
				for (let i = 0; i < response.data.events.length; i++) {
					const event = response.data.events[i];
					let date_start = moment(event.date_start);
					let date_end = moment(event.date_end);
					while (date_start.isSame(date_end)) {
						this.disableDays.push(date_start);
						date_start.add(1, 'days');
					}
				}
				$('#date').data("DateTimePicker").destroy();
				$('#date').datetimepicker({
					locale: 'id',
					format: 'YYYY-MM-DD',
					disabledDates: this.disableDays
				});
			});
		},
		editAttendance: function () {
			this.kelas = null;
			const temp = this.att_date;
			this.selected.att_date = temp;
			const params = {};
			params.class_id = this.class_id;
			params.att_date = this.att_date;
			axios.get('{{ route('attendances.show') }}', { params: params }).then(response => {
				this.kelas = response.data.kelas;
			}).then(e => {
				this.kelas.students = this.kelas.students.map(function (student, index) {
					const status = student.attendances.length > 0 ? '1' : '0';
					Vue.set(student, 'att_status', status);
					return student;
				})
			});
		},
		updateAttendance: function () {
			const formdata = {};
			formdata.att_date = this.selected.att_date;
			formdata.students = [];
			for (let i= 0; i < this.kelas.students.length; i++) {
				formdata.students.push({
					student_id: this.kelas.students[i].id,
					att_status: this.kelas.students[i].att_status
				});
			}
			axios.post('{{ route('attendances.update') }}', formdata).then(response => {
				alert('Penyimpanan berhasil');
			});
		},
		createPermit: function (student) {
			this.selected.student = student;
			this.form_permit.student_id = this.selected.student.id;
			this.form_permit.date_start = this.selected.att_date;
			this.form_permit.date_end = this.selected.att_date;
			this.form_permit.permit_category_id = '';
			this.form_permit.description = '';
			$('#modal-create').modal('show');
		},
		storePermit: function () {
			const formdata = this.form_permit;
			axios.post('{{ route('student_permits.store') }}', formdata).then(response => {
				$('#modal-create').modal('hide');
				this.editAttendance();
			});
		},
		editPermit: function (student) {
			this.selected.student = student;
			const formdata = this.form_permit;
			let url = '{{ route('student_permits.edit', ['id' => '#']) }}';
			url = url.replace('#', this.selected.student.student_permits[0].id);
			axios.get(url).then(response => {
				this.form_permit.student_id = response.data.student_permit.student.id;
				this.form_permit.date_start = moment(response.data.student_permit.date_start).format('YYYY-MM-DD');
				this.form_permit.date_end = moment(response.data.student_permit.date_end).format('YYYY-MM-DD');
				this.form_permit.permit_category_id = response.data.student_permit.permit_category_id;
				this.form_permit.description = response.data.student_permit.description;
				$('#modal-edit').modal('show');
			});
		},
		updatePermit: function () {
			const formdata = this.form_permit;
			let url = '{{ route('student_permits.update', ['id' => '#']) }}';
			url = url.replace('#', this.selected.student.student_permits[0].id);
			axios.put(url, formdata).then(response => {
				$('#modal-edit').modal('hide');
				this.editAttendance();
			});
		},
		deletePermit: function () {
			let url = '{{ route('student_permits.destroy', ['id' => '#']) }}';
			url = url.replace('#', this.selected.student.student_permits[0].id);
			axios.delete(url).then(response => {
				$('#modal-edit').modal('hide');
				this.editAttendance();
			});
		}
	},
	mounted(){
		$('#date').datetimepicker({
            locale: 'id',
            format: 'YYYY-MM-DD',
		});
		$('#date').on('change.datetimepicker', e => { 
			this.att_date = e.date.format('YYYY-MM-DD');
		});
		$('#create-date-start').datetimepicker({
            locale: 'id',
            format: 'YYYY-MM-DD',
		});
		$('#create-date-start').on('change.datetimepicker', e => { 
			this.form_permit.date_start = e.date.format('YYYY-MM-DD');
		});
		$('#create-date-end').datetimepicker({
            locale: 'id',
            format: 'YYYY-MM-DD',
		});
		$('#create-date-end').on('change.datetimepicker', e => { 
			this.form_permit.date_end = e.date.format('YYYY-MM-DD');
		});
		$('#edit-date-start').datetimepicker({
            locale: 'id',
            format: 'YYYY-MM-DD',
		});
		$('#edit-date-start').on('change.datetimepicker', e => { 
			this.form_permit.date_start = e.date.format('YYYY-MM-DD');
		});
		$('#edit-date-end').datetimepicker({
            locale: 'id',
            format: 'YYYY-MM-DD',
		});
		$('#edit-date-end').on('change.datetimepicker', e => { 
			this.form_permit.date_end = e.date.format('YYYY-MM-DD');
		});
		this.getClasses();
		this.getEvents();
		this.getPermitCategories();
	}
})
</script>
@endsection