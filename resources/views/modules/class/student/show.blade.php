@extends('templates.data.tabler.app')

@section('title')
Detail Rombongan Kelas
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('class student show', $class) }}
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="card-header">
				<div class="card-options">
					<a class="btn btn-primary" href="{{ route('classes.students.add', ['id' => $class->id]) }}"><i class="fa fa-plus-circle"></i> Tambah</a>
					<a href="{{ route('classes.students.index') }}" class="btn btn-default"><i class="fa fa-back"></i> Kembali</a>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<table id="table" class="table table-hovered table-striped" width="100%">
							<thead class="thead-light">
								<tr>
									<th class="text-center">NIS</th>
									<th class="text-center">Nama Siswa</th>
									<th class="text-center">JK</th>
									<th class="text-center">Desa</th>
									<th class="text-center">Nama Wali Murid</th>
									<th class="text-center" style="width: 150px;"></th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
var vm = new Vue({
	el: '#baseapp',
	data: {
		table: null,
	},
	methods: {
		initDatatable: function () {
		    this.table = $('#table').DataTable({
				responsive: true,
				processing: true,
		        serverSide: true,
		        paging: false,
		        order: [[ 1, "asc" ]],
		        ajax: '{{ route('classes.students.show.datatable', ['id' => $class->id]) }}',
		        columns: [
		            { data: 'nis', name: 'nis', className: 'text-center' },
		            { data: 'name', name: 'name' },
		            { data: 'sex', name: 'sex', className: 'text-center' },
		            { data: 'village.name', name: 'village.name', className: 'text-left' },
		            { data: 'guardian_name', name: 'guardian_name', className: 'text-left' },
		            { data: 'action', name: 'action', className: 'text-center', sortable: false, searchable: false }
		        ],
		        language: { 
			       "processing": "<span class='text text-info'><b><i class='fa fa-spinner fa-spin'></i> Loading...</b></span>"
			    }
		    });
		}
	},
	mounted(){
		this.initDatatable();
	}
})
</script>
@endsection

