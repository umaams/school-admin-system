@extends('templates.data.tabler.app')

@section('title')
Tambah Rombongan Kelas
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('class student add', $class) }}
@endsection

@section('content')
<div class="row" id="baseapp">
	<div class="col-md-12">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<form action="{{ route('classes.students.store', ['id' => $class->id]) }}" method="POST" class="form-horizontal" role="form">
							 @csrf
							 @method('PUT')
							 <div class="form-group row">
							 	<div class="col-md-3">
							 		<select class="form-control" v-model="filter.school_year_id">
							 			<option value="">Semua Data</option>
							 			@foreach($school_years as $school_year)
							 			<option value="{{ $school_year->id }}">Data Tahun {{ $school_year->name }}</option>
							 			@endforeach
							 		</select>
							 	</div>
							 </div>
							<div class="form-group row">
								<div class="col-md-12">
									<table id="table" class="table table-hovered table-striped" width="100%">
										<thead class="thead-light">
											<tr>
												<th class="text-center"><input type="checkbox"></th>
												<th class="text-center">NIS</th>
												<th class="text-center">Nama Siswa</th>
												<th class="text-center">JK</th>
												<th class="text-center">Desa</th>
												<th class="text-center">Wali Murid</th>
												<th class="text-center">Tahun Ajaran Masuk</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
							<hr>
							<div class="form-group row">
								<div class="col-md-12">
									<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Tambahkan</button>
                                    <a href="{{ route('classes.students.show', ['id' => $class->id]) }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
var vm = new Vue({
	el: '#baseapp',
	data: {
		table: null,
		checkall: [],
		classes: [],
		filter: {
			school_year_id: '',
			class_id: ''
		}
	},
	watch: {
		'filter.school_year_id': function (value) {
			this.table.draw();
		}
	},
	methods: {
		initDatatable: function () {
			const $this = this;
		    this.table = $('#table').DataTable({
				orderCellsTop: true,
				responsive: true,
				processing: true,
		        serverSide: true,
		        ajax: {
					url: '{{ route('students.hasclass.none.datatable') }}',
					data: function (d) {
						if ($this.filter.school_year_id != '') d.school_year_id = $this.filter.school_year_id;
					}
				},
		        columns: [
		            { data: 'action', name: 'action', className: 'text-center', sortable: false, searchable: false },
		            { data: 'nis', name: 'nis' },
		            { data: 'name', name: 'name' },
		            { data: 'sex', name: 'sex', className: 'text-center' },
		            { data: 'village.name', name: 'village.name', className: 'text-left' },
		            { data: 'guardian_name', name: 'guardian_name' },
		            { data: 'registered_school_year.name', name: 'registered_school_year.name', className: 'text-center' }
		        ],
		        language: { 
			       "processing": "<span class='text text-info'><b><i class='fa fa-spinner fa-spin'></i> Loading...</b></span>"
			    }
		    });
		}
	},
	mounted(){
		this.initDatatable();
	}
})
</script>
@endsection