@extends('templates.data.tabler.app')

@section('title')
Edit Wali Kelas
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('class guardian edit', $class) }}
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
            <div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('classes.guardians.update', $class->id) }}" method="POST" class="form-horizontal" role="form">
                            @csrf
                            @method('PUT')
                            <div class="form-group row @if ($errors->has('employee_id')) has-error @endif">
                                <label class="form-label col-md-2">Wali Kelas</label>
                                <div class="col-md-3">
                                    <select class="form-control" name="employee_id">
                                        <option value="">Pilih Wali Kelas...</option>
                                        @foreach ($employees as $employee)
                                        <option value="{{ $employee->id }}" @if (($class->guardian->count() > 0 ? $class->guardian[0]->id : null) == $employee->id) selected @endif>{{ $employee->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('employee_id')) <div class="invalid-feedback">{{ $errors->first('grade_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection

