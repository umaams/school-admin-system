@extends('templates.data.tabler.app')

@section('title')
Wali Kelas
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('class guardian') }}
@endsection

@section('content')
<div class="row">
	<div class="col-md-6">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table id="table" class="table table-hovered table-striped">
								<thead class="thead-light">
									<tr>
										<th class="text-center">Nama Kelas</th>
										<th class="text-center">Tingkat</th>
										<th class="text-center">Wali Kelas</th>
										<th class="text-center"></th>
									</tr>
								</thead>
								<tbody>
									@foreach($classes as $i => $class)
									<tr>
										<td class="text-left">Kelas {{ $class->name }}</td>
										<td class="text-center">{{ $class->grade->name }}</td>
										<td>{{ $class->guardian->count() > 0 ? $class->guardian[0]->name : '' }}</td>
										<td class="text-center">
											<a class="btn btn-primary btn-sm" href="{{ route('classes.guardians.edit', ['id' => $class->id]) }}"><i class="fa fa-edit"></i> Edit</a>
										</td>
									</tr>
									@endforeach
									@if ($classes->count() == 0)
									<tr>
										<td class="text-center" colspan="4">Tidak ada data</td>
									</tr>
									@endif
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
@endsection