@extends('templates.data.tabler.app')

@section('title')
Input Jurnal Mata Pelajaran
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('attendance') }}
@endsection

@section('content')
<div id="lesson-journal-view" class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-2">
						<label class="form-label">Tanggal</label>
						<div class="input-group date" id="date" data-target-input="nearest">
							<input type="text" class="form-control datetimepicker-input" data-target="#date" v-model="journal_date">
							<div class="input-group-append" data-target="#date" data-toggle="datetimepicker"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
						</div>
                    </div>
                    <div class="col-md-3">
                        <label class="form-label">Kelas</label>
                        <select class="form-control" v-model="class_id">
                            <option value="">Pilih Kelas...</option>
                            <option v-for="kelas in classes" v-bind:value="kelas.id">Kelas @{{ kelas.name }}</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label class="form-label" style="width:100%; visibility: hidden;">Action</label>
                        <button type="button" class="btn btn-primary" v-on:click="showLessonJournals"><i class="fa fa-eye"></i> Tampilkan</button>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="card" v-if="class_id != '' && classes.length > 0">
			<div class="card-status bg-blue"></div>
			<div class="card-header">Jurnal Mata Pelajaran Kelas: @{{ collect(classes).firstWhere('id', selected.class_id).name }} - Tanggal: @{{ selected.journal_date | dateFormat }}</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
                        <div class="table-responsive">
                        	<table class="table table-striped table-bordered table-hover">
								<thead class="thead-light">
									<tr>
										<th class="text-center">Jam</th>
										<th class="text-center">Jadwal</th>
										<th class="text-center">Deskripsi Jurnal</th>
										<th class="text-center"></th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="lesson_schedule in lesson_schedules">
										<td class="text-center">
											Jam Ke-@{{ lesson_schedule.school_time_id }}<br>
											@{{ lesson_schedule.school_time.time_start.substring(0, 5) }} - @{{ lesson_schedule.school_time.time_end.substring(0, 5) }}
										</td>
										<td>
											<small>Mata Pelajaran:</small><br>
											@{{ lesson_schedule.lesson.name }}<br><br>
											<small>Pengajar:</small><br>
											@{{ lesson_schedule.teacher.employee.name }}
										</td>
										<td v-if="lesson_schedule.lesson_journal != null">
											<textarea class="form-control description" rows="4" v-model="lesson_schedule.lesson_journal.description"></textarea>
										</td>
										<td class="text-center"><button class="btn btn-sm btn-success" v-on:click="updateLessonJournal(lesson_schedule)"><i class="fa fa-save"></i> Simpan</button></td>
									</tr>
									<tr v-if="lesson_schedules.length == 0">
										<td class="text-center" colspan="4">Tidak ada jadwal</td>
									</tr>
								</tbody>
							</table>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
var vm = new Vue({
	el: '#lesson-journal-view',
	data: {
		journal_date: moment().format('YYYY-MM-DD'),
		classes: [],
		class_id: '',
		disableDays: [],
		lesson_schedules: [],
		selected: {
			class_id: null,
			journal_date: null
		}
	},
	methods: {
		getClasses: function () {
			this.classes = [];
			axios.get('{{ route('classes.active') }}').then(response => {
				this.classes = response.data.classes;
			});
		},
		getEvents: function () {
			this.disableDays = [];
			axios.get('{{ route('events.show.school_year.active') }}', { params: { isActiveDay: 0 } }).then(response => {
				for (let i = 0; i < response.data.events.length; i++) {
					const event = response.data.events[i];
					let date_start = moment(event.date_start);
					let date_end = moment(event.date_end);
					while (date_start.isSame(date_end)) {
						this.disableDays.push(date_start);
						date_start.add(1, 'days');
					}
				}
				$('#date').data("DateTimePicker").destroy();
				$('#date').datetimepicker({
					locale: 'id',
					format: 'YYYY-MM-DD',
					disabledDates: this.disableDays
				});
			});
		},
		showLessonJournals: function () {
			this.lesson_schedules = [];
			const temp_journal_date = this.journal_date;
			const temp_class_id = this.class_id;
			this.selected.journal_date = temp_journal_date;
			this.selected.class_id = temp_class_id;
			const params = {};
			params.class_id = this.selected.class_id;
			params.journal_date = this.selected.journal_date;
			axios.get('{{ route('lesson_journals.show') }}', { params: params }).then(response => {
				this.lesson_schedules = response.data.lesson_schedules;
			}).then(e => {
				this.lesson_schedules = this.lesson_schedules.map(function (lesson_schedule) {
					if (lesson_schedule.lesson_journal == null) {
						lesson_schedule.lesson_journal = {
							description: ''
						};
					}
					return lesson_schedule;
				})
			}).then(e => {
				ClassicEditor.create(document.querySelector('.description'));
			});
		},
		updateLessonJournal: function (lesson_schedule) {
			const formdata = {};
			formdata.journal_date = this.selected.journal_date;
			formdata.lesson_schedule_id = lesson_schedule.id;
			formdata.description = lesson_schedule.lesson_journal.description;
			axios.post('{{ route('lesson_journals.update') }}', formdata).then(response => {
				alert('Penyimpanan berhasil');
			});
		}
	},
	mounted(){
		$('#date').datetimepicker({
            locale: 'id',
            format: 'YYYY-MM-DD',
		});
		$('#date').on('change.datetimepicker', e => {
			this.journal_date = e.date.format('YYYY-MM-DD');
		});
		this.getClasses();
		this.getEvents();
	}
})
</script>
@endsection