@extends('templates.data.tabler.app')

@section('title')
Report Absensi Siswa Per Kelas
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('report student') }}
@endsection

@section('content')
<div class="row" id="baseapp">
	<div class="col-md-12">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-2">
						<label class="form-label">Tahun Ajaran</label>
						<select class="form-control" v-model="filter.school_year_id">
							<option v-if="loading.school_year" value="">Loading...</option>
							<option v-for="item in school_years" v-bind:value="item.id">@{{ item.name }}</option>
						</select>
                    </div>
                    <div class="col-md-2">
						<label class="form-label">Kelas</label>
						<select class="form-control" v-model="filter.class_id">
							<option v-if="loading.classes" value="">Loading...</option>
							<option v-for="item in classes" v-bind:value="item.id">Kelas @{{ item.name }}</option>
						</select>
                    </div>
                    <div class="col-md-2">
						<label class="form-label">Bulan</label>
						<div class='input-group month'>
							<input type="text" class="form-control" v-model="filter.month"/>
							<span class="input-group-append">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
					<div class="col-md-2">
						<label class="form-label">Urutkan berdasarkan</label>
						<select class="form-control" v-model="orderBy">
							<option value="nis">No. Induk</option>
							<option value="name">Nama</option>
						</select>
					</div>
					<div class="col-md-4">
                        <label class="form-label" style="width:100%; visibility: hidden;">Action</label>
						<button class="btn btn-primary" v-on:click="showReport"><i class="fa fa-eye"></i> Tampilkan</button>
					</div>
				</div>
				<hr>
				<div class="row" style="margin-top: 15px;">
					<div class="col-md-12" v-if="kelas != null">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead class="thead-light">
									<tr>
										<th class="text-center" rowspan="2" style="vertical-align: middle;">No.</th>
										<th class="text-center" rowspan="2" style="vertical-align: middle;">NIS</th>
										<th class="text-center" style="width: 250px; vertical-align: middle;" rowspan="2">Nama Lengkap</th>
										<th class="text-center" :colspan="dates.length">Tanggal Absensi</th>
										<th class="text-center" colspan="4">Total</th>
									</tr>
									<tr>
										<th v-for="date in dates" class="text-center">@{{ date }}</th>
										<th class="text-center">S</th>
										<th class="text-center">I</th>
										<th class="text-center">A</th>
										<th class="text-center" style="width: 100px;">% Kehadiran</th>
									</tr>
								</thead>
								<tbody>
									<tr v-if="kelas.students.length == 0 && !loading.report">
										<td class="text-center" colspan="9">Tidak ada data</td>
									</tr>
									<tr v-if="loading.report">
										<td class="text-center" colspan="9"><i class="fa fa-spin fa-spinner"></i> Loading Data...</td>
									</tr>
									<tr v-for="(student, i) in kelas.students">
										<td class="text-center">@{{ (i+1) }}</td>
										<td class="text-center">@{{ student.nis }}</td>
                                        <td>@{{ student.name }}</td>
                                        <td v-for="(attendance, j) in student.attendances" class="text-center" v-bind:style="attendance.active_day == 0 ? 'background-color: #bfbcbc;' : ''">
											<template v-if="attendance.present == 1"><i class="fa fa-check"></i></template>
											<template v-else-if="attendance.sick == 1">S</template>
											<template v-else-if="attendance.permit == 1">I</template>
											<template v-else-if="attendance.absence == 1">A</template>
										</td>
										<td class="text-center">@{{ student.total_sick }}</td>
										<td class="text-center">@{{ student.total_permit }}</td>
										<td class="text-center">@{{ student.total_absence }}</td>
										<td class="text-center">@{{ student.total_present / student.total_active_day * 100 | currencyFormat }}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
var vm = new Vue({
	el: '#baseapp',
	data: {
		school_years: [],
        classes: [],
        kelas: null,
		orderBy: 'nis',
		loading: {
			school_year: false,
            classes: false,
			report: false,
		},
		filter: {
			school_year_id: '',
            class_id: '',
            month: moment().format('MM-YYYY')
		}
	},
	watch: {
		school_years: function (value) {
			for (let i = 0; i < value.length; i++) {
				if (value[i].active == '1') {
					this.filter.school_year_id = value[i].id;
				}
			}
		},
        classes: function (value) {
			if (value.length > 0) {
                this.filter.class_id = value[0].id;
            }
		}
	},
	methods: {
		showSchoolYears: function () {
			this.school_years = [];
			this.loading.school_year = true;
			axios.get('{{ route('school_years.index') }}').then(response => {
				this.loading.school_year = false;
				this.school_years = response.data.data.school_years;
			}).catch((error) => {
				this.loading.school_year = false;
				console.log(error);
			});
		},
        showClasses: function () {
			this.classes = [];
			this.loading.classes = true;
			axios.get('{{ route('classes.index') }}').then(response => {
				this.loading.classes = false;
				this.classes = response.data.data.classes;
			}).catch((error) => {
				this.loading.school_year = false;
				console.log(error);
			});
		},
		showReport: function () {
			const params = {};
			params.orderBy = this.orderBy;
            params.class_id = this.filter.class_id;
			params.school_year_id = this.filter.school_year_id;
            params.month = moment(this.filter.month, 'MM-YYYY').format('MM');
            params.year = moment(this.filter.month, 'MM-YYYY').format('YYYY');
			this.kelas = null;
			this.loading.report = true;
			axios.get('{{ route('reports.student_attendances.class.show') }}', { params: params }).then(response => {
				this.loading.report = false;
				this.kelas = response.data.data['class'];
			}).catch((error) => {
				this.loading.report = false;
				console.log(error);
			}).then((e) => {
				$('table').dataTable({
					"ordering": false,
					"scrollX": true,
					"paging": false,
					"searching": false,
					"bInfo": false,
					"fixedColumns": {
						leftColumns: 3,
						rightColumns: 4,
					}
				});
			});
		}
	},
    computed: {
        dates: function () {
            let dates = [];
            const lastDay = moment(this.filter.month ,'MM-YYYY').endOf('month').format('D');
            for (let i = 1; i <= lastDay; i++) {
                dates.push(i);
            }
            return dates;
        }
    },
	mounted(){
		this.showSchoolYears();
        this.showClasses();
        $('.month').datetimepicker({
            locale: 'id',
            viewMode: 'years',
            format: 'MM-YYYY'
        });
        const $this = this;
        $('.month').on('change.datetimepicker', function(e){ 
            $this.filter.month = e.date.format('MM-YYYY');
        });
	}
})
</script>
@endsection