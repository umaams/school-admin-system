@extends('templates.data.tabler.app')

@section('title')
Report Rombongan Kelas
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('report student') }}
@endsection

@section('content')
<div class="row" id="baseapp">
	<div class="col-md-12">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-2">
						<div class="form-group row">
							<label class="form-label">Tahun Ajaran</label>
							<select class="form-control" v-model="filter.school_year_id">
								<option v-if="loading.school_year" value="">Loading...</option>
								<option v-for="item in school_years" v-bind:value="item.id">@{{ item.name }}</option>
							</select>
						</div>
					</div>
					<div class="col-md-2">
						<div class="form-group row">
							<label class="form-label">Urutkan berdasarkan</label>
							<select class="form-control" v-model="orderBy">
								<option value="nis">No. Induk</option>
								<option value="name">Nama</option>
							</select>
						</div>
					</div>
					<div class="col-md-2">
						<label class="form-label" style="visibility: hidden; width: 100%;">Action</label>
						<button class="btn btn-primary" v-on:click="showReport"><i class="fa fa-eye"></i> Tampilkan</button>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-12">
						<ul class="nav nav-tabs" role="tablist">
							<template v-for="(grade, i) in grades">
								<li v-for="(kelas, j) in grade.classes" class="nav-item"><a v-bind:class="'nav-link ' + ((i == 0 && j == 0) ? 'active' : '')" data-toggle="tab" role="tab" v-bind:aria-controls="'#class-' + kelas.id" v-bind:href="'#class-' + kelas.id" aria-selected="false"> Kelas @{{ kelas.id }}</a></li>
							</template>
						</ul>
						<div class="tab-content">
							<template v-for="(grade, i) in grades">
								<div v-for="(kelas, j) in grade.classes" v-bind:id="'class-' + kelas.id" v-bind:class="((i == 0 && j == 0) ? 'tab-pane active' : 'tab-pane')">
									<div class="panel-body" style="margin-top: 15px;">
										<div class="row">
											<div class="col-md-12">
												<table>
													<tr>
														<td style="width: 100px;"><b>Nama Kelas</b></td>
														<td>: <b>@{{ kelas.name }}</b></td>
													</tr>
													<tr>
														<td><b>Wali Kelas</b></td>
														<td>: <b v-if="kelas.guardian.length > 0">@{{ kelas.guardian[0].name }}</b></td>
													</tr>
												</table>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="table-responsive">
													<table class="table table-bordered table-striped">
														<thead class="thead-light">
															<tr>
																<th class="text-center">No.</th>
																<th class="text-center">NIS</th>
																<th class="text-center">Nama Lengkap</th>
																<th class="text-center">JK</th>
																<th class="text-center">Tgl Lahir</th>
																<th class="text-center">Nama Wali Murid</th>
																<th class="text-center">Alamat Desa</th>
															</tr>
														</thead>
														<tbody>
															<tr v-for="(student, k) in kelas.students">
																<td class="text-center">@{{ (k+1) }}</td>
																<td class="text-center">@{{ student.nis }}</td>
																<td>@{{ student.name }}</td>
																<td class="text-center">
																	@{{ (student.sex == "M" ? "LK" : "PR") }}
																</td>
																<td class="text-center">@{{ student.birth_date | dateFormat }}</td>
																<td>@{{ student.guardian_name }}</td>
																<td>@{{ student.village.name }}</td>
															</tr>
															<tr v-if="kelas.students.length == 0">
																<td class="text-center" colspan="5">Tidak ada data</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</template>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
var vm = new Vue({
	el: '#baseapp',
	data: {
		school_years: [],
		grades: [],
		orderBy: 'nis',
		loading: {
			school_year: false,
			student: false,
		},
		filter: {
			school_year_id: ''
		}
	},
	watch: {
		school_years: function (value) {
			for (let i = 0; i < value.length; i++) {
				if (value[i].active == '1') {
					this.filter.school_year_id = value[i].id;
				}
			}
		}
	},
	methods: {
		showSchoolYears: function () {
			this.school_years = [];
			this.loading.school_year = true;
			axios.get('{{ route('school_years.index') }}').then(response => {
				this.loading.school_year = false;
				this.school_years = response.data.data.school_years;
			}).catch(function (error) {
				$this.loading.school_year = false;
				console.log(error);
			});
		},
		showReport: function () {
			const params = {};
			params.orderBy = this.orderBy;
			if (this.filter.school_year_id != '') params.school_year_id = this.filter.school_year_id;
			this.students = [];
			this.loading.student = true;
			axios.get('{{ route('reports.class_students.show') }}', { params: params }).then(response => {
				this.loading.student = false;
				this.grades = response.data.data.grades;
			}).catch(function (error) {
				$this.loading.student = false;
				console.log(error);
			});
		}
	},
	mounted(){
		this.showSchoolYears();
		this.showReport();
	}
})
</script>
@endsection