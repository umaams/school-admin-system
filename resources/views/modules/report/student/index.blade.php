@extends('templates.data.tabler.app')

@section('title')
Report Data Siswa
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('report student') }}
@endsection

@section('content')
<div class="row" id="baseapp">
	<div class="col-md-12">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-2">
						<div class="form-group row">
							<label class="form-label">Tahun Ajaran</label>
							<select class="form-control" v-model="filter.school_year_id">
								<option v-if="loading.school_year" value="">Loading...</option>
								<option v-for="item in school_years" v-bind:value="item.id">@{{ item.name }}</option>
							</select>
						</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-12">
						<div class="btn-group" role="group">
						  	<button type="button" class="btn btn-default" v-on:click="exportExcel">Export Xls</button>
						  	<button type="button" class="btn btn-default" v-on:click="exportPdf">Export Pdf</button>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table id="table" class="table table-bordered table-striped table-hover" width="100%;">
							<thead class="thead-light">
								<tr>
									<th class="text-center" style="vertical-align: middle;">NIS</th>
									<th class="text-center" style="vertical-align: middle;">Nama Lengkap</th>
									<th class="text-center" style="vertical-align: middle;">JK</th>
									<th class="text-center" style="vertical-align: middle;">Tempat Lahir</th>
									<th class="text-center" style="vertical-align: middle;">Tgl. Lahir</th>
									<th class="text-center" style="vertical-align: middle;">Agama</th>
									<th class="text-center" style="vertical-align: middle;">Alamat Desa</th>
									<th class="text-center" style="vertical-align: middle;">Nama Wali Murid</th>
									<th class="text-center" style="vertical-align: middle;">Tahun Masuk</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
var vm = new Vue({
	el: '#baseapp',
	data: {
		school_years: [],
		table: null,
		loading: {
			school_years: false
		},
		filter: {
			school_year_id: ''
		}
	},
	watch: {
		school_years: function (value) {
			for (let i = 0; i < value.length; i++) {
				if (value[i].active == '1') {
					this.filter.school_year_id = value[i].id;
				}
			}
		},
		'filter.school_year_id': function (value) {
			this.table.draw();
		}
	},
	methods: {
		getSchoolYears: function () {
			this.school_years = [];
			this.loading.school_years = true;
			axios.get('{{ route('school_years.index') }}').then(response => {
				this.loading.school_years = false;
				this.school_years = response.data.data.school_years;
			}).catch(function (error) {
				$this.loading.school_years = false;
			});
		},
		initDatatable: function () {
			const $this = this;
		    this.table = $('#table').DataTable({
				orderCellsTop: true,
				processing: true,
		        serverSide: true,
				searching: false,
				responsive: true,
				paging: false,
		        ajax: {
					url: '{{ route('reports.students.datatable') }}',
					data: function (d) {
						d.school_year_id = $this.filter.school_year_id;
					}
				},
		        columns: [
		            { data: 'nis', name: 'nis' },
		            { data: 'name', name: 'name' },
		            { data: 'sex', name: 'sex', className: 'text-center' },
		            { data: 'birth_place', name: 'birth_place' },
		            { data: 'birth_date', name: 'birth_date', className: 'text-center' },
		            { data: 'religion.name', name: 'religion.name' },
		            { data: 'village.name', name: 'village.name', className: 'text-left' },
		            { data: 'guardian_name', name: 'guardian_name' },
		            { data: 'registered_school_year.name', name: 'registered_school_year.name', className: 'text-center' }
		        ],
		        language: { 
			       "processing": "<span class='text text-info'><b><i class='fa fa-spinner fa-spin'></i> Loading...</b></span>"
			    }
		    });
		},
		exportPdf: function() {
			const url = '{{ route('reports.students.export.pdf') }}?school_year_id=' + this.filter.school_year_id;
			window.open(url);
		},
		exportExcel: function() {
			const url = '{{ route('reports.students.export.xls') }}?school_year_id=' + this.filter.school_year_id;
			window.open(url);
		}
	},
	mounted(){
		this.getSchoolYears();
		this.initDatatable();
	}
})
</script>
@endsection

