@extends('templates.data.tabler.app')

@section('title')
Pengaturan Umum
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('setting general') }}
@endsection

@section('content')
<form class="form form-horizontal" action="{{ route('settings.update') }}" method="post">
@csrf
@method('PUT')
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-status bg-blue"></div>
				<div class="card-header">Sekolah</div>
				<div class="card-body">
	                <div class="form-group row">
	                    <label class="form-label col-md-3">Nama Sekolah</label>
	                    <div class="col-md-5">
							<input type="text" class="form-control" placeholder="Nama Sekolah" name="settings[SCHOOL_NAME]"/>
						</div>
					</div>
					<div class="form-group row">
	                    <label class="form-label col-md-3">Alamat</label>
	                    <div class="col-md-5">
							<textarea class="form-control" placeholder="Alamat Sekolah" name="settings[SCHOOL_ADDRESS]"></textarea>
						</div>
					</div>
					<div class="form-group row">
	                    <label class="form-label col-md-3">Kode Pos</label>
	                    <div class="col-md-2">
							<input type="text" class="form-control" name="settings[SCHOOL_ZIPCODE]"/>
						</div>
					</div>
					<div class="form-group row">
	                    <label class="form-label col-md-3">No. Telepon</label>
	                    <div class="col-md-3">
							<input type="text" class="form-control" name="settings[SCHOOL_PHONE]"/>
						</div>
					</div>
					<div class="form-group row">
	                    <label class="form-label col-md-3">E-mail</label>
	                    <div class="col-md-3">
							<input type="text" class="form-control" name="settings[SCHOOL_EMAIL]"/>
						</div>
					</div>
					<div class="form-group row">
						<label class="form-label col-md-3">Kepala Sekolah</label>
						<div class="col-md-5">
							<select class="form-control" name="settings[SCHOOL_HEADMASTER]">
								<option value="">Pilih kepala sekolah...</option>
								@foreach ($employees as $employee)
								<option value="{{ $employee->id }}" @if(isset($settings['SCHOOL_HEADMASTER'])) @if ($settings['SCHOOL_HEADMASTER']->value == $employee->id) selected @endif @endif>{{ $employee->name }}</option>	
								@endforeach
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-md-offset-3">
			<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
		</div>
	</div>
</form>
@endsection

