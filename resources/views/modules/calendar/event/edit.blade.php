@extends('templates.data.tabler.app')

@section('title')
Edit Agenda Akademik
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('event edit', $event) }}
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
            <div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('events.update', $event->id) }}" method="POST" class="form-horizontal" role="form">
                            @csrf
                            @method('PUT')
                            <div class="form-group row @if ($errors->has('name')) has-error @endif">
                                <label class="form-label col-md-2">Nama</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" placeholder="Nama Agenda" value="{{ $event->name }}" required>
                                    @if ($errors->has('name')) <div class="invalid-feedback">{{ $errors->first('name') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('event_category_id')) has-error @endif">
                                <label class="form-label col-md-2">Kategori Agenda</label>
                                <div class="col-md-3">
                                    <select class="form-control" name="event_category_id">
                                        <option value="">Pilih Kategori...</option>
                                        @foreach ($event_categories as $event_category)
                                        <option value="{{ $event_category->id }}" @if ($event->event_category_id == $event_category->id) selected @endif>{{ $event_category->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('event_category_id')) <div class="invalid-feedback">{{ $errors->first('event_category_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('date_start') || $errors->has('date_end')) has-error @endif">
                                <label class="form-label col-md-2">Tanggal Agenda</label>
                                <div class="col-md-2">
                                    <div class="input-group date" id="date_start" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" name="date_start" placeholder="Tanggal Mulai" value="{{$event->date_start}}" data-target="#date_start" required>
                                        <div class="input-group-append" data-target="#date_start" data-toggle="datetimepicker"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                    </div>
                                    @if ($errors->has('date_start')) <div class="invalid-feedback">{{ $errors->first('date_start') }}</div> @endif
                                </div>
                                <div class="col-md-2">
                                    <div class="input-group date" id="date_end" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" name="date_end" placeholder="Tanggal Selesai" value="{{$event->date_end}}" data-target="#date_end" required>
                                        <div class="input-group-append" data-target="#date_end" data-toggle="datetimepicker"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                    </div>
                                    @if ($errors->has('date_end')) <div class="invalid-feedback">{{ $errors->first('date_end') }}</div> @endif
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
const app = new Vue({
    el: '#baseapp',
    mounted () {
        $('select[name=event_category_id]').selectize({
            create: true,
            sortField: 'text'
        });
        $('#date_start').datetimepicker({
            locale: 'id',
            format: 'YYYY-MM-DD'
        });
        $('#date_end').datetimepicker({
            locale: 'id',
            format: 'YYYY-MM-DD',
            useCurrent: false
        });
        $("#date_start").on("change.datetimepicker", function (e) {
            $('#date_end').data("DateTimePicker").minDate(e.date);
        });
        $("#date_end").on("change.datetimepicker", function (e) {
            $('#date_start').data("DateTimePicker").maxDate(e.date);
        });
    } 
})
</script>    
@endsection

