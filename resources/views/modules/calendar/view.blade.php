@extends('templates.data.tabler.app')

@section('title')
Kalender Akademik
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('calendar') }}
@endsection

@section('content')
<div class="row" id="baseapp">
	<div class="col-md-12">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						@if ($calendar)
						<div class="row">
							<div class="col-md-12">
								<a href="{{ route('calendar.edit') }}" class="btn btn-primary pull-right" style="margin-top: 15px;" dusk="link-calendar-edit"><i class="fa fa-wrench"></i> Edit Kalendar Akademik</a>
							</div>
						</div>
						<hr>
						<div id="calendar"></div>
						@else
							<div align="center" style="margin-top: 35px; margin-bottom: 35px;">
								<i class="fa fa-4x fa-warning"></i><br>
								<h2 class="text text-warning">Kalender Akademik belum disetting</h2><br>
								<a href="{{ route('calendar.edit') }}" dusk="link-calendar-edit" class="btn btn-primary" style="margin-top: 15px;"><i class="fa fa-wrench"></i> Atur Kalendar Akademik</a>
							</div>
						@endif	
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
var vm = new Vue({
	el: '#baseapp',
	data: {
		table: null,
		events: []
	},
	methods: {
		getEvents: function () {
			axios.get('{{ route('events.show.school_year.active') }}').then(response => {
				$('#calendar').data('calendar').setDataSource(
					response.data.events.map(function(event, index) {
						return {
							id: event.id,
							name: event.name,
							location: '',
							startDate: new Date(event.date_start),
							endDate: new Date(event.date_end),
							color: '#ea4335'
						};
					})
				);
			}).catch(function (error) {
				console.log(error);
			});
		}
	},
	mounted(){
		$('#calendar').calendar({
			minDate: new Date('@if($calendar){{$calendar->semester_1_date_start->format('Y-m-d')}}@endif'),
			maxDate: new Date('@if($calendar){{$calendar->semester_2_date_end->format('Y-m-d')}}@endif'),
		});
		this.getEvents();
	}
})
</script>
@endsection