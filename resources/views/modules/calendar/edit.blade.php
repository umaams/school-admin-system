@extends('templates.data.tabler.app')

@section('title')
Setting Kalender Akademik
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('calendar setting') }}
@endsection

@section('content')
<div class="row" id="baseapp">
	<div class="col-md-12">
		<div class="card">
            <div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('calendar.update') }}" method="POST" role="form">
                            @csrf
                            <div class="form-group row @if ($errors->has('semester_1_date_start') || $errors->has('semester_1_date_end')) has-error @endif">
                                <label class="form-label col-sm-3">Periode Semester Ganjil</label>
                                <div class="col-sm-2">
                                    <div class="input-group date" id="semester_1_date_start" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" name="semester_1_date_start" placeholder="Tanggal Mulai" value="@if($calendar){{$calendar->semester_1_date_start->format('Y-m-d')}}@else{{ old('semester_1_date_start') }}@endif" data-target="#semester_1_date_start" required>
                                        <div class="input-group-append" data-target="#semester_1_date_start" data-toggle="datetimepicker"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                    </div>
                                    @if ($errors->has('semester_1_date_start')) <div class="invalid-feedback">{{ $errors->first('semester_1_date_start') }}</div> @endif
                                </div>
                                <div class="col-md-2">
                                    <div class="input-group date" id="semester_1_date_end" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" name="semester_1_date_end" placeholder="Tanggal Akhir" value="@if($calendar){{ $calendar->semester_1_date_end->format('Y-m-d') }}@else{{ old('semester_1_date_end') }}@endif" data-target="#semester_1_date_end" required>
                                        <div class="input-group-append" data-target="#semester_1_date_end" data-toggle="datetimepicker"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                    </div>
                                    @if ($errors->has('semester_1_date_end')) <div class="invalid-feedback">{{ $errors->first('semester_1_date_end') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('semester_2_date_start') || $errors->has('semester_2_date_end')) has-error @endif">
                                <label class="form-label col-md-3">Periode Semester Genap</label>
                                <div class="col-md-2">
                                    <div class="input-group date" id="semester_2_date_start" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" name="semester_2_date_start" placeholder="Tanggal Mulai" value="@if($calendar){{ $calendar->semester_2_date_start->format('Y-m-d') }}@else{{ old('semester_2_date_start') }}@endif" data-target="#semester_2_date_start" required>
                                        <div class="input-group-append" data-target="#semester_2_date_start" data-toggle="datetimepicker"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                    </div>
                                    @if ($errors->has('semester_2_date_start')) <div class="invalid-feedback">{{ $errors->first('semester_2_date_start') }}</div> @endif
                                </div>
                                <div class="col-md-2">
                                    <div class="input-group date" id="semester_2_date_end" data-target-input="nearest">
                                        <input type="text" class="form-control datetimepicker-input" name="semester_2_date_end" placeholder="Tanggal Akhir" value="@if($calendar){{ $calendar->semester_2_date_end->format('Y-m-d') }}@else{{ old('semester_2_date_end') }}@endif" data-target="#semester_2_date_end" required>
                                        <div class="input-group-append" data-target="#semester_2_date_end" data-toggle="datetimepicker"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div>
                                    </div>
                                    @if ($errors->has('semester_2_date_end')) <div class="invalid-feedback">{{ $errors->first('semester_2_date_end') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('day_week_id')) has-error @endif">
                                <label class="form-label col-md-3">Hari Libur</label>
                                <div class="col-md-3">
                                    <select class="form-control" name="day_week_id[]" multiple required>
                                        @foreach ($day_weeks as $day_week)
                                        <option value="{{ $day_week->id  }}" @if(in_array($day_week->id, $holidays)) selected @endif>{{ $day_week->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('day_week_id')) <div class="invalid-feedback">{{ $errors->first('day_week_id') }}</div> @endif
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button type="submit" class="btn btn-primary" dusk="btn-calendar-update"><i class="fa fa-save"></i> Simpan</button>
                                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
const app = new Vue({
    el: '#baseapp',
    mounted () {
        $('#semester_1_date_start').datetimepicker({
            locale: 'id',
            format: 'YYYY-MM-DD'
        });
        $('#semester_1_date_end').datetimepicker({
            locale: 'id',
            format: 'YYYY-MM-DD',
            useCurrent: false
        });
        $('#semester_2_date_start').datetimepicker({
            locale: 'id',
            format: 'YYYY-MM-DD',
            useCurrent: false
        });
        $('#semester_2_date_end').datetimepicker({
            locale: 'id',
            format: 'YYYY-MM-DD',
            useCurrent: false
        });
        $("#semester_1_date_start").on("change.datetimepicker", function (e) {
            $('#semester_1_date_end').data("DateTimePicker").minDate(e.date);
            $('#semester_2_date_start').data("DateTimePicker").minDate(e.date);
            $('#semester_2_date_end').data("DateTimePicker").minDate(e.date);
        });
        $("#semester_1_date_end").on("change.datetimepicker", function (e) {
            $('#semester_1_date_start').data("DateTimePicker").maxDate(e.date);
            $('#semester_2_date_start').data("DateTimePicker").minDate(e.date);
            $('#semester_2_date_end').data("DateTimePicker").minDate(e.date);
        });
        $("#semester_2_date_start").on("change.datetimepicker", function (e) {
            $('#semester_1_date_start').data("DateTimePicker").maxDate(e.date);
            $('#semester_1_date_end').data("DateTimePicker").maxDate(e.date);
            $('#semester_2_date_end').data("DateTimePicker").minDate(e.date);
        });
        $("#semester_2_date_end").on("change.datetimepicker", function (e) {
            $('#semester_1_date_start').data("DateTimePicker").maxDate(e.date);
            $('#semester_1_date_end').data("DateTimePicker").maxDate(e.date);
            $('#semester_2_date_start').data("DateTimePicker").maxDate(e.date);
        });
    } 
})
</script>    
@endsection