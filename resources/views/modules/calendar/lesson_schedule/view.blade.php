@extends('templates.data.tabler.app')

@section('title')
Jadwal Pelajaran
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('lesson_schedule') }}
@endsection

@section('content')
<div class="row" id="baseapp">
	<div class="col-md-12">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<table id="table" class="table table-hovered table-striped" width="100%">
							<thead class="thead-light">
								<tr>
									<th class="text-center">Nama Kelas</th>
									<th class="text-center">Tingkat</th>
									<th class="text-center">Total Jam</th>
									<th class="text-center">Jumlah Mata Pelajaran</th>
									<th class="text-center">Jumlah Guru</th>
									<th class="text-center"></th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
var vm = new Vue({
	el: '#baseapp',
	data: {
		table: null,
	},
	methods: {
		initDatatable: function () {
		    this.table = $('#table').DataTable({
		        processing: true,
		        serverSide: true,
		        responsive: true,
		        searching: false,
		        paging: false,
		        ajax: '{{ route('lesson_schedules.datatable') }}',
		        columns: [
		            { data: 'name', name: 'name' },
		            { data: 'grade.name', name: 'grade.name', className: 'text-center' },
		            { data: 'lesson_schedules_count', name: 'lesson_schedules_count', className: 'text-center' },
		            { data: 'lessons_count', name: 'lessons_count', className: 'text-center' },
		            { data: 'teachers_count', name: 'teachers_count', className: 'text-center' },
		            { data: 'action', name: 'action', className: 'text-center', sortable: false, searchable: false }
		        ],
		        language: { 
			       "processing": "<span class='text text-info'><b><i class='fa fa-spinner fa-spin'></i> Loading...</b></span>"
			    }
		    });
		}
	},
	mounted(){
		this.initDatatable();
	}
})
</script>
@endsection