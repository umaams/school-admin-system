@extends('templates.data.tabler.app')

@section('title')
Detail Jadwal Pelajaran
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('lesson_schedule show', $class) }}
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<div class="card-status bg-blue"></div>
			<div class="card-header">
				<div class="card-options">
					<a href="{{ route('lesson_schedules.add', ['id' => $class->id]) }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</a>
					<a href="{{ route('lesson_schedules.index') }}" class="btn btn-default"><i class="fa fa-back"></i> Kembali</a>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead class="thead-light">
									<tr>
										<th class="text-center" style="width: 30px;">JAM</th>
										@foreach ($dayWeeks as $dayWeek)
										<th class="text-center">{{ $dayWeek->name }}</th>
										@endforeach
									</tr>
								</thead>
								<tbody>
									@foreach ($schoolTimes as $schoolTime)
									<tr>
										<td class="text-center">{{ $schoolTime->id }}</td>
										@foreach ($schoolTime->dayWeeks as $dayWeek)
										<td class="text-center">
											@if ($dayWeek->lessonSchedule) <a href="{{ route('lesson_schedules.edit', ['id' => $dayWeek->lessonSchedule->id]) }}">{{ $dayWeek->lessonSchedule->lesson->name }}</a> @endif
											@if ($dayWeek->lessonSchedule)
												<br>
												<a>{{ $dayWeek->lessonSchedule->teacher->name }}</a>
											@endif
										</td>
										@endforeach
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
@endsection