@extends('templates.data.tabler.app')

@section('title')
Edit Jadwal
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('lesson_schedule edit', $lesson_schedule) }}
@endsection

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card">
            <div class="card-status bg-blue"></div>
			<div class="card-body">
				<div class="row">
                    <div class="col-md-12">
                        <form action="{{ route('lesson_schedules.update', ['id' => $lesson_schedule->id]) }}" method="POST" class="form-horizontal" role="form">
                            @csrf
                            @method('PATCH')
                            <div class="form-group row @if ($errors->has('day_week_id')) has-error @endif">
                                <label class="form-label col-md-2">Hari</label>
                                <div class="col-md-3">
                                    <select class="form-control selectize" name="day_week_id">
                                        <option value="">Pilih Hari...</option>
                                        @foreach ($dayWeeks as $dayWeek)
                                        <option value="{{ $dayWeek->id }}" @if ($dayWeek->id == $lesson_schedule->day_week_id) selected @endif>{{ $dayWeek->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('day_week_id')) <div class="invalid-feedback">{{ $errors->first('day_week_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('school_time_id')) has-error @endif">
                                <label class="form-label col-md-2">Jam Pelajaran</label>
                                <div class="col-md-2">
                                    <select class="form-control selectize" name="school_time_id">
                                        <option value="">Pilih Jam...</option>
                                        @foreach ($schoolTimes as $schoolTime)
                                        <option value="{{ $schoolTime->id }}" @if ($schoolTime->id == $lesson_schedule->school_time_id) selected @endif>{{ $schoolTime->id }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('school_time_id')) <div class="invalid-feedback">{{ $errors->first('school_time_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('lesson_id')) has-error @endif">
                                <label class="form-label col-md-2">Mata Pelajaran</label>
                                <div class="col-md-4">
                                    <select class="form-control selectize" name="lesson_id">
                                        <option value="">Pilih Mata Pelajaran...</option>
                                        @foreach ($lessons as $lesson)
                                        <option value="{{ $lesson->id }}" @if ($lesson->id == $lesson_schedule->lesson_id) selected @endif>[{{ $lesson->code }}] {{ $lesson->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('lesson_id')) <div class="invalid-feedback">{{ $errors->first('lesson_id') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group row @if ($errors->has('teacher_id')) has-error @endif">
                                <label class="form-label col-md-2">Guru</label>
                                <div class="col-md-4">
                                    <select class="form-control selectize" name="teacher_id">
                                        <option value="">Pilih Guru...</option>
                                        @foreach ($teachers as $teacher)
                                        <option value="{{ $teacher->id }}" @if ($teacher->id == $lesson_schedule->teacher_id) selected @endif>[{{ $teacher->code }}] {{ $teacher->employee->name }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('teacher_id')) <div class="invalid-feedback">{{ $errors->first('teacher_id') }}</div> @endif
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary"  dusk="btn-update"><i class="fa fa-save"></i> Simpan</button>
                                    <form method="post" action="{{ route('lesson_schedules.delete', ['id' => $lesson_schedule->id]) }}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" type="submit"><i class="fa fa-trash"></i> Hapus</button>
                                    </form>
                                    <a href="{{ url()->previous() }}" class="btn btn-default"><i class="fa fa-times-circle"></i> Batal</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
$('.selectize').selectize({
    create: true,
    sortField: 'text'
});
</script>
@endsection