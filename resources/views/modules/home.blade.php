@extends('templates.data.tabler.app')

@section('title')
Home
@endsection

@section('breadcrumbs')
    {{ Breadcrumbs::render('home') }}
@endsection

@section('content')
<div class="row">
    <div class="col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="h1 m-0">{{ $studentCount }}</div>
                <div class="text-muted mb-4">Siswa Aktif</div> 
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="h1 m-0">{{ $employeeCount }}</div>
                <div class="text-muted mb-4">Pegawai Aktif</div> 
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="h1 m-0">{{ $teacherCount }}</div>
                <div class="text-muted mb-4">Guru Aktif</div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card">
            <div class="card-body">
                <div class="h1 m-0">80,600</div>
                <div class="text-muted mb-4">User activity</div>
            </div>
        </div>
    </div>
</div>
@endsection
