<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Login | Sistem Informasi Sekolah</title>
    <link href="/css/app.css" rel="stylesheet">
</head>

<body class="gray-bg">
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <div>
                    <h1 class="logo-name"></h1>
                </div>
                <h3>Login</h3>
                <p>Silahkan login dengan akun anda</p>
                <form class="m-t" role="form" method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group @if ($errors->has('email')) has-error @endif">
                        <input type="email" class="form-control" name="email" placeholder="E-mail" required="">
                        @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                    </div>
                    <div class="form-group @if ($errors->has('password')) has-error @endif">
                        <input type="password" class="form-control" name="password" placeholder="Password" required="">
                        @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
                    </div>
                    <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                    <a href="#"><small>Forgot password?</small></a>
                </form>
                <p class="m-t"> <small>Nesbit Studio &copy; 2018</small> </p>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="/js/app.js"></script>

</body>

</html>