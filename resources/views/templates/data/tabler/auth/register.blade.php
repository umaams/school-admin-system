<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Register | Sistem Informasi Sekolah</title>
    <link href="/css/app.css" rel="stylesheet">
</head>

<body class="gray-bg">
    <div class="middle-box text-center loginscreen   animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name"></h1>
            </div>
            <h3>Register</h3>
            <p>Buat akun anda untuk mengakses berbagai fitur</p>
            <form class="m-t" role="form" method="POST" action="{{ route('register') }}">
                @csrf
                <div class="form-group @if ($errors->has('name')) has-error @endif">
                    <input type="text" name="name" class="form-control" placeholder="Nama Lengkap" required="">
                </div>
                <div class="form-group @if ($errors->has('email')) has-error @endif">
                    <input type="email" name="email" class="form-control" placeholder="Email" required="">
                </div>
                <div class="form-group @if ($errors->has('password')) has-error @endif">
                    <input type="password" name="password" class="form-control" placeholder="Password" required="">
                </div>
                <div class="form-group @if ($errors->has('password_confirmation')) has-error @endif">
                    <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password" required="">
                </div>
                <div class="form-group">
                    <div class="checkbox i-checks"><label> <input type="checkbox"><i></i> Agree the terms and policy </label></div>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Register</button>
                <p class="text-muted text-center"><small>Already have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="{{ route('login') }}">Login</a>
            </form>
            <p class="m-t"> <small>Nesbit Studio &copy; 2018</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="/js/app.js"></script>
</body>

</html>