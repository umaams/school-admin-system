<div class="sidebar-collapse">
    <ul class="nav metismenu" id="side-menu">
        <li class="nav-header">
            <div class="dropdown profile-element"> 
                <span>
                    <img alt="image" class="img-circle" src="{{ asset('images/profile_small.jpg') }}" />
                </span>
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ Auth()->user()->name }}</strong>
                        </span> <span class="text-muted text-xs block">{{ Auth()->user()->roles[0]->description }} <b class="caret"></b></span> </span> </a>
                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                    <li><a href="profile.html">Profile</a></li>
                    <li><a href="contacts.html">Contacts</a></li>
                    <li><a href="mailbox.html">Mailbox</a></li>
                    <li class="divider"></li>
                    <li><a href="login.html">Logout</a></li>
                </ul>
            </div>
            <div class="logo-element">
                SF+
            </div>
        </li>
        <li class="{{ set_active('home') }}">
            <a href="{{ route('home') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
        </li>
        <li class="{{ set_active(['users.index', 'students.index', 'employees.index','teachers.index', 'lesson_categories.index', 'lessons.index', 'classes.index', 'school_times.index', 'curriculums.index']) }}">
            <a href="#" dusk="link-master"><i class="fa fa-database"></i> <span class="nav-label">Master</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                <li class="{{ set_active('users.index') }}"><a href="{{ route('users.index') }}" dusk="link-user-view">User</a></li>
                <li class="{{ set_active('students.index') }}"><a href="{{ route('students.index') }}">Siswa</a></li>
                <li class="{{ set_active('employees.index') }}"><a href="{{ route('employees.index') }}">Pegawai</a></li>
                <li class="{{ set_active('teachers.index') }}"><a href="{{ route('teachers.index') }}">Guru</a></li>
                <li class="{{ set_active('lesson_categories.index') }}"><a href="{{ route('lesson_categories.index') }}">Kelompok Mapel</a></li>
                <li class="{{ set_active('lessons.index') }}"><a href="{{ route('lessons.index') }}">Mata Pelajaran</a></li>
                <li class="{{ set_active('classes.index') }}"><a href="{{ route('classes.index') }}">Kelas</a></li>
                <li class="{{ set_active('school_times.index') }}"><a href="{{ route('school_times.index') }}">Jam Pelajaran</a></li>
                <li class="{{ set_active('curriculums.index') }}"><a href="{{ route('curriculums.index') }}">Kurikulum</a></li>
                <li class="{{ set_active('extracurriculars.index') }}"><a href="{{ route('extracurriculars.index') }}">Ekstrakurikuler</a></li>
            </ul>
        </li>
        <li class="{{ set_active(['calendar.index', 'events.index', 'lesson_schedules.index']) }}">
            <a href="#" dusk="link-calendar"><i class="fa fa-calendar"></i> <span class="nav-label">Kalender Akademik</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                <li class="{{ set_active('calendar.index') }}"><a href="{{ route('calendar.index') }}" dusk="link-calendar-view">Kalender</a></li>
                <li class="{{ set_active('events.index') }}"><a href="{{ route('events.index') }}">Agenda</a></li>
                <li class="{{ set_active('lesson_schedules.index') }}"><a href="{{ route('lesson_schedules.index') }}">Jadwal Pelajaran</a></li>
            </ul>
        </li>
        <li>
            <a href="#"><i class="fa fa-folder"></i> <span class="nav-label">Manajemen Kelas</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                <li><a href="{{ route('classes.guardians.index') }}">Wali Kelas</a></li>
                <li><a href="{{ route('classes.students.index') }}">Rombongan Kelas</a></li>
                <li>
                    <a href="#" dusk="link-calendar"><span class="nav-label">Absensi Siswa</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-third-level collapse">
                        <li><a href="{{ route('attendances.index') }}">Input Absensi</a></li>
                        <li><a href="{{ route('student_permits.index') }}">Ijin Tidak Masuk</a></li>
                    </ul>
                </li>
                <li><a href="{{ route('lesson_journals.index') }}">Jurnal Mata Pelajaran</a></li>
            </ul>
        </li>
        <li>
            <a href="#"><i class="fa fa-print"></i> <span class="nav-label">Report</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                <li><a href="{{ route('reports.students') }}">Siswa</a></li>
                <li><a href="{{ route('reports.class_students') }}">Rombongan Kelas</a></li>
                <li>
                    <a href="#" dusk="link-calendar"><span class="nav-label">Absensi Siswa</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-third-level collapse">
                        <li><a href="{{ route('reports.student_attendances.class') }}">Per Kelas</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <a href="#"><i class="fa fa-gears"></i> <span class="nav-label">Pengaturan</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse">
                <li><a href="{{ route('settings.index') }}">General</a></li>
                <li><a href="{{ route('reports.class_students') }}">Rombongan Kelas</a></li>
            </ul>
        </li>
    </ul>

</div>