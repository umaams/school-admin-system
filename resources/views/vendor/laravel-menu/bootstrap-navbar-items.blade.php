@foreach($items as $item)
  <li class="nav-item @if($item->hasChildren()) dropdown @endif">
    @if($item->link) <a class="nav-link" @lm_attrs($item->link) @if($item->hasChildren()) href="#" data-toggle="dropdown" @endif @lm_endattrs href="{!! $item->url() !!}">
      @if($item->icon) <i class="{{ $item->icon }}"></i> @endif
      {!! $item->title !!}
    </a>
    @else
      @if($item->icon) <i class="{{ $item->icon }}"></i> @endif
      {!! $item->title !!}
    @endif
    @if($item->hasChildren())
      <div class="dropdown-menu dropdown-menu-arrow">
          @foreach($item->children() as $children)
          <a href="{!! $children->url() !!}" class="dropdown-item ">@if($children->icon) <i class="{{ $children->icon }}"></i> @endif {!! $children->title !!}</a>
          @endforeach
      </div>
    @endif
  </li>
  @if($item->divider)
    <li{!! Lavary\Menu\Builder::attributes($item->divider) !!}></li>
  @endif
@endforeach
