<html>
    <body>
        <table>
            <thead>
                <tr>
                    <th colspan="13">REPORT DATA SISWA</th>
                </tr>
                <tr>
                    <th colspan="13" align="center">Tahun Ajaran : </th>
                </tr>
                <tr>
                    <th colspan="10"></th>
                </tr>
                <tr>
                    <th>NO</th>
                    <th>NIS</th>
                    <th>NAMA</th>
                    <th>TEMPAT LAHIR</th>
                    <th>TGL. LAHIR</th>
                    <th>SALDO AWAL</th>
                    <th>MUTASI MASUK</th>
                    <th>MUTASI KELUAR</th>
                    <th>SALDO AKHIR</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $item)
                <tr>
                    <td>{{ $item->fg_product_name }}</td>
                    <td>{{ $item->fg_size_name }}</td>
                    <td>{{ $item->fg_package_name }}</td>
                    <td>{{ $item->fg_brand_name }}</td>
                    <td>{{ $item->fg_glazing_name }}</td>
                    <td>{{ $item->beginning_quantity }}</td>
                    <td>{{ $item->beginning_weight }}</td>
                    <td>{{ $item->debit_quantity }}</td>
                    <td>{{ $item->debit_weight }}</td>
                    <td>{{ $item->credit_quantity }}</td>
                    <td>{{ $item->credit_weight }}</td>
                    <td>{{ $item->final_quantity }}</td>
                    <td>{{ $item->final_weight }}</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="5">TOTAL</th>
                    <th>{{ $result['data']->sum('beginning_quantity') }}</th>
                    <th>{{ $result['data']->sum('beginning_weight') }}</th>
                    <th>{{ $result['data']->sum('debit_quantity') }}</th>
                    <th>{{ $result['data']->sum('debit_weight') }}</th>
                    <th>{{ $result['data']->sum('credit_quantity') }}</th>
                    <th>{{ $result['data']->sum('credit_weight') }}</th>
                    <th>{{ $result['data']->sum('final_quantity') }}</th>
                    <th>{{ $result['data']->sum('final_weight') }}</th>
                </tr>
                <tr>
                    <th colspan="13">&nbsp;</th>
                </tr>
                <tr>
                    <th colspan="13">Diexport pada {{ date('d M Y H:i:s') }} WIB, Oleh: {{ Auth::user()->name }}</th>
                </tr>
            </tfoot>
        </table>
    </body>
</html>
