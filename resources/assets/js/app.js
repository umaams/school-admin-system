
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
global.$ = window.jQuery = window.$ = global.jQuery = require('jquery');
require('./bootstrap');
require('metismenu');
window.moment = require('moment');
window.Chart = require('chart.js');
window.accounting = require("accounting");
require('jquery-slimscroll');
require('datatables.net');
require('datatables.net-bs4');
require('datatables.net-responsive-bs4');
require('datatables.net-fixedcolumns-bs4');
require('datatables.net-fixedheader');
require('select2');
window.selectize = require('selectize');
window.collect = require('collect.js');
window.feather = require('feather-icons');
import CKEditor from '@ckeditor/ckeditor5-vue';
require('bootstrap-year-calendar-bs4');
require('../templates/inspinia/js/inspinia.js');
require('../../../node_modules/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js');

window.Vue = require('vue');
Vue.use(CKEditor);
moment.locale('id');
feather.replace();

$.extend(true, $.fn.dataTable.defaults, {
    "language": {
        "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Indonesian.json"
    }
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example-component', require('./components/ExampleComponent.vue'));

Vue.filter('dateFormat', function (value) {
  return moment(value).format('DD-MM-YYYY');
});

Vue.filter('currencyFixedFormat', function (value) {
    return accounting.formatMoney(value, "", 0);
})

Vue.filter('currencyFormat', function (value) {
    return accounting.formatMoney(value, "", 2);
})

const app = new Vue({
    el: '#app'
});
