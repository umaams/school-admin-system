<?php

use Illuminate\Database\Seeder;
use App\Models\Master\Curriculum;
use App\Models\Master\SchoolYear;
use App\Models\Master\Lesson;
use App\Models\Master\Grade;

class CurriculumsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $curriculum = Curriculum::updateOrCreate(['id' => 1], [
        	'name' => 'Kurikulum 2013'
        ]);
        
        $grade = Grade::findOrFail(1);
        $grade->lessons()->sync([
            Lesson::where('code', 'QDS')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'AQD')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'FQH')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'PKN')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 5, 'kkm' => 0],
            Lesson::where('code', 'BID')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 8, 'kkm' => 0],
            Lesson::where('code', 'MTK')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 5, 'kkm' => 0],
            Lesson::where('code', 'BAR')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'SBK')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'PJK')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 4, 'kkm' => 0],
            Lesson::where('code', 'BJA')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0]
        ]);

        $grade = Grade::findOrFail(2);
        $grade->lessons()->sync([
            Lesson::where('code', 'QDS')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'AQD')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'FQH')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'PKN')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 5, 'kkm' => 0],
            Lesson::where('code', 'BID')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 9, 'kkm' => 0],
            Lesson::where('code', 'MTK')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 6, 'kkm' => 0],
            Lesson::where('code', 'BAR')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'SBK')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'PJK')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 4, 'kkm' => 0],
            Lesson::where('code', 'BJA')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0]
        ]);

        $grade = Grade::findOrFail(3);
        $grade->lessons()->sync([
            Lesson::where('code', 'QDS')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'AQD')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'FQH')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'SKI')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'PKN')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 6, 'kkm' => 0],
            Lesson::where('code', 'BID')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 10, 'kkm' => 0],
            Lesson::where('code', 'MTK')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 6, 'kkm' => 0],
            Lesson::where('code', 'BAR')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'SBK')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'PJK')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 4, 'kkm' => 0],
            Lesson::where('code', 'BJA')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0]
        ]);

        $grade = Grade::findOrFail(4);
        $grade->lessons()->sync([
            Lesson::where('code', 'QDS')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'AQD')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'FQH')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'SKI')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'PKN')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 5, 'kkm' => 0],
            Lesson::where('code', 'BID')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 7, 'kkm' => 0],
            Lesson::where('code', 'MTK')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 6, 'kkm' => 0],
            Lesson::where('code', 'BAR')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'IPA')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 3, 'kkm' => 0],
            Lesson::where('code', 'IPS')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 3, 'kkm' => 0],
            Lesson::where('code', 'SBK')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'PJK')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 4, 'kkm' => 0],
            Lesson::where('code', 'BJA')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0]
        ]);

        $grade = Grade::findOrFail(5);
        $grade->lessons()->sync([
            Lesson::where('code', 'QDS')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'AQD')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'FQH')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'SKI')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'PKN')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 5, 'kkm' => 0],
            Lesson::where('code', 'BID')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 7, 'kkm' => 0],
            Lesson::where('code', 'MTK')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 6, 'kkm' => 0],
            Lesson::where('code', 'BAR')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'IPA')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 3, 'kkm' => 0],
            Lesson::where('code', 'IPS')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 3, 'kkm' => 0],
            Lesson::where('code', 'SBK')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'PJK')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 4, 'kkm' => 0],
            Lesson::where('code', 'BJA')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0]
        ]);

        $grade = Grade::findOrFail(6);
        $grade->lessons()->sync([
            Lesson::where('code', 'QDS')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'AQD')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'FQH')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'SKI')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'PKN')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 5, 'kkm' => 0],
            Lesson::where('code', 'BID')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 7, 'kkm' => 0],
            Lesson::where('code', 'MTK')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 6, 'kkm' => 0],
            Lesson::where('code', 'BAR')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'IPA')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 3, 'kkm' => 0],
            Lesson::where('code', 'IPS')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 3, 'kkm' => 0],
            Lesson::where('code', 'SBK')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0],
            Lesson::where('code', 'PJK')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 4, 'kkm' => 0],
            Lesson::where('code', 'BJA')->first()->id => ['curriculum_id' => $curriculum->id, 'allocation' => 2, 'kkm' => 0]
        ]);

        $school_year = SchoolYear::where('active', '1')->first();
        $school_year->curriculum_id = $curriculum->id;
        $school_year->save();
    }
}
