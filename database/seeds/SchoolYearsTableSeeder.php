<?php

use Illuminate\Database\Seeder;
use App\Models\Master\SchoolYear;

class SchoolYearsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SchoolYear::updateOrCreate(['name' => '2010/2011'], ['start_year' => '2010', 'end_year' => '2011', 'active' => '0']);
        SchoolYear::updateOrCreate(['name' => '2011/2012'], ['start_year' => '2011', 'end_year' => '2012', 'active' => '0']);
        SchoolYear::updateOrCreate(['name' => '2012/2013'], ['start_year' => '2012', 'end_year' => '2013', 'active' => '0']);
        SchoolYear::updateOrCreate(['name' => '2013/2014'], ['start_year' => '2013', 'end_year' => '2014', 'active' => '0']);
        SchoolYear::updateOrCreate(['name' => '2014/2015'], ['start_year' => '2014', 'end_year' => '2015', 'active' => '0']);
        SchoolYear::updateOrCreate(['name' => '2015/2016'], ['start_year' => '2015', 'end_year' => '2016', 'active' => '0']);
        SchoolYear::updateOrCreate(['name' => '2016/2017'], ['start_year' => '2016', 'end_year' => '2017', 'active' => '0']);
        SchoolYear::updateOrCreate(['name' => '2017/2018'], ['start_year' => '2017', 'end_year' => '2018', 'active' => '0']);
        SchoolYear::updateOrCreate(['name' => '2018/2019'], ['start_year' => '2018', 'end_year' => '2019', 'active' => '1']);
    }
}
