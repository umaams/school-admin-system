<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bouncer::role()->updateOrCreate(['name' => 'superadmin'], ['title' => 'Super Administrator']);
        Bouncer::role()->updateOrCreate(['name' => 'admin'], ['title' => 'Administrator']);
        Bouncer::role()->updateOrCreate(['name' => 'teacher'], ['title' => 'Guru']);
        Bouncer::role()->updateOrCreate(['name' => 'operator'], ['title' => 'Operator']);
    }
}
