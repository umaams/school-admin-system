<?php

use Illuminate\Database\Seeder;
use App\Models\Master\SchoolTime;

class SchoolTimesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SchoolTime::updateOrCreate(['id' => '1'], ['time_start' => '06:30', 'time_end' => '07:15']);
        SchoolTime::updateOrCreate(['id' => '2'], ['time_start' => '07:15', 'time_end' => '08:00']);
        SchoolTime::updateOrCreate(['id' => '3'], ['time_start' => '08:00', 'time_end' => '08:45']);
        SchoolTime::updateOrCreate(['id' => '4'], ['time_start' => '08:45', 'time_end' => '09:30']);
        SchoolTime::updateOrCreate(['id' => '5'], ['time_start' => '09:30', 'time_end' => '10:15']);
        SchoolTime::updateOrCreate(['id' => '6'], ['time_start' => '10:15', 'time_end' => '11:00']);
        SchoolTime::updateOrCreate(['id' => '7'], ['time_start' => '11:00', 'time_end' => '11:45']);
    }
}
