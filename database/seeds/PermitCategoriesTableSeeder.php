<?php

use Illuminate\Database\Seeder;
use App\Models\Master\PermitCategory;

class PermitCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PermitCategory::updateOrCreate(['id' => '1'], ['name' => 'SAKIT']);
        PermitCategory::updateOrCreate(['id' => '2'], ['name' => 'IJIN']);
    }
}
