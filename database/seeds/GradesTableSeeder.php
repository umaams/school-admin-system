<?php

use Illuminate\Database\Seeder;
use App\Models\Master\Grade;

class GradesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Grade::updateOrCreate(['id' => '1'], ['name' => 'I']);
        Grade::updateOrCreate(['id' => '2'], ['name' => 'II']);
        Grade::updateOrCreate(['id' => '3'], ['name' => 'III']);
        Grade::updateOrCreate(['id' => '4'], ['name' => 'IV']);
        Grade::updateOrCreate(['id' => '5'], ['name' => 'V']);
        Grade::updateOrCreate(['id' => '6'], ['name' => 'VI']);
    }
}
