<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(BloodTypesTableSeeder::class);
        $this->call(ReligionsTableSeeder::class);
        $this->call(EducationsTableSeeder::class);
        $this->call(OccupationsTableSeeder::class);
        $this->call(SemestersTableSeeder::class);
        $this->call(GradesTableSeeder::class);
        $this->call(DayWeeksTableSeeder::class);
        $this->call(SchoolYearsTableSeeder::class);
        $this->call(SchoolYearCalendarsTableSeeder::class);
        $this->call(SchoolTimesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(PermitCategoriesTableSeeder::class);
        $this->call(EventCategoriesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ClassesTableSeeder::class);
        $this->call(LessonCategoriesTableSeeder::class);
        $this->call(LessonsTableSeeder::class);
        $this->call(StudentsTableSeeder::class);
        $this->call(EmployeesTableSeeder::class);
        $this->call(CurriculumsTableSeeder::class);
        $this->call(ClassStudentsTableSeeder::class);
    }
}
