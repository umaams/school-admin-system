<?php

use Illuminate\Database\Seeder;
use App\Models\Master\Lesson;

class LessonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Lesson::updateOrCreate(['id' => 1], ['code' => 'QDS', 'name' => 'Al-Quran Hadist']);
        Lesson::updateOrCreate(['id' => 2], ['code' => 'AQD', 'name' => 'Akidah Akhlak']);
        Lesson::updateOrCreate(['id' => 3], ['code' => 'FQH', 'name' => 'Fiqih']);
        Lesson::updateOrCreate(['id' => 4], ['code' => 'SKI', 'name' => 'Sejarah Kebudayaan Islam']);
        Lesson::updateOrCreate(['id' => 5], ['code' => 'PKN', 'name' => 'Pendidikan Kewarganegaraan']);
        Lesson::updateOrCreate(['id' => 6], ['code' => 'BID', 'name' => 'Bahasa Indonesia']);
        Lesson::updateOrCreate(['id' => 7], ['code' => 'MTK', 'name' => 'Matematika']);
        Lesson::updateOrCreate(['id' => 8], ['code' => 'BAR', 'name' => 'Bahasa Arab']);
        Lesson::updateOrCreate(['id' => 9], ['code' => 'IPA', 'name' => 'Ilmu Pengetahuan Alam']);
        Lesson::updateOrCreate(['id' => 10], ['code' => 'IPS', 'name' => 'Ilmu Pengetahuan Sosial']);
        Lesson::updateOrCreate(['id' => 11], ['code' => 'SBK', 'name' => 'Seni Budaya dan Keterampilan']);
        Lesson::updateOrCreate(['id' => 12], ['code' => 'PJK', 'name' => 'Pendidikan Jasmani Olahraga dan Kesehatan']);
        Lesson::updateOrCreate(['id' => 13], ['code' => 'BJA', 'name' => 'Bahasa Jawa']);
        Lesson::updateOrCreate(['id' => 14], ['code' => 'BIG', 'name' => 'Bahasa Inggris']);
    }
}
