<?php

use Illuminate\Database\Seeder;
use App\Models\Master\Classes;

class ClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Classes::updateOrCreate(['id' => 1], ['name' => 'I', 'grade_id' => 1]);
        Classes::updateOrCreate(['id' => 2], ['name' => 'II', 'grade_id' => 2]);
        Classes::updateOrCreate(['id' => 3], ['name' => 'III', 'grade_id' => 3]);
        Classes::updateOrCreate(['id' => 4], ['name' => 'IV', 'grade_id' => 4]);
        Classes::updateOrCreate(['id' => 5], ['name' => 'V', 'grade_id' => 5]);
        Classes::updateOrCreate(['id' => 6], ['name' => 'VI', 'grade_id' => 6]);
    }
}
