<?php

use Illuminate\Database\Seeder;
use App\Models\Master\BloodType;

class BloodTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BloodType::updateOrCreate(['id' => '1'], ['name' => 'O']);
        BloodType::updateOrCreate(['id' => '2'], ['name' => 'A']);
        BloodType::updateOrCreate(['id' => '3'], ['name' => 'B']);
        BloodType::updateOrCreate(['id' => '4'], ['name' => 'AB']);
    }
}
