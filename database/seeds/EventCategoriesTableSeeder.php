<?php

use Illuminate\Database\Seeder;
use App\Models\Master\EventCategory;

class EventCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EventCategory::updateOrCreate(['id' => '1'], ['name' => 'UJIAN TENGAH SEMESTER', 'active_day' => '1', 'holiday' => '0']);
        EventCategory::updateOrCreate(['id' => '2'], ['name' => 'UJIAN AKHIR SEMESTER', 'active_day' => '1', 'holiday' => '0']);
        EventCategory::updateOrCreate(['id' => '3'], ['name' => 'UJIAN AKHIR SEKOLAH', 'active_day' => '1', 'holiday' => '0']);
        EventCategory::updateOrCreate(['id' => '4'], ['name' => 'LIBUR NASIONAL', 'active_day' => '0', 'holiday' => '1']);
        EventCategory::updateOrCreate(['id' => '5'], ['name' => 'LIBUR PUASA/HARI RAYA', 'active_day' => '0', 'holiday' => '1']);
        EventCategory::updateOrCreate(['id' => '6'], ['name' => 'LIBUR AKHIR SEMESTER', 'active_day' => '0', 'holiday' => '1']);
    }
}
