<?php

use Illuminate\Database\Seeder;
use App\Models\Master\Classes;
use App\Models\Master\Student;
use App\Models\Master\SchoolYear;

class ClassStudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $classes = Classes::all();
        foreach ($classes as $key => $class) {
        	$students = Student::whereDoesntHave('classes', function($query) {
        		$query->where('school_year_id', SchoolYear::where('active', '1')->first()->id);
        	})->inRandomOrder()->limit(10)->pluck('id')->all();
        	$class->students()->attach($students, ['school_year_id' => SchoolYear::where('active', '1')->first()->id]);
        }
    }
}
