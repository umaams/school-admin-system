<?php

use Illuminate\Database\Seeder;
use App\Models\Master\Religion;

class ReligionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Religion::updateOrCreate(['id' => '1'], ['name' => 'ISLAM']);
        Religion::updateOrCreate(['id' => '2'], ['name' => 'KRISTEN']);
        Religion::updateOrCreate(['id' => '3'], ['name' => 'KATOLIK']);
        Religion::updateOrCreate(['id' => '4'], ['name' => 'HINDU']);
        Religion::updateOrCreate(['id' => '5'], ['name' => 'BUDHA']);
        Religion::updateOrCreate(['id' => '6'], ['name' => 'KONGHUCU']);
        Religion::updateOrCreate(['id' => '7'], ['name' => 'LAINNYA']);
    }
}
