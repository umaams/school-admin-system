<?php

use Illuminate\Database\Seeder;
use App\Models\Master\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::updateOrCreate([ 'email' => 'khotib.umam7@gmail.com' ], [
            'name' => 'Khotibul Umam',
            'password' => bcrypt('secret')
        ]);
        //$user->assignRole('superadmin');
    }
}
