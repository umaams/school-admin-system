<?php

use Illuminate\Database\Seeder;
use Bouncer;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bouncer::ability()->updateOrCreate(['name' => 'master.user.view'], ['title' => 'Lihat User']);
        Bouncer::ability()->updateOrCreate(['name' => 'master.user.create'], ['title' => 'Tambah User']);
        Bouncer::ability()->updateOrCreate(['name' => 'master.user.edit'], ['title' => 'Edit User']);
        Bouncer::ability()->updateOrCreate(['name' => 'master.user.delete'], ['title' => 'Hapus User']);

        Bouncer::ability()->updateOrCreate(['name' => 'master.employee.view'], ['title' => 'Lihat Pegawai']);
        Bouncer::ability()->updateOrCreate(['name' => 'master.employee.create'], ['title' => 'Tambah Pegawai']);
        Bouncer::ability()->updateOrCreate(['name' => 'master.employee.edit'], ['title' => 'Edit Pegawai']);
        Bouncer::ability()->updateOrCreate(['name' => 'master.employee.delete'], ['title' => 'Hapus Pegawai']);

        Bouncer::ability()->updateOrCreate(['name' => 'master.class.view'], ['title' => 'Lihat Kelas']);
        Bouncer::ability()->updateOrCreate(['name' => 'master.class.create'], ['title' => 'Tambah Kelas']);
        Bouncer::ability()->updateOrCreate(['name' => 'master.class.edit'], ['title' => 'Edit Kelas']);
        Bouncer::ability()->updateOrCreate(['name' => 'master.class.delete'], ['title' => 'Hapus Kelas']);

        Bouncer::ability()->updateOrCreate(['name' => 'master.student.view'], ['title' => 'Lihat Siswa']);
        Bouncer::ability()->updateOrCreate(['name' => 'master.student.create'], ['title' => 'Tambah Siswa']);
        Bouncer::ability()->updateOrCreate(['name' => 'master.student.edit'], ['title' => 'Edit Siswa']);
        Bouncer::ability()->updateOrCreate(['name' => 'master.student.delete'], ['title' => 'Hapus Siswa']);

        Bouncer::ability()->updateOrCreate(['name' => 'master.teacher.view'], ['title' => 'Lihat Guru']);
        Bouncer::ability()->updateOrCreate(['name' => 'master.teacher.create'], ['title' => 'Tambah Guru']);
        Bouncer::ability()->updateOrCreate(['name' => 'master.teacher.edit'], ['title' => 'Edit Guru']);
        Bouncer::ability()->updateOrCreate(['name' => 'master.teacher.delete'], ['title' => 'Hapus Guru']);

        Bouncer::ability()->updateOrCreate(['name' => 'master.lesson.view'], ['title' => 'Lihat Mata Pelajaran']);
        Bouncer::ability()->updateOrCreate(['name' => 'master.lesson.create'], ['title' => 'Tambah Mata Pelajaran']);
        Bouncer::ability()->updateOrCreate(['name' => 'master.lesson.edit'], ['title' => 'Edit Mata Pelajaran']);
        Bouncer::ability()->updateOrCreate(['name' => 'master.lesson.delete'], ['title' => 'Hapus Mata Pelajaran']);
    }
}
