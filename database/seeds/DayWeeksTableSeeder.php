<?php

use Illuminate\Database\Seeder;
use App\Models\Master\DayWeek;

class DayWeeksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DayWeek::updateOrCreate(['id' => '1'], ['name' => 'MINGGU']);
        DayWeek::updateOrCreate(['id' => '2'], ['name' => 'SENIN']);
        DayWeek::updateOrCreate(['id' => '3'], ['name' => 'SELASA']);
        DayWeek::updateOrCreate(['id' => '4'], ['name' => 'RABU']);
        DayWeek::updateOrCreate(['id' => '5'], ['name' => 'KAMIS']);
        DayWeek::updateOrCreate(['id' => '6'], ['name' => 'JUMAT']);
        DayWeek::updateOrCreate(['id' => '7'], ['name' => 'SABTU']);
    }
}
