<?php

use Illuminate\Database\Seeder;
use App\Models\Master\Occupation;

class OccupationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        Occupation::updateOrCreate(['id' => 1], ['name' => 'BELUM/TIDAK BEKERJA']);
        Occupation::updateOrCreate(['id' => 2], ['name' => 'MENGURUS RUMAH TANGGA']);
        Occupation::updateOrCreate(['id' => 3], ['name' => 'PELAJAR/MAHASISWA']);
        Occupation::updateOrCreate(['id' => 4], ['name' => 'PENSIUNAN']);
        Occupation::updateOrCreate(['id' => 5], ['name' => 'PEGAWAI NEGERI SIPIL (PNS)']);
        Occupation::updateOrCreate(['id' => 6], ['name' => 'TENTARA NASIONAL INDONESIA (TNI)']);
        Occupation::updateOrCreate(['id' => 7], ['name' => 'KEPOLISIAN RI (POLRI)']);
        Occupation::updateOrCreate(['id' => 8], ['name' => 'PERDAGANGAN']);
        Occupation::updateOrCreate(['id' => 9], ['name' => 'PETANI/PERKEBUNAN']);
        Occupation::updateOrCreate(['id' => 10], ['name' => 'PETERNAK']);
        Occupation::updateOrCreate(['id' => 11], ['name' => 'NELAYAN/PERIKANAN']);
        Occupation::updateOrCreate(['id' => 12], ['name' => 'INDUSTRI']);
        Occupation::updateOrCreate(['id' => 13], ['name' => 'KONSTRUKSI']);
        Occupation::updateOrCreate(['id' => 14], ['name' => 'TRANSPORTASI']);
        Occupation::updateOrCreate(['id' => 15], ['name' => 'KARYAWAN SWASTA']);
        Occupation::updateOrCreate(['id' => 16], ['name' => 'KARYAWAN BUMN']);
        Occupation::updateOrCreate(['id' => 17], ['name' => 'KARYAWAN BUMD']);
        Occupation::updateOrCreate(['id' => 18], ['name' => 'KARYAWAN HONORER']);
        Occupation::updateOrCreate(['id' => 19], ['name' => 'BURUH HARIAN LEPAS']);
        Occupation::updateOrCreate(['id' => 20], ['name' => 'BURUH TANI/PERKEBUNAN']);
        Occupation::updateOrCreate(['id' => 21], ['name' => 'BURUH NELAYAN/PERIKANAN']);
        Occupation::updateOrCreate(['id' => 22], ['name' => 'BURUH PETERNAKAN']);
        Occupation::updateOrCreate(['id' => 23], ['name' => 'PEMBANTU RUMAH TANGGA']);
        Occupation::updateOrCreate(['id' => 24], ['name' => 'TUKANG CUKUR']);
        Occupation::updateOrCreate(['id' => 25], ['name' => 'TUKANG LISTRIK']);
        Occupation::updateOrCreate(['id' => 26], ['name' => 'TUKANG BATU']);
        Occupation::updateOrCreate(['id' => 27], ['name' => 'TUKANG KAYU']);
        Occupation::updateOrCreate(['id' => 28], ['name' => 'TUKANG SOL SEPATU']);
        Occupation::updateOrCreate(['id' => 29], ['name' => 'TUKANG LAS/PANDAI BESI']);
        Occupation::updateOrCreate(['id' => 30], ['name' => 'TUKANG JAHIT']);
        Occupation::updateOrCreate(['id' => 31], ['name' => 'TUKANG GIGI']);
        Occupation::updateOrCreate(['id' => 32], ['name' => 'PENATA RIAS']);
        Occupation::updateOrCreate(['id' => 33], ['name' => 'PENATA BUSANA']);
        Occupation::updateOrCreate(['id' => 34], ['name' => 'PENATA RAMBUT']);
        Occupation::updateOrCreate(['id' => 35], ['name' => 'MEKANIK']);
        Occupation::updateOrCreate(['id' => 36], ['name' => 'SENIMAN']);
        Occupation::updateOrCreate(['id' => 37], ['name' => 'TABIB']);
        Occupation::updateOrCreate(['id' => 38], ['name' => 'PARAJI']);
        Occupation::updateOrCreate(['id' => 39], ['name' => 'PERANCANG BUSANA']);
        Occupation::updateOrCreate(['id' => 40], ['name' => 'PENTERJEMAH']);
        Occupation::updateOrCreate(['id' => 41], ['name' => 'IMAM MASJID']);
        Occupation::updateOrCreate(['id' => 42], ['name' => 'PENDETA']);
        Occupation::updateOrCreate(['id' => 43], ['name' => 'PASTOR']);
        Occupation::updateOrCreate(['id' => 44], ['name' => 'WARTAWAN']);
        Occupation::updateOrCreate(['id' => 45], ['name' => 'USTADZ/MUBALIGH']);
        Occupation::updateOrCreate(['id' => 46], ['name' => 'JURU MASAK']);
        Occupation::updateOrCreate(['id' => 47], ['name' => 'PROMOTOR ACARA']);
        Occupation::updateOrCreate(['id' => 48], ['name' => 'ANGGOTA DPR-RI']);
        Occupation::updateOrCreate(['id' => 49], ['name' => 'ANGGOTA DPD']);
        Occupation::updateOrCreate(['id' => 50], ['name' => 'ANGGOTA BPK']);
        Occupation::updateOrCreate(['id' => 51], ['name' => 'PRESIDEN']);
        Occupation::updateOrCreate(['id' => 52], ['name' => 'WAKIL PRESIDEN']);
        Occupation::updateOrCreate(['id' => 53], ['name' => 'ANGGOTA MAHKAMAH KONSTITUSI']);
        Occupation::updateOrCreate(['id' => 54], ['name' => 'ANGGOTA KABINET KEMENTERIAN']);
        Occupation::updateOrCreate(['id' => 55], ['name' => 'DUTA BESAR']);
        Occupation::updateOrCreate(['id' => 56], ['name' => 'GUBERNUR']);
        Occupation::updateOrCreate(['id' => 57], ['name' => 'WAKIL GUBERNUR']);
        Occupation::updateOrCreate(['id' => 58], ['name' => 'BUPATI']);
        Occupation::updateOrCreate(['id' => 59], ['name' => 'WAKIL BUPATI']);
        Occupation::updateOrCreate(['id' => 60], ['name' => 'WALIKOTA']);
        Occupation::updateOrCreate(['id' => 61], ['name' => 'WAKIL WALIKOTA']);
        Occupation::updateOrCreate(['id' => 62], ['name' => 'ANGGOTA DPRD PROVINSI']);
        Occupation::updateOrCreate(['id' => 63], ['name' => 'ANGGOTA DPRD KABUPATEN/KOTA']);
        Occupation::updateOrCreate(['id' => 64], ['name' => 'DOSEN']);
        Occupation::updateOrCreate(['id' => 65], ['name' => 'GURU']);
        Occupation::updateOrCreate(['id' => 66], ['name' => 'PILOT']);
        Occupation::updateOrCreate(['id' => 67], ['name' => 'PENGACARA']);
        Occupation::updateOrCreate(['id' => 68], ['name' => 'NOTARIS']);
        Occupation::updateOrCreate(['id' => 69], ['name' => 'ARSITEK']);
        Occupation::updateOrCreate(['id' => 70], ['name' => 'AKUNTAN']);
        Occupation::updateOrCreate(['id' => 71], ['name' => 'KONSULTAN']);
        Occupation::updateOrCreate(['id' => 72], ['name' => 'DOKTER']);
        Occupation::updateOrCreate(['id' => 73], ['name' => 'BIDAN']);
        Occupation::updateOrCreate(['id' => 74], ['name' => 'PERAWAT']);
        Occupation::updateOrCreate(['id' => 75], ['name' => 'APOTEKER']);
        Occupation::updateOrCreate(['id' => 76], ['name' => 'PSIKIATER/PSIKOLOG']);
        Occupation::updateOrCreate(['id' => 77], ['name' => 'PENYIAR TELEVISI']);
        Occupation::updateOrCreate(['id' => 78], ['name' => 'PENYIAR RADIO']);
        Occupation::updateOrCreate(['id' => 79], ['name' => 'PELAUT']);
        Occupation::updateOrCreate(['id' => 80], ['name' => 'PENELITI']);
        Occupation::updateOrCreate(['id' => 81], ['name' => 'SOPIR']);
        Occupation::updateOrCreate(['id' => 82], ['name' => 'PIALANG']);
        Occupation::updateOrCreate(['id' => 83], ['name' => 'PARANORMAL']);
        Occupation::updateOrCreate(['id' => 84], ['name' => 'PEDAGANG']);
        Occupation::updateOrCreate(['id' => 85], ['name' => 'PERANGKAT DESA']);
        Occupation::updateOrCreate(['id' => 86], ['name' => 'KEPALA DESA']);
        Occupation::updateOrCreate(['id' => 87], ['name' => 'BIARAWATI']);
        Occupation::updateOrCreate(['id' => 88], ['name' => 'WIRASWASTA']);
        Occupation::updateOrCreate(['id' => 89], ['name' => 'LAINNYA']);
    }
}