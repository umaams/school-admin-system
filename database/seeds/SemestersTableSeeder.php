<?php

use Illuminate\Database\Seeder;
use App\Models\Master\Semester;

class SemestersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Semester::updateOrCreate(['id' => '1'], ['name' => 'I']);
        Semester::updateOrCreate(['id' => '2'], ['name' => 'II']);
    }
}
