<?php

use Illuminate\Database\Seeder;
use App\Models\Master\LessonCategory;

class LessonCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LessonCategory::updateOrCreate(['id' => 1], ['name' => 'Kelompok A']);
        LessonCategory::updateOrCreate(['id' => 2], ['name' => 'Kelompok B']);
        LessonCategory::updateOrCreate(['id' => 3], ['name' => 'Kelompok C']);
    }
}
