<?php

use Illuminate\Database\Seeder;
use App\Models\Master\SchoolYear;
use App\Models\SchoolYearCalendar;
use App\Models\SchoolYearHoliday;

class SchoolYearCalendarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SchoolYearCalendar::updateOrCreate(['school_year_id' => SchoolYear::where('active', '1')->first()->id], [
            'semester_1_date_start' => '2019-07-01',
            'semester_1_date_end' => '2019-12-31',
            'semester_2_date_start' => '2020-01-01',
            'semester_2_date_end' => '2020-06-30'
        ]);
        SchoolYearHoliday::create(['school_year_id' => SchoolYear::where('active', '1')->first()->id,
            'day_week_id' => 7
        ]);
        SchoolYearHoliday::create(['school_year_id' => SchoolYear::where('active', '1')->first()->id,
            'day_week_id' => 1
        ]);
    }
}
