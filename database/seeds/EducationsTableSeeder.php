<?php

use Illuminate\Database\Seeder;
use App\Models\Master\Education;

class EducationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        Education::updateOrCreate(['id' => '1'], ['name' => 'SD']);
        Education::updateOrCreate(['id' => '2'], ['name' => 'SMP']);
        Education::updateOrCreate(['id' => '3'], ['name' => 'SMA']);
        Education::updateOrCreate(['id' => '4'], ['name' => 'D-I']);
        Education::updateOrCreate(['id' => '5'], ['name' => 'D-II']);
        Education::updateOrCreate(['id' => '6'], ['name' => 'D-III']);
        Education::updateOrCreate(['id' => '7'], ['name' => 'D-IV']);
        Education::updateOrCreate(['id' => '8'], ['name' => 'S-1']);
        Education::updateOrCreate(['id' => '9'], ['name' => 'S-2']);
        Education::updateOrCreate(['id' => '10'], ['name' => 'S-3']);
    }
}