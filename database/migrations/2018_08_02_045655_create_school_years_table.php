<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSchoolYearsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('school_years', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 12);
			$table->integer('start_year');
			$table->integer('end_year');
			$table->enum('active', array('0','1'))->default('0');
			$table->unsignedInteger('curriculum_id')->nullable();
			$table->timestamps();

            $table->foreign('curriculum_id')->references('id')->on('curriculums');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('school_years');
	}

}
