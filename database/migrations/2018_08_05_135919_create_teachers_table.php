<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeachersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('teachers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('code', 10)->unique();
			$table->string('nuptk', 50)->unique();
			$table->unsignedInteger('employee_id');
			$table->enum('active', array('0','1'))->default('1');
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('employee_id')->references('id')->on('employees');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('teachers');
	}

}
