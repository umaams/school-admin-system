<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonJournalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lesson_journals', function (Blueprint $table) {
            $table->increments('id');
            $table->date('journal_date');
            $table->unsignedInteger('lesson_schedule_id');
            $table->text('description');
            $table->timestamps();

            $table->foreign('lesson_schedule_id')->references('id')->on('lesson_schedules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lesson_journals');
    }
}
