<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateEmployeesTable.
 */
class CreateEmployeesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('employees', function(Blueprint $table) {
            $table->increments('id');
			$table->string('nip', 50)->unique();
			$table->string('nik', 16)->nullable();
			$table->string('name', 80);
			$table->enum('sex', array('M','F'));
			$table->string('birth_place', 50);
			$table->date('birth_date');
			$table->text('address');
			$table->char('village_id', 10);
			$table->string('zip_code', 8)->nullable();
			$table->integer('religion_id');
			$table->string('phone', 20)->nullable();
			$table->date('registered_at');
			$table->string('image_path')->nullable();
			$table->enum('active', array('0','1'))->default('1');
			$table->unsignedInteger('user_id')->nullable();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('village_id')->references('id')->on('indonesia_villages');
			$table->foreign('user_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employees');
	}
}
