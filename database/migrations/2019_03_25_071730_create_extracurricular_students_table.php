<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtracurricularStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extracurricular_students', function (Blueprint $table) {
            $table->unsignedInteger('school_year_id');
            $table->unsignedInteger('extracurricular_id');
            $table->unsignedInteger('student_id');
            $table->timestamps();

            $table->foreign('school_year_id')->references('id')->on('school_years');
            $table->foreign('extracurricular_id')->references('id')->on('extracurriculars');
            $table->foreign('student_id')->references('id')->on('students');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extracurricular_students');
    }
}
