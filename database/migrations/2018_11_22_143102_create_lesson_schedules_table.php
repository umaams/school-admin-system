<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateLessonSchedulesTable.
 */
class CreateLessonSchedulesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lesson_schedules', function(Blueprint $table) {
            $table->increments('id');
			$table->unsignedInteger('school_year_id');
			$table->unsignedInteger('lesson_id');
			$table->unsignedInteger('teacher_id');
			$table->unsignedInteger('class_id');
			$table->unsignedInteger('day_week_id');
			$table->unsignedInteger('school_time_id');
			$table->timestamps();
			
            $table->foreign('school_year_id')->references('id')->on('school_years');
            $table->foreign('lesson_id')->references('id')->on('lessons');
            $table->foreign('teacher_id')->references('id')->on('teachers');
            $table->foreign('class_id')->references('id')->on('classes');
            $table->foreign('day_week_id')->references('id')->on('day_weeks');
            $table->foreign('school_time_id')->references('id')->on('school_times');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lesson_schedules');
	}
}
