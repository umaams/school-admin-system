<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolYearCalendars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_year_calendars', function (Blueprint $table) {
            $table->unsignedInteger('school_year_id');
            $table->date('semester_1_date_start');
            $table->date('semester_1_date_end');
            $table->date('semester_2_date_start');
            $table->date('semester_2_date_end');
            $table->timestamps();

            $table->foreign('school_year_id')->references('id')->on('school_years');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('school_year_calendars');
    }
}
