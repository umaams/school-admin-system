<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassGuardiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('class_guardians', function (Blueprint $table) {
            $table->unsignedInteger('school_year_id');
            $table->unsignedInteger('class_id');
            $table->unsignedInteger('employee_id');
            $table->timestamps();

            $table->foreign('school_year_id')->references('id')->on('school_years');
            $table->foreign('class_id')->references('id')->on('classes');
            $table->foreign('employee_id')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_guardians');
    }
}
