<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('students', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nis', 50)->unique();
			$table->string('nik', 16)->nullable();
			$table->string('name', 80);
			$table->enum('sex', array('M','F'));
			$table->string('birth_place', 50);
			$table->date('birth_date');
			$table->text('address');
			$table->char('village_id', 10);
			$table->string('zip_code', 8);
			$table->unsignedInteger('religion_id');
			$table->unsignedInteger('blood_type_id')->nullable();
			$table->integer('family_number')->default(1);
			$table->integer('family_order')->default(1);
			$table->string('longitude')->nullable();
			$table->string('latitude')->nullable();
			$table->string('father_name', 80);
			$table->enum('father_alive', array('0','1'))->default('1');
			$table->unsignedInteger('father_occupation_id');
			$table->text('father_address');
			$table->string('father_phone', 20)->nullable();
			$table->string('mother_name', 80);
			$table->enum('mother_alive', array('0','1'))->default('1');
			$table->unsignedInteger('mother_occupation_id');
			$table->text('mother_address');
			$table->string('mother_phone', 20)->nullable();
			$table->string('guardian_name', 80);
			$table->unsignedInteger('guardian_occupation_id');
			$table->text('guardian_address');
			$table->string('guardian_phone', 20)->nullable();
			$table->enum('poor_family', array('0','1'))->default('0');
			$table->date('registered_at');
			$table->unsignedInteger('registered_school_year_id');
			$table->unsignedInteger('registered_grade_id');
			$table->string('old_school', 50)->nullable();
			$table->string('image_path')->nullable();
			$table->enum('active', array('0','1'))->default('1');
			$table->unsignedInteger('user_id')->nullable();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('village_id')->references('id')->on('indonesia_villages');
			$table->foreign('religion_id')->references('id')->on('religions');
			$table->foreign('blood_type_id')->references('id')->on('blood_types');
			$table->foreign('father_occupation_id')->references('id')->on('occupations');
			$table->foreign('mother_occupation_id')->references('id')->on('occupations');
			$table->foreign('guardian_occupation_id')->references('id')->on('occupations');
			$table->foreign('registered_school_year_id')->references('id')->on('school_years');
			$table->foreign('registered_grade_id')->references('id')->on('grades');
			$table->foreign('user_id')->references('id')->on('users');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('students');
	}

}
