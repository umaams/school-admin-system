<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Master\Student::class, function (Faker $faker) {
    $student_gender = $faker->randomElement(array('M', 'F'));
    $guardian_gender = $faker->randomElement(array('M', 'F'));
    return [
        'nis' => $faker->unique()->numberBetween(10000, 99999),
        'nik' => $faker->nik(),
        'name' => $student_gender == 'M' ? $faker->name('male') : $faker->name('female'),
        'sex' => $student_gender,
        'birth_place' => $faker->city,
        'birth_date' => $faker->date('Y-m-d', 'now'),
        'address' => $faker->address,
        'village_id' => DB::table('indonesia_villages')->where('district_id', '3516150')->inRandomOrder()->first()->id,
        'zip_code' => '61353',
        'religion_id' => DB::table('religions')->inRandomOrder()->first()->id,
        'blood_type_id' => DB::table('blood_types')->inRandomOrder()->first()->id,
        'family_number' => 1,
        'family_order' => 1,
        'father_name' => $faker->name('male'),
        'father_alive' => $faker->randomElement(array('1', '0')),
        'father_occupation_id' => DB::table('occupations')->inRandomOrder()->first()->id,
        'father_address' => $faker->address,
        'father_phone' => $faker->phoneNumber,
        'mother_name' => $faker->name('female'),
        'mother_alive' => $faker->randomElement(array('1', '0')),
        'mother_occupation_id' => DB::table('occupations')->inRandomOrder()->first()->id,
        'mother_address' => $faker->address,
        'mother_phone' => $faker->phoneNumber,
        'guardian_name' => $guardian_gender == 'M' ? $faker->name('male') : $faker->name('female'),
        'guardian_occupation_id' => DB::table('occupations')->inRandomOrder()->first()->id,
        'guardian_address' => $faker->address,
        'guardian_phone' => $faker->phoneNumber,
        'poor_family' => $faker->randomElement(array('1', '0')),
        'registered_at' => date('Y-m-d'),
        'registered_school_year_id' => $faker->randomElement(array(1, 2, 3, 4, 5, 6)),
        'registered_grade_id' => 1,
        'old_school' => '',
        'active' => '1'
    ];
});
