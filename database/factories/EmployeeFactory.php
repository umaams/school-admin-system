<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Master\Employee::class, function (Faker $faker) {
    $employee_gender = $faker->randomElement(array('M', 'F'));
    return [
        'nip' => $faker->unique()->numberBetween(10000, 99999),
        'nik' => $faker->nik(),
        'name' => $employee_gender == 'M' ? $faker->name('male') : $faker->name('female'),
        'sex' => $employee_gender,
        'birth_place' => $faker->city,
        'birth_date' => $faker->date('Y-m-d', 'now'),
        'address' => $faker->address,
        'village_id' => DB::table('indonesia_villages')->where('district_id', '3516150')->inRandomOrder()->first()->id,
        'zip_code' => '61353',
        'phone' => $faker->phoneNumber,
        'religion_id' => DB::table('religions')->inRandomOrder()->first()->id,
        'registered_at' => date('Y-m-d'),
        'active' => '1'
    ];
});
