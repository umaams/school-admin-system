<?php

namespace App\Services;

use App\Repositories\EventRepository;
use App\Repositories\CalendarRepository;
use App\Repositories\SchoolYearRepository;
use App\Models\SchoolYear;

class EventService
{
	public function __construct (EventRepository $event)
	{
		$this->event = $event;
		$this->school_year = new SchoolYearRepository(new SchoolYear);
	}

	public function showOnActiveSchoolYear()
	{
		return $this->event->showOnSchoolYear($this->school_year->findActiveId());
	}
}