<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Repositories\SchoolYearRepository;

class SchoolYearService
{

	public function __construct (SchoolYearRepository $school_year)
	{
		$this->school_year = $school_year;
	}

	public function findActiveId()
	{
		return $this->school_year->findActiveId();
	}
}