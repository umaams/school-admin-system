<?php

namespace App\Services;

use App\Repositories\ClassRepository;
use App\Repositories\SchoolYearRepository;
use App\Models\SchoolYear;

class ClassService
{
	public function __construct (ClassRepository $class)
	{
		$this->class = $class;
		$this->school_year = new SchoolYearRepository(new SchoolYear);
	}

	public function showStudentAttendances()
	{
		return $this->class->showOnSchoolYear($this->school_year->findActiveId());
	}
}