<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Repositories\CalendarRepository;
use App\Repositories\SchoolYearRepository;
use App\Repositories\EventRepository;
use App\Models\SchoolYear;
use App\Models\Event;
use Carbon\Carbon;

class CalendarService
{
	private $school_year;
	private $event;

	public function __construct (CalendarRepository $calendar)
	{
		$this->calendar = $calendar;
		$this->school_year = new SchoolYearRepository(new SchoolYear);
		$this->event = new EventRepository(new Event);
	}

	public function update(Request $request)
	{
		$this->school_year->deleteHoliday($this->school_year->findActiveId());
		$attributes = array();
		foreach ($request->day_week_id as $value) {
			array_push($attributes, ['day_week_id' => $value]);
		}
		$this->school_year->insertHoliday($this->school_year->findActiveId(), $attributes);
		$this->event->deleteHolidayOnSchoolYear($this->school_year->findActiveId());
		$date_start = Carbon::parse($request->semester_1_date_start);
		$date_end = Carbon::parse($request->semester_2_date_end);
		while ($date_start->lte($date_end)) {
			if (in_array((($date_start->format('N') + 1) % 7 != 0) ? ($date_start->format('N') + 1) % 7 : 7, $request->day_week_id)) {
				$this->event->create([
					'name' => 'Hari Libur',
					'school_year_id' => $this->school_year->findActiveId(),
					'event_category_id' => 4,
					'visibility' => '0',
					'date_start' => $date_start->format('Y-m-d'),
					'date_end' => $date_start->format('Y-m-d')
				]);
			}
			$date_start = $date_start->addDay();
		}
		return $this->calendar->update($this->school_year->findActiveId(), $request->all());
	}
}