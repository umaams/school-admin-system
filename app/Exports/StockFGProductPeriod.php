<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class StockFGProductPeriod implements FromView, WithEvents
{
    protected $result;

    public function __construct($result)
    {
        $this->result = $result;
    }

    public function view(): View
    {
        return view('exports.pdf.fg_stock_product_period', [
            'result' => $this->result
        ]);
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $event->sheet->getDelegate()->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
                $event->sheet->getDelegate()->getPageSetup()->setFitToWidth(1);
                $event->sheet->getDelegate()->getPageMargins()->setTop(0.1);
                $event->sheet->getDelegate()->getPageMargins()->setLeft(0.3);
                $event->sheet->getDelegate()->getPageMargins()->setRight(0.1);
                $event->sheet->getDelegate()->getPageMargins()->setBottom(0.1);
                $event->sheet->getDelegate()->setShowGridLines(false);

                $rowTitle = 1;
                $rowParameter = 5;
                $rowHeaderTable = 2;
                $rowHeader = $rowTitle + $rowParameter;
                $rowDataTable = $this->result['data']->count();
                $rowFooterTable = 1;
                $rowFooter = 2;
                
                $columnSheet = 'M';

                $rowSheet = $rowTitle + $rowParameter + $rowHeaderTable + $rowDataTable + $rowFooterTable + $rowFooter;

                /** Styling whole sheet */
                $styleArray = [
                    'font' => [
                        'name' => 'Arial',
                        'size' => 8
                    ]
                ];
                $event->sheet->getDelegate()->getStyle('A1:'.$columnSheet.$rowSheet)->applyFromArray($styleArray);

                /** Styling title */
                $styleArray = [
                    'font' => [
                        'bold' => true,
                        'size' => 12
                    ],
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ]
                ];
                $event->sheet->getDelegate()->getStyle('A1:'.$columnSheet.'1')->applyFromArray($styleArray);
                
                /** Styling period */
                $styleArray = [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    ]
                ];
                $event->sheet->getDelegate()->getStyle('A2:'.$columnSheet.'2')->applyFromArray($styleArray);

                /** Styling table */
                $styleArray = [
                    'borders' => [
                        'allBorders' => [
                            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                            'color' => ['rgb' => '000000'],
                        ]
                    ]
                ];
                $event->sheet->getDelegate()->getStyle('A'.($rowHeader + 1).':'.$columnSheet.($rowHeader + $rowHeaderTable + $rowDataTable + $rowFooterTable))->applyFromArray($styleArray);

                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(20);
                $event->sheet->getDelegate()->getColumnDimension('C')->setWidth(15);
                $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(15);
                $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(15);
                $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(5);
                $event->sheet->getDelegate()->getColumnDimension('G')->setWidth(5);
                $event->sheet->getDelegate()->getColumnDimension('H')->setWidth(5);
                $event->sheet->getDelegate()->getColumnDimension('I')->setWidth(5);
                $event->sheet->getDelegate()->getColumnDimension('J')->setWidth(5);
                $event->sheet->getDelegate()->getColumnDimension('K')->setWidth(5);
                $event->sheet->getDelegate()->getColumnDimension('L')->setWidth(5);
                $event->sheet->getDelegate()->getColumnDimension('M')->setWidth(5);

                /* Styling Header table */
                $styleArray = [
                    'alignment' => [
                        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER
                    ]
                ];
                $event->sheet->getDelegate()->getStyle('A'.($rowHeader + 1).':'.$columnSheet.($rowHeader + $rowHeaderTable))->applyFromArray($styleArray);

                /* Styling data table */
                foreach ($this->result['data'] as $i => $data) {
                    $event->sheet->getDelegate()->getStyle('B'.($i + $rowHeader + $rowHeaderTable + 1).':B'.($i + $rowHeader + $rowHeaderTable + 1))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $event->sheet->getDelegate()->getStyle('C'.($i + $rowHeader + $rowHeaderTable + 1).':C'.($i + $rowHeader + $rowHeaderTable + 1))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $event->sheet->getDelegate()->getStyle('E'.($i + $rowHeader + $rowHeaderTable + 1).':E'.($i + $rowHeader + $rowHeaderTable + 1))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                    $event->sheet->getDelegate()->getStyle('F'.($i + $rowHeader + $rowHeaderTable + 1).':M'.($i + $rowHeader + $rowHeaderTable + 1))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                    $event->sheet->getDelegate()->getStyle('F'.($i + $rowHeader + $rowHeaderTable + 1).':F'.($i + $rowHeader + $rowHeaderTable + 1))->getNumberFormat()->setFormatCode('#,##0');
                    $event->sheet->getDelegate()->getStyle('G'.($i + $rowHeader + $rowHeaderTable + 1).':G'.($i + $rowHeader + $rowHeaderTable + 1))->getNumberFormat()->setFormatCode('#,##0.00');
                    $event->sheet->getDelegate()->getStyle('H'.($i + $rowHeader + $rowHeaderTable + 1).':H'.($i + $rowHeader + $rowHeaderTable + 1))->getNumberFormat()->setFormatCode('#,##0');
                    $event->sheet->getDelegate()->getStyle('I'.($i + $rowHeader + $rowHeaderTable + 1).':I'.($i + $rowHeader + $rowHeaderTable + 1))->getNumberFormat()->setFormatCode('#,##0.00');
                    $event->sheet->getDelegate()->getStyle('J'.($i + $rowHeader + $rowHeaderTable + 1).':J'.($i + $rowHeader + $rowHeaderTable + 1))->getNumberFormat()->setFormatCode('#,##0');
                    $event->sheet->getDelegate()->getStyle('K'.($i + $rowHeader + $rowHeaderTable + 1).':K'.($i + $rowHeader + $rowHeaderTable + 1))->getNumberFormat()->setFormatCode('#,##0.00');
                    $event->sheet->getDelegate()->getStyle('L'.($i + $rowHeader + $rowHeaderTable + 1).':L'.($i + $rowHeader + $rowHeaderTable + 1))->getNumberFormat()->setFormatCode('#,##0');
                    $event->sheet->getDelegate()->getStyle('M'.($i + $rowHeader + $rowHeaderTable + 1).':M'.($i + $rowHeader + $rowHeaderTable + 1))->getNumberFormat()->setFormatCode('#,##0.00');
                }

                /* Styling footer table */
                $event->sheet->getDelegate()->getStyle('A'.($i + $rowHeader + $rowHeaderTable + 1 + 1).':A'.($i + $rowHeader + $rowHeaderTable + 1 + 1))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->getDelegate()->getStyle('F'.($i + $rowHeader + $rowHeaderTable + 1 + 1).':M'.($i + $rowHeader + $rowHeaderTable + 1 + 1))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
                $event->sheet->getDelegate()->getStyle('F'.($i + $rowHeader + $rowHeaderTable + 1 + 1).':F'.($i + $rowHeader + $rowHeaderTable + 1 + 1))->getNumberFormat()->setFormatCode('#,##0');
                $event->sheet->getDelegate()->getStyle('G'.($i + $rowHeader + $rowHeaderTable + 1 + 1).':G'.($i + $rowHeader + $rowHeaderTable + 1 + 1))->getNumberFormat()->setFormatCode('#,##0.00');
                $event->sheet->getDelegate()->getStyle('H'.($i + $rowHeader + $rowHeaderTable + 1 + 1).':H'.($i + $rowHeader + $rowHeaderTable + 1 + 1))->getNumberFormat()->setFormatCode('#,##0');
                $event->sheet->getDelegate()->getStyle('I'.($i + $rowHeader + $rowHeaderTable + 1 + 1).':I'.($i + $rowHeader + $rowHeaderTable + 1 + 1))->getNumberFormat()->setFormatCode('#,##0.00');
                $event->sheet->getDelegate()->getStyle('J'.($i + $rowHeader + $rowHeaderTable + 1 + 1).':J'.($i + $rowHeader + $rowHeaderTable + 1 + 1))->getNumberFormat()->setFormatCode('#,##0');
                $event->sheet->getDelegate()->getStyle('K'.($i + $rowHeader + $rowHeaderTable + 1 + 1).':K'.($i + $rowHeader + $rowHeaderTable + 1 + 1))->getNumberFormat()->setFormatCode('#,##0.00');
                $event->sheet->getDelegate()->getStyle('L'.($i + $rowHeader + $rowHeaderTable + 1 + 1).':L'.($i + $rowHeader + $rowHeaderTable + 1 + 1))->getNumberFormat()->setFormatCode('#,##0');
                $event->sheet->getDelegate()->getStyle('M'.($i + $rowHeader + $rowHeaderTable + 1 + 1).':M'.($i + $rowHeader + $rowHeaderTable + 1 + 1))->getNumberFormat()->setFormatCode('#,##0.00');
            },
        ];
    }
}
