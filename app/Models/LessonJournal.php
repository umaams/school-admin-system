<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class LessonJournal.
 *
 * @package namespace App\Models;
 */
class LessonJournal extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['journal_date', 'lesson_schedule_id', 'description'];

    protected $dates = [
        'journal_date',
        'created_at',
        'updated_at'
    ];
}
