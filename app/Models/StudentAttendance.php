<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class StudentAttendance.
 *
 * @package namespace App\Models;
 */
class StudentAttendance extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'student_attendances';

    protected $fillable = [
        'school_year_id', 'semester_id', 'student_id', 'att_date', 'log_date'
    ];

}
