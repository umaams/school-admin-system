<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Event.
 *
 * @package namespace App\Models;
 */
class Event extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'events';

    protected $fillable = [
        'name', 'event_category_id', 'school_year_id', 'date_start', 'date_end', 'visibility'
    ];

    protected $dates = [
        'date_start',
        'date_end',
        'created_at',
        'updated_at'
    ];

    
    public function event_category()
    {
        return $this->belongsTo('App\Models\Master\EventCategory', 'event_category_id');
    }

}
