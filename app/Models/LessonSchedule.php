<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class LessonSchedule.
 *
 * @package namespace App\Models;
 */
class LessonSchedule extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lesson_schedules';

    protected $fillable = [
        'school_year_id', 'day_week_id', 'school_time_id', 'lesson_id', 'teacher_id', 'class_id'
    ];

    public function lesson()
    {
        return $this->belongsTo('App\Models\Master\Lesson', 'lesson_id');
    }

    public function school_time()
    {
        return $this->belongsTo('App\Models\Master\SchoolTime', 'school_time_id');
    }

    public function teacher()
    {
        return $this->belongsTo('App\Models\Master\Teacher', 'teacher_id');
    }

    public function lesson_journal()
    {
        return $this->hasOne('App\Models\Master\LessonJournal', 'lesson_schedule_id');
    }
}
