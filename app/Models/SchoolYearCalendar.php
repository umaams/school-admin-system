<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class SchoolYearCalendar.
 *
 * @package namespace App\Models;
 */
class SchoolYearCalendar extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'school_year_calendars';

    protected $fillable = [
        'school_year_id',
        'semester_1_date_start',
        'semester_1_date_end',
        'semester_2_date_start',
        'semester_2_date_end'
    ];

    protected $dates = [
        'semester_1_date_start',
        'semester_1_date_end',
        'semester_2_date_start',
        'semester_2_date_end',
        'created_at',
        'updated_at'
    ];

}
