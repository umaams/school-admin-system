<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class StudentPermit.
 *
 * @package namespace App\Models;
 */
class StudentPermit extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'student_permits';

    protected $fillable = [
        'permit_category_id', 'school_year_id', 'student_id', 'date_start', 'date_end', 'description'
    ];

    protected $dates = [
        'date_start',
        'date_end',
        'created_at',
        'updated_at'
    ];

    public function student()
    {
        return $this->belongsTo('App\Models\Master\Student', 'student_id');
    }
    
    public function permit_category()
    {
        return $this->belongsTo('App\Models\Master\PermitCategory', 'permit_category_id');
    }

}
