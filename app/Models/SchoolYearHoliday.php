<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolYearHoliday extends Model
{
    protected $table = 'school_year_holidays';

    protected $primaryKey = null;
    public $incrementing = false;

    protected $fillable = [
        'school_year_id',
        'day_week_id'
    ];
}
