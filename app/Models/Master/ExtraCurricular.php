<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ExtraCurricular.
 *
 * @package namespace App\Models;
 */
class ExtraCurricular extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'extracurriculars';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'coach', 'event_time', 'description', 'active'];


    public function students()
    {
        return $this->belongsToMany('App\Models\Master\Student', 'extracurricular_students', 'extracurricular_id', 'student_id')->withTimestamps();
    }
}
