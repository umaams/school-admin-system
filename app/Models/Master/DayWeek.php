<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class DayWeek.
 *
 * @package namespace App\Models;
 */
class DayWeek extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'day_weeks';

    public function holiday()
    {
        return $this->hasOne('App\Models\SchoolYearHoliday', 'day_week_id');
    }
}
