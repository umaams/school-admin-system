<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Lesson.
 *
 * @package namespace App\Models;
 */
class Lesson extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lessons';

    protected $fillable = [
        'code', 'name', 'lesson_category_id', 'active', 'allocation'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords($value);
    }

    public function lesson_category()
    {
        return $this->belongsTo('App\Models\Master\LessonCategory', 'lesson_category_id');
    }

    public function grades()
    {
        return $this->belongsToMany('App\Models\Master\Grade', 'curriculum_details', 'lesson_id', 'grade_id')->withPivot('allocation')->withTimestamps();
    }

}
