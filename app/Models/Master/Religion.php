<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Religion.
 *
 * @package namespace App\Models;
 */
class Religion extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public function employees()
    {
        return $this->hasMany('App\Models\Master\Employee');
    }

    public function students()
    {
        return $this->hasMany('App\Models\Master\Student');
    }
}
