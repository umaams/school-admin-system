<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class SchoolYear.
 *
 * @package namespace App\Models;
 */
class SchoolYear extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'start_year', 'end_year', 'active', 'day_week_id'
    ];

    public function curriculums()
    {
        return $this->belongsToMany('App\Models\Master\Curriculum', 'school_year_curriculums', 'school_year_id', 'curriculum_id')->withTimestamps();
    }

    public function calendar()
    {
    	return $this->hasOne('App\Models\SchoolYearCalendar', 'school_year_id');
    }

    public function holidays()
    {
        return $this->hasMany('App\Models\SchoolYearHoliday', 'school_year_id');
    }

    public function classes()
    {
        return $this->belongsToMany('App\Models\Master\Classes', 'class_students', 'school_year_id', 'class_id');
    }
}
