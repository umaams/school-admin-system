<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Occupation.
 *
 * @package namespace App\Models;
 */
class Occupation extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

	public function father_students()
    {
        return $this->hasMany('App\Models\Master\Student', 'father_occupation_id');
    }

    public function mother_students()
    {
        return $this->hasMany('App\Models\Master\Student', 'mother_occupation_id');
    }

    public function guardian_students()
    {
        return $this->hasMany('App\Models\Master\Student', 'guardian_occupation_id');
    }
}
