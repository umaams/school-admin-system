<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Employee.
 *
 * @package namespace App\Models;
 */
class Employee extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'employees';

    protected $fillable = [
        'nip', 'nik', 'name', 'sex', 'birth_place', 'birth_date', 'address', 'village_id', 'zip_code', 'religion_id', 'registered_at', 'image_path', 'active', 'phone'
    ];

    protected $dates = [
        'birth_date',
        'registered_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords($value);
    }

    public function setBirthPlaceAttribute($value)
    {
        $this->attributes['birth_place'] = ucwords($value);
    }

    public function religion()
    {
        return $this->belongsTo('App\Models\Master\Religion', 'religion_id');
    }

    public function village()
    {
        return $this->belongsTo('App\Models\Master\Village', 'village_id');
    }

    public function teacher()
    {
        return $this->hasOne('App\Models\Master\Teacher', 'employee_id');
    }

    public function employee_educations()
    {
        return $this->hasMany('App\Models\Master\EmployeeEducation', 'employee_id');
    }

}
