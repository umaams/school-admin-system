<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Teacher.
 *
 * @package namespace App\Models;
 */
class Teacher extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'teachers';

    protected $fillable = [
        'nuptk', 'code', 'employee_id'
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function employee()
    {
        return $this->belongsTo('App\Models\Master\Employee', 'employee_id');
    }

    public function lessons()
    {
        return $this->belongsToMany('App\Models\Master\Lesson', 'teacher_lessons', 'teacher_id', 'lesson_id')->withTimestamps();
    }
}
