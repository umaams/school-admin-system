<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Village.
 *
 * @package namespace App\Models;
 */
class Village extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'indonesia_villages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public function employees()
    {
        return $this->hasMany('App\Models\Master\Employee');
    }

	public function students()
    {
        return $this->hasMany('App\Models\Master\Student');
    }
}
