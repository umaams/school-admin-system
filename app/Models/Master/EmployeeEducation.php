<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class EmployeeEducation.
 *
 * @package namespace App\Models;
 */
class EmployeeEducation extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'employee_educations';

    protected $fillable = [
        'employee_id', 'education_id', 'place', 'department', 'year_start', 'year_end', 'certificate_number', 'gpa'
    ];

    public function employee()
    {
        return $this->belongsTo('App\Models\Master\Employee', 'employee_id');
    }

    public function education()
    {
        return $this->belongsTo('App\Models\Master\Education', 'education_id');
    }

}
