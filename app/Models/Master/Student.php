<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Student.
 *
 * @package namespace App\Models;
 */
class Student extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nis', 'nik', 'name', 'sex', 'birth_place', 'birth_date', 'address', 'village_id', 'zip_code', 'religion_id', 'blood_type_id', 'family_number', 'family_order', 'longitude', 'latitude', 'father_name', 'father_alive', 'father_address', 'father_occupation_id', 'father_phone', 'mother_name', 'mother_alive', 'mother_address', 'mother_occupation_id', 'mother_phone', 'guardian_name', 'guardian_address', 'guardian_occupation_id', 'guardian_phone', 'poor_family', 'registered_at', 'registered_school_year_id', 'registered_grade_id', 'old_school', 'image_path', 'active'
    ];

    protected $dates = [
        'birth_date',
        'registered_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords($value);
    }

    public function setBirthPlaceAttribute($value)
    {
        $this->attributes['birth_place'] = ucwords($value);
    }

    public function setFatherNameAttribute($value)
    {
        $this->attributes['father_name'] = ucwords($value);
    }

    public function setMotherNameAttribute($value)
    {
        $this->attributes['mother_name'] = ucwords($value);
    }

    public function setGuardianNameAttribute($value)
    {
        $this->attributes['guardian_name'] = ucwords($value);
    }

    public function religion()
    {
        return $this->belongsTo('App\Models\Master\Religion', 'religion_id');
    }

    public function blood_type()
    {
        return $this->belongsTo('App\Models\Master\Religion', 'blood_type_id');
    }

    public function village()
    {
        return $this->belongsTo('Laravolt\Indonesia\Models\Village', 'village_id');
    }

    public function father_occupation()
    {
        return $this->belongsTo('App\Models\Master\Occupation', 'father_occupation_id');
    }

    public function mother_occupation()
    {
        return $this->belongsTo('App\Models\Master\Occupation', 'mother_occupation_id');
    }

    public function guardian_occupation()
    {
        return $this->belongsTo('App\Models\Master\Occupation', 'guardian_occupation_id');
    }

    public function registered_school_year()
    {
        return $this->belongsTo('App\Models\Master\SchoolYear', 'registered_school_year_id');
    }

    public function registered_grade()
    {
        return $this->belongsTo('App\Models\Master\Grade', 'registered_grade_id');
    }

    public function classes()
    {
        return $this->belongsToMany('App\Models\Master\Classes', 'class_students', 'student_id', 'class_id')->withTimestamps();
    }

    public function extracurriculars()
    {
        return $this->belongsToMany('App\Models\Master\Extracurriculars', 'extracurricular_students', 'student_id', 'extracurricular_id')->withTimestamps();
    }

    public function attendances()
    {
        return $this->hasMany('App\Models\StudentAttendance', 'student_id');
    }

    public function student_permits()
    {
        return $this->hasMany('App\Models\StudentPermit', 'student_id');
    }
}
