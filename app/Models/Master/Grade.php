<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Grade.
 *
 * @package namespace App\Models;
 */
class Grade extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public function lessons()
    {
        return $this->belongsToMany('App\Models\Master\Lesson', 'curriculum_details', 'grade_id', 'lesson_id')->withPivot('allocation', 'kkm')->withTimestamps();
    }

    public function classes()
    {
        return $this->hasMany('App\Models\Master\Classes', 'grade_id');
    }
}
