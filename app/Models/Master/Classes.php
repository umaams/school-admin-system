<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Classes extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'classes';

    protected $fillable = [
        'name', 'grade_id', 'school_year_id', 'active'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucwords($value);
    }

    public function grade()
    {
        return $this->belongsTo('App\Models\Master\Grade', 'grade_id');
    }

    public function students()
    {
        return $this->belongsToMany('App\Models\Master\Student', 'class_students', 'class_id', 'student_id')->withTimestamps();
    }

    public function guardian()
    {
        return $this->belongsToMany('App\Models\Master\Employee', 'class_guardians', 'class_id', 'employee_id')->withTimestamps();
    }

    public function lesson_schedules()
    {
        return $this->hasMany('App\Models\LessonSchedule', 'class_id');
    }

    public function lessons()
    {
        return $this->belongsToMany('App\Models\Master\Lesson', 'lesson_schedules', 'class_id', 'lesson_id');
    }

    public function teachers()
    {
        return $this->belongsToMany('App\Models\Master\Teacher', 'lesson_schedules', 'class_id', 'teacher_id');
    }

    public function school_years()
    {
        return $this->belongsToMany('App\Models\Master\SchoolYear', 'class_students', 'class_id', 'school_year_id');
    }
}
