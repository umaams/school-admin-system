<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Curriculum extends Model implements Transformable
{
    use TransformableTrait;
    
    protected $table = 'curriculums';

    protected $fillable = [
        'name'
    ];

    public function lessons()
    {
        return $this->belongsToMany('App\Models\Master\Lesson', 'curriculum_details', 'curriculum_id', 'lesson_id');
    }
}
