<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class TeacherRepository.
 *
 * @package namespace App\Repositories;
 */
class TeacherRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\Teacher";
    }
}
