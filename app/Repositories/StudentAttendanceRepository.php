<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class StudentAttendanceRepository.
 *
 * @package namespace App\Repositories;
 */
class StudentAttendanceRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\StudentAttendance";
    }
}
