<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class LessonRepository.
 *
 * @package namespace App\Repositories;
 */
class LessonRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\Lesson";
    }

    public function whereDoesntHave($relation, $closure)
    {
        $this->model = $this->model->whereDoesntHave($relation, $closure);
        return $this;
    }
}
