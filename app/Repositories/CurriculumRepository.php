<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class CurriculumRepository.
 *
 * @package namespace App\Repositories;
 */
class CurriculumRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Curriculum";
    }
}
