<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class LessonJournalRepository.
 *
 * @package namespace App\Repositories;
 */
class LessonJournalRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\LessonJournal";
    }
}
