<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class EventRepository.
 *
 * @package namespace App\Repositories;
 */
class EventRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Event";
    }
}
