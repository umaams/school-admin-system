<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class SchoolYearRepository.
 *
 * @package namespace App\Repositories;
 */
class SchoolYearRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\SchoolYear";
    }

    function findActiveId()
    {
        return $this->model->where('active', '1')->first()->id;
    }
}
