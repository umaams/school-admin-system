<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class StudentPermitRepository.
 *
 * @package namespace App\Repositories;
 */
class StudentPermitRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "App\\Models\\StudentPermit";
    }
}
