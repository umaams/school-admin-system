<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class EducationRepository.
 *
 * @package namespace App\Repositories;
 */
class EducationRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\Education";
    }
}
