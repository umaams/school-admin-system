<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class EmployeeRepository.
 *
 * @package namespace App\Repositories;
 */
class EmployeeRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\Employee";
    }
}
