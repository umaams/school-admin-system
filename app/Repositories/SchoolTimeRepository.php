<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class SchoolTimeRepository.
 *
 * @package namespace App\Repositories;
 */
class SchoolTimeRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\SchoolTime";
    }
}
