<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class OccupationRepository.
 *
 * @package namespace App\Repositories;
 */
class OccupationRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\Occupation";
    }
}
