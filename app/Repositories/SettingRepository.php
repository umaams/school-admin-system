<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class SettingRepository.
 *
 * @package namespace App\Repositories;
 */
class SettingRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Setting";
    }
}
