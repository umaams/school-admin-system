<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class EmployeeEducationRepository.
 *
 * @package namespace App\Repositories;
 */
class EmployeeEducationRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\EmployeeEducation";
    }
}
