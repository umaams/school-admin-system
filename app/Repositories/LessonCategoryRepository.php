<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class LessonCategoryRepository.
 *
 * @package namespace App\Repositories;
 */
class LessonCategoryRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\LessonCategory";
    }
}
