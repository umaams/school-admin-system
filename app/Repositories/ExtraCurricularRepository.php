<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class ExtraCurricularRepository.
 *
 * @package namespace App\Repositories;
 */
class ExtraCurricularRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\ExtraCurricular";
    }
}
