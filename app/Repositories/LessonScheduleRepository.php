<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class LessonScheduleRepository.
 *
 * @package namespace App\Repositories;
 */
class LessonScheduleRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\LessonSchedule";
    }
}
