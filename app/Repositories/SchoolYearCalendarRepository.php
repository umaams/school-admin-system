<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class SchoolYearCalendarRepository.
 *
 * @package namespace App\Repositories;
 */
class SchoolYearCalendarRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\SchoolYearCalendar";
    }
}
