<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class BloodTypeRepository.
 *
 * @package namespace App\Repositories;
 */
class BloodTypeRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\BloodType";
    }
}
