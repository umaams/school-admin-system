<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class ClassRepository.
 *
 * @package namespace App\Repositories;
 */
class ClassRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\Classes";
    }
}
