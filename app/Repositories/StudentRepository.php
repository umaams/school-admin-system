<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class StudentRepository.
 *
 * @package namespace App\Repositories;
 */
class StudentRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\Student";
    }

    public function whereDoesntHave($relation, $closure)
    {
        $this->model = $this->model->whereDoesntHave($relation, $closure);
        return $this;
    }
}
