<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class ReligionRepository.
 *
 * @package namespace App\Repositories;
 */
class ReligionRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\Religion";
    }
}
