<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class PermitCategoryRepository.
 *
 * @package namespace App\Repositories;
 */
class PermitCategoryRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\PermitCategory";
    }
}
