<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class VillageRepository.
 *
 * @package namespace App\Repositories;
 */
class VillageRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\Village";
    }
}
