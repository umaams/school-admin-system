<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class GradeRepository.
 *
 * @package namespace App\Repositories;
 */
class GradeRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\Grade";
    }
}
