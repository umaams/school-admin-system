<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class EventCategoryRepository.
 *
 * @package namespace App\Repositories;
 */
class EventCategoryRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\EventCategory";
    }
}
