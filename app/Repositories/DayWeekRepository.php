<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class DayWeekRepository.
 *
 * @package namespace App\Repositories;
 */
class DayWeekRepository extends BaseRepository
{
    function model()
    {
        return "App\\Models\\Master\\DayWeek";
    }
}
