<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AttendanceEdit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'att_date' => 'required|date',
            'class_id' => 'required|exists:classes,id'
        ];
    }

    public function attributes()
    {
        return [
            'att_date' => 'tanggal',
            'class_id' => 'kelas'
        ];
    }
}
