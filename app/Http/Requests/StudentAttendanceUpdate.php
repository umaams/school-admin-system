<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentAttendanceUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'students.*.student_id' => 'required|exists:students,id',
            'students.*.att_status' => 'required|in:1,0',
            'att_date' => 'required|date'
        ];
    }

    public function attributes()
    {
        return [
            'students.*.student_id' => 'siswa',
            'students.*.att_status' => 'status absensi',
            'att_date' => 'tanggal absensi'
        ];
    }
}
