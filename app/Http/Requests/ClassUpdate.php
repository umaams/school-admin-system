<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClassUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'grade_id' => 'required|exists:grades,id',
            'active' => 'required|in:1,0'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nama',
            'grade_id' => 'tingkat',
            'active' => 'status'
        ];
    }
}
