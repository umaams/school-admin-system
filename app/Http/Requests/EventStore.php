<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'event_category_id' => 'required|exists:event_categories,id',
            'date_start' => 'required|date|before_or_equal:date_end',
            'date_end' => 'required|date|after_or_equal:date_start'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nama',
            'event_category_id' => 'kategori',
            'date_start' => 'tanggal mulai',
            'date_end' => 'tanggal akhir'
        ];
    }
}
