<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Repositories\LessonScheduleRepository;
use App\Repositories\SchoolYearRepository;

class LessonScheduleStore extends FormRequest
{
    protected $repository, $schoolYearRepository;

    public function __construct(LessonScheduleRepository $repository, SchoolYearRepository $schoolYearRepository)
    {
        $this->repository = $repository;
        $this->schoolYearRepository = $schoolYearRepository;
    }    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        $validator->after(function ($validator) {
            $oldtime = $this->repository->findWhere([
                'school_year_id' => $this->schoolYearRepository->findActiveId(),
                'class_id' => $this->route('id'),
                'day_week_id' => $this->request->get('day_week_id'),
                'school_time_id' => $this->request->get('school_time_id')
            ])->first();
            if ($oldtime) {
                $validator->errors()->add('day_week_id', 'Hari bentrok dengan hari lain');
                $validator->errors()->add('school_time_id', 'Jam pelajaran bentrok dengan jam lain');
            }
            $oldteacher = $this->repository->findWhere([
                'school_year_id' => $this->schoolYearRepository->findActiveId(),
                'teacher_id' => $this->request->get('teacher_id'),
                'day_week_id' => $this->request->get('day_week_id'),
                'school_time_id' => $this->request->get('school_time_id')
            ])->first();
            if ($oldteacher) {
                $validator->errors()->add('teacher_id', 'Guru sudah punya jadwal lain');
            }
        });
        return $validator;
    }   

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lesson_id' => 'required|exists:lessons,id',
            'teacher_id' => 'required|exists:teachers,id',
            'day_week_id' => 'required|exists:day_weeks,id',
            'school_time_id' => 'required|exists:school_times,id'
        ];
    }

    public function attributes()
    {
        return [
            'day_week_id' => 'hari',
            'school_time_id' => 'jam pelajaran',
            'lesson_id' => 'pelajaran',
            'teacher_id' => 'guru'
        ];
    }
}
