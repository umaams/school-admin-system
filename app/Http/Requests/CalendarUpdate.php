<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CalendarUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'semester_1_date_start' => 'required|date|before_or_equal:semester_1_date_end',
            'semester_1_date_end' => 'required|date|after_or_equal:semester_1_date_start|before:semester_2_date_start',
            'semester_2_date_start' => 'required|date|after:semester_1_date_end|before_or_equal:semester_2_date_end',
            'semester_2_date_end' => 'required|date|after_or_equal:semester_2_date_start',
            'day_week_id' => 'required|array'
        ];
    }

    public function attributes()
    {
        return [
            'semester_1_date_start' => 'tanggal mulai semester ganjil',
            'semester_1_date_end' => 'tanggal akhir semester ganjil',
            'semester_2_date_start' => 'tanggal awal semester genap',
            'semester_2_date_end' => 'tanggal akhir semester genap',
            'day_week_id' => 'hari libur'
        ];
    }
}
