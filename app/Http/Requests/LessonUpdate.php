<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LessonUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|unique:lessons,code,'.$this->route()->parameters()['lesson'].',id',
            'name' => 'required',
            'active' => 'required|in:1,0'
        ];
    }

    public function attributes()
    {
        return [
            'code' => 'kode',
            'name' => 'nama',
            'active' => 'status'
        ];
    }
}
