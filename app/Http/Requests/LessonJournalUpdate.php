<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LessonJournalUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'journal_date' => 'required|date',
            'lesson_schedule_id' => 'required|exists:lesson_schedules,id',
            'description' => 'required'
        ];
    }
}
