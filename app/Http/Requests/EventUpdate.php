<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->route()->parameters()['event'];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'event_category_id' => 'required|exists:event_categories,id',
            'date_start' => 'required|date',
            'date_end' => 'required|date'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nama',
            'event_category_id' => 'kategori',
            'date_start' => 'tanggal mulai',
            'date_end' => 'tanggal akhir'
        ];
    }
}
