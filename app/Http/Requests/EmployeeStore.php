<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nip' => 'required|unique:employees,nip',
            'name' => 'required',
            'sex' => 'required|in:M,F',
            'birth_place' => 'required',
            'birth_date' => 'required|date',
            'address' => 'required',
            'village_id' => 'required|exists:indonesia_villages,id',
            'zip_code' => 'required|numeric',
            'religion_id' => 'required|exists:religions,id',
            'registered_at' => 'required|date'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nama',
            'sex' => 'jenis kelamin',
            'birth_place' => 'tempat lahir',
            'birth_date' => 'tanggal lahir',
            'address' => 'alamat',
            'village_id' => 'desa',
            'zip_code' => 'kode pos',
            'religion_id' => 'agama',
            'registered_at' => 'tanggal terdaftar'
        ];
    }
}
