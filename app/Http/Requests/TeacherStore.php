<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeacherStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nuptk' => 'required|unique:teachers,nuptk',
            'code' => 'required|unique:teachers,code',
            'employee_id' => 'required|exists:employees,id'
        ];
    }

    public function attributes()
    {
        return [
            'code' => 'kode',
            'employee_id' => 'pegawai'
        ];
    }
}
