<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExtraCurricularUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'coach' => 'required',
            'event_time' => 'required',
            'active' => 'required|in:1,0'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nama',
            'coach' => 'pembina',
            'event_time' => 'waktu kegiatan',
            'active' => 'status'
        ];
    }
}
