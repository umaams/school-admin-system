<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Repositories\StudentPermitRepository;
use App\Repositories\SchoolYearRepository;

class StudentPermitStore extends FormRequest
{
    protected $repository, $schoolYearRepository;

    public function __construct(StudentPermitRepository $repository, SchoolYearRepository $schoolYearRepository)
    {
        $this->repository = $repository;
        $this->schoolYearRepository = $schoolYearRepository;
    }    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
        $validator->after(function ($validator) {
            $oldpermit = $this->repository->findWhere([
                'school_year_id' => $this->schoolYearRepository->findActiveId(),
                'student_id' => $this->request->get('student_id'),
                ['date_start', '<=', $this->request->get('date_start')], [
                'date_end', '>=', $this->request->get('date_start')]
            ])->first();
            if ($oldpermit) {
                $validator->errors()->add('date_start', 'Tanggal mulai ijin bentrok dengan tanggal ijin lain');
            }
            $oldpermit = $this->repository->findWhere([
                'school_year_id' => $this->schoolYearRepository->findActiveId(),
                'student_id' => $this->request->get('student_id'),
                ['date_end', '>=', $this->request->get('date_end')], [
                'date_end', '<=', $this->request->get('date_end')]
            ])->first();
            if ($oldpermit) {
                $validator->errors()->add('date_end', 'Tanggal akhir ijin bentrok dengan tanggal ijin lain');
            }
        });
        return $validator;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_id' => 'required|exists:students,id',
            'permit_category_id' => 'required|exists:permit_categories,id',
            'date_start' => 'required|date|before_or_equal:date_end',
            'date_end' => 'required|date|after_or_equal:date_start',
            'description' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'student_id' => 'siswa',
            'permit_category_id' => 'kategori',
            'date_start' => 'tanggal mulai',
            'date_end' => 'tanggal akhir',
            'description' => 'keterangan'
        ];
    }
}
