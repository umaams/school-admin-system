<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nis' => 'required|unique:students,nis,'.$this->route()->parameters()['student'].',id',
            'name' => 'required',
            'sex' => 'required|in:M,F',
            'birth_place' => 'required',
            'birth_date' => 'required|date',
            'address' => 'required',
            'village_id' => 'required|exists:indonesia_villages,id',
            'zip_code' => 'required|numeric',
            'religion_id' => 'required|exists:religions,id',
            'blood_type_id' => 'required|exists:blood_types,id',
            'family_number' =>'required|numeric|min:1',
            'family_order' =>'required|numeric|min:1',
            'father_name' => 'required',
            'father_occupation_id' => 'required|exists:occupations,id',
            'father_address' => 'required',
            'mother_name' => 'required',
            'mother_occupation_id' => 'required|exists:occupations,id',
            'mother_address' => 'required',
            'guardian_name' => 'required',
            'guardian_occupation_id' => 'required|exists:occupations,id',
            'guardian_address' => 'required',
            'registered_at' => 'required|date',
            'registered_school_year_id' => 'required|exists:school_years,id',
            'registered_grade_id' => 'required|exists:grades,id'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'nama',
            'sex' => 'jenis kelamin',
            'birth_place' => 'tempat lahir',
            'birth_date' => 'tanggal lahir',
            'address' => 'alamat',
            'village_id' => 'desa',
            'zip_code' => 'kode pos',
            'religion_id' => 'agama',
            'father_name' => 'nama ayah',
            'father_occupation_id' => 'pekerjaan ayah',
            'father_address' => 'alamat ayah',
            'mother_name' => 'nama ibu',
            'mother_occupation_id' => 'pekerjaan ibu',
            'mother_address' => 'alamat ibu',
            'guardian_name' => 'nama wali',
            'guardian_occupation_id' => 'pekerjaan wali',
            'guardian_address' => 'alamat wali',
            'registered_at' => 'tanggal terdaftar',
            'registered_school_year_id' => 'tahun ajaran terdaftar',
            'registered_grade_id' => 'tingkat'
        ];
    }
}
