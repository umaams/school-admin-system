<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\SchoolYearCalendar;

class CalendarSet
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $calendar = SchoolYearCalendar::where('school_year_id', \App\Models\Master\SchoolYear::where('active', '1')->first()->id)->first();
        if (!$calendar) {
            abort(403);
        }
        return $next($request);
    }
}
