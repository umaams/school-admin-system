<?php

namespace App\Http\Middleware;

use Closure;

class GenerateMenus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Menu::make('MyNavBar', function($menu){
            $menu->add('Dashboard', ['route'  => 'home'])->data('icon', 'fas fa-home')->nickname('dashboard');

            $menu->add('Master Data')->data('icon', 'fas fa-database')->nickname('master');
            $menu->master->add('User', ['route'  => 'users.index']);
            $menu->master->add('Siswa', ['route'  => 'students.index']);
            $menu->master->add('Pegawai', ['route'  => 'employees.index']);
            $menu->master->add('Guru', ['route'  => 'teachers.index']);
            $menu->master->add('Kelompok Mapel', ['route'  => 'lesson_categories.index']);
            $menu->master->add('Mata Pelajaran', ['route'  => 'lessons.index']);
            $menu->master->add('Kelas', ['route'  => 'classes.index']);
            $menu->master->add('Jam Pelajaran', ['route'  => 'school_times.index']);
            $menu->master->add('Kurikulum', ['route'  => 'curriculums.index']);
            $menu->master->add('Ekstrakurikuler', ['route'  => 'extracurriculars.index']);

            $menu->add('Kalendar Akademik')->data('icon', 'fa fa-calendar')->nickname('calendar');
            $menu->calendar->add('Kalender', ['route'  => 'calendar.index']);
            $menu->calendar->add('Agenda', ['route'  => 'events.index']);
            $menu->calendar->add('Jadwal', ['route'  => 'lesson_schedules.index']);

            $menu->add('Absensi')->data('icon', 'fa fa-calendar')->nickname('attendance');
            $menu->attendance->add('Absensi Siswa', ['route'  => 'attendances.index']);
            $menu->attendance->add('Ijin Tidak Masuk', ['route'  => 'student_permits.index']);
            
            $menu->add('Manajemen Kelas')->data('icon', 'fa fa-folder')->nickname('classes');
            $menu->classes->add('Wali Kelas', ['route'  => 'classes.guardians.index']);
            $menu->classes->add('Rombongan Kelas', ['route'  => 'classes.students.index']);
            $menu->classes->add('Jurnal Mata Pelajaran', ['route'  => 'lesson_journals.index']);

            $menu->add('Report')->data('icon', 'fa fa-print')->nickname('report');
            $menu->report->add('Siswa', ['route'  => 'reports.students']);
            $menu->report->add('Rombongan Kelas', ['route'  => 'reports.class_students']);
            $menu->report->add('Absensi Siswa', ['route'  => 'reports.student_attendances.class']);

            $menu->add('Pengaturan')->data('icon', 'fas fa-cogs')->nickname('setting');
            $menu->setting->add('General', ['route'  => 'settings.index']);
            $menu->setting->add('Rombongan Kelas', ['route'  => 'reports.class_students']);

        });

        return $next($request);
    }
}
