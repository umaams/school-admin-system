<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Curriculum;
use App\Repositories\CurriculumRepository;
use App\Repositories\GradeRepository;
use App\Repositories\LessonRepository;

class CurriculumDetailController extends Controller
{
    protected $curriculumRepository, $gradeRepository, $lessonRepository;

    public function __construct(CurriculumRepository $curriculumRepository, GradeRepository $gradeRepository, LessonRepository $lessonRepository){
        $this->curriculumRepository = $curriculumRepository;
        $this->gradeRepository = $gradeRepository;
        $this->lessonRepository = $lessonRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id, $grade_id, Request $request)
    {
        $curriculum = $this->curriculumRepository->find($id);
        $grade = $this->gradeRepository->find($grade_id);
        $lessons = $this->lessonRepository->orderBy('name')->whereDoesntHave('grades', function ($query) use ($id, $grade_id) {
            $query->where('curriculum_id', $id)->where('grade_id', $grade_id);
        })->all();
        return view('modules.master.curriculum.add', compact('curriculum', 'grade', 'lessons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, $grade_id, Request $request)
    {
        $grade = $this->gradeRepository->find($grade_id);
        $grade->lessons()->attach($request->lesson_id, ['curriculum_id' => $id, 'allocation' => 0, 'kkm' => 0]);
        return redirect()->route('curriculums.show', ['id' => $id])->with('grade_id', $grade_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $grade_id)
    {
        $grade = $this->gradeRepository->find($grade_id);
        $grade->lessons()->wherePivot('curriculum_id', $id)->updateExistingPivot($request->lesson_id, ['kkm' => $request->kkm, 'allocation' => $request->allocation]);
        return redirect()->route('curriculums.show', ['id' => $id])->with('grade_id', $grade_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $grade_id, Request $request)
    {
        $grade = $this->gradeRepository->find($grade_id);
        $grade->lessons()->wherePivot('curriculum_id', $id)->detach($request->lesson_id);
        return redirect()->route('curriculums.show', ['id' => $id])->with('grade_id', $grade_id);
    }
}