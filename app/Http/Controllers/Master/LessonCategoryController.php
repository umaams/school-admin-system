<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Http\Requests\LessonCategoryStore;
use App\Http\Requests\LessonCategoryUpdate;
use App\Repositories\LessonCategoryRepository;

class LessonCategoryController extends Controller
{
    protected $repository;

    public function __construct(LessonCategoryRepository $repository){
        $this->repository = $repository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modules.master.lesson_category.view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.master.lesson_category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LessonCategoryStore $request)
    {
        $lesson_category = $this->repository->create($request->all());
        return redirect()->route('lesson_categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lesson_category = $this->repository->find($id);
        return view('modules.master.lesson_category.edit', compact('lesson_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LessonCategoryUpdate $request, $id)
    {
        $lesson_category = $this->repository->update($request->all(), $id);
        return redirect()->route('lesson_categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('lesson_categories.index');
    }

    public function datatable(Request $request)
    {
        return Datatables::of($this->repository->all())
        ->editColumn('created_at', function ($lesson_category) {
            return $lesson_category->created_at->diffForHumans();
        })
        ->editColumn('updated_at', function ($lesson_category) {
            return $lesson_category->updated_at->diffForHumans();
        })
        ->addColumn('action', function ($lesson_category) {
            $text = "";
            $text.= '<a href="'.route('lesson_categories.edit', ['id' => $lesson_category->id]).'" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a>';
            $text.= "<form class='form-horizontal' style='display: inline;' method='POST' action='".route('lesson_categories.destroy', ['id' => $lesson_category->id])."'><input type='hidden' name='_token' value='".csrf_token()."'> <input type='hidden' name='_method' value='DELETE'><button class='btn btn-sm btn-danger' type='submit'><i class='fas fa-trash'></i> Hapus</button></form><form>";
            return $text;
        })
        ->rawColumns(['active', 'action'])
        ->make(true);
    }
}
