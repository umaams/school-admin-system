<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Http\Requests\SchoolTimeStore;
use App\Http\Requests\SchoolTimeUpdate;
use App\Repositories\SchoolTimeRepository;

class SchoolTimeController extends Controller
{
    protected $repository;

    public function __construct(SchoolTimeRepository $repository){
        $this->repository = $repository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $school_times = $this->repository->orderBy('id')->all();
        return view('modules.master.school_time.view')->with(compact('school_times'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.master.school_time.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SchoolTimeStore $request)
    {
        $this->repository->create($request->all());
        return redirect()->route('school_times.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $school_time = $this->repository->find($id);
        return view('modules.master.school_time.edit')->with(compact('school_time'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SchoolTimeUpdate $request, $id)
    {
        $this->repository->update($request->all(), $id);
        return redirect()->route('school_times.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('school_times.index');
    }

    public function datatable(Request $request)
    {
        return Datatables::of($this->repository->all())
        ->editColumn('time_start', function ($school_time) {
            return substr($school_time->time_start, 0, 5);
        })
        ->editColumn('time_end', function ($school_time) {
            return substr($school_time->time_end, 0, 5);
        })
        ->addColumn('action', function ($school_time) {
            $text = "";
            $text.= '<a href="'.route('school_times.edit', ['id' => $school_time->id]).'" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a>';
            $text.= "<form class='form-horizontal' style='display: inline;' method='POST' action='".route('school_times.destroy', ['id' => $school_time->id])."'><input type='hidden' name='_token' value='".csrf_token()."'> <input type='hidden' name='_method' value='DELETE'><button class='btn btn-sm btn-danger' type='submit'><i class='fas fa-trash'></i> Hapus</button></form><form>";
            return $text;
        })
        ->rawColumns(['active', 'action'])
        ->make(true);
    }
}
