<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClassStore;
use App\Http\Requests\ClassUpdate;
use Yajra\Datatables\Datatables;
use App\Repositories\ClassRepository;
use App\Repositories\GradeRepository;

class ClassController extends Controller
{
    protected $repository, $gradeRepository;

    public function __construct(ClassRepository $repository, GradeRepository $gradeRepository){
        $this->repository = $repository;
        $this->gradeRepository = $gradeRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->expectsJson()) {
            $data['classes'] = $this->repository->findByField('active', '1');
            return response()->json(compact('data'));
        } else {
            return view('modules.master.class.view');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $grades = $this->gradeRepository->orderBy('id')->all();
        return view('modules.master.class.create', compact('grades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClassStore $request)
    {
        $class = $this->repository->create($request->all());
        return redirect()->route('classes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grades = $this->gradeRepository->orderBy('id')->all();
        $class = $this->repository->find($id);
        return view('modules.master.class.edit', compact('class', 'grades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClassUpdate $request, $id)
    {
        $class = $this->repository->update($request->all(), $id);
        return redirect()->route('classes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('classes.index');
    }

    public function datatable(Request $request)
    {
        return Datatables::of($this->repository->with('grade')->all())
        ->editColumn('active', function ($class) {
            return $class->active == '1' ? '<span class="text text-success">Aktif</span>' : '<span class="text text-warning">Nonaktif</span>';
        })
        ->editColumn('created_at', function ($class) {
            return $class->created_at->diffForHumans();
        })
        ->editColumn('updated_at', function ($class) {
            return $class->updated_at->diffForHumans();
        })
        ->addColumn('action', function ($class) {
            $text = "";
            $text.= '<a href="'.route('classes.edit', ['id' => $class->id]).'" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a>';
            $text.= "<form class='form-horizontal' style='display: inline;' method='POST' action='".route('classes.destroy', ['id' => $class->id])."'><input type='hidden' name='_token' value='".csrf_token()."'> <input type='hidden' name='_method' value='DELETE'><button class='btn btn-sm btn-danger' type='submit'><i class='fas fa-trash'></i> Hapus</button></form><form>";
            return $text;
        })
        ->rawColumns(['active', 'action'])
        ->make(true);
    }

    public function getActive()
    {
        $classes = $this->repository->with('grade')->all();
        return response()->json(compact('classes'));
    }
}
