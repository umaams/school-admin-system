<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Http\Requests\LessonStore;
use App\Http\Requests\LessonUpdate;
use App\Repositories\LessonRepository;
use App\Repositories\LessonCategoryRepository;

class LessonController extends Controller
{
    protected $repository, $lessonCategoryRepository;

    public function __construct(LessonRepository $repository, LessonCategoryRepository $lessonCategoryRepository){
        $this->repository = $repository;
        $this->lessonCategoryRepository = $lessonCategoryRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modules.master.lesson.view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lesson_categories = $this->lessonCategoryRepository->all();
        return view('modules.master.lesson.create', compact('lesson_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LessonStore $request)
    {
        $lesson = $this->repository->create($request->all());
        return redirect()->route('lessons.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lesson = $this->repository->find($id);
        $lesson_categories = $this->lessonCategoryRepository->all();
        return view('modules.master.lesson.edit', compact('lesson', 'lesson_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LessonUpdate $request, $id)
    {
        $lesson = $this->repository->update($request->all(), $id);
        return redirect()->route('lessons.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('lessons.index');
    }

    public function datatable(Request $request)
    {
        return Datatables::of($this->repository->with('lesson_category')->all())
        ->editColumn('lesson_category.name', function ($lesson) {
            return $lesson->lesson_category != null ? $lesson->lesson_category->name : "";
        })
        ->editColumn('active', function ($lesson) {
            return $lesson->active == '1' ? '<span class="text text-success">Aktif</span>' : '<span class="text text-warning">Nonaktif</span>';
        })
        ->editColumn('created_at', function ($lesson) {
            return $lesson->created_at->diffForHumans();
        })
        ->editColumn('updated_at', function ($lesson) {
            return $lesson->updated_at->diffForHumans();
        })
        ->addColumn('action', function ($lesson) {
            $text = "";
            $text.= '<a href="'.route('lessons.edit', ['id' => $lesson->id]).'" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a>';
            $text.= "<form class='form-horizontal' style='display: inline;' method='POST' action='".route('lessons.destroy', ['id' => $lesson->id])."'><input type='hidden' name='_token' value='".csrf_token()."'> <input type='hidden' name='_method' value='DELETE'><button class='btn btn-sm btn-danger' type='submit'><i class='fas fa-trash'></i> Hapus</button></form><form>";
            return $text;
        })
        ->rawColumns(['active', 'action'])
        ->make(true);
    }
}
