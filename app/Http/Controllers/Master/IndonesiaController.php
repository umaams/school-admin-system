<?php

namespace App\Http\Controllers\Master;
use Indonesia;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndonesiaController extends Controller
{
    public function showProvinces()
    {
        $provinces = Indonesia::allProvinces();
        return response()->json(compact('provinces'));
    }

    public function showCities($id)
    {
        $cities = Indonesia::findProvince($id, ['cities'])->cities;
        return response()->json(compact('cities'));
    }

    public function showDistricts($id)
    {
        $districts = Indonesia::findCity($id, ['districts'])->districts;
        return response()->json(compact('districts'));
    }

    public function showVillages($id)
    {
        $villages = Indonesia::findDistrict($id, ['villages'])->villages;
        return response()->json(compact('villages'));
    }
}
