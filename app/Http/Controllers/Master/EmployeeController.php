<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Http\Requests\EmployeeStore;
use App\Http\Requests\EmployeeUpdate;
use Indonesia;
use App\Repositories\EmployeeRepository;
use App\Repositories\ReligionRepository;
use App\Repositories\EducationRepository;
use App\Repositories\EmployeeEducationRepository;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller
{
    protected $repository, $religionRepository, $educationRepository, $employeeEducationRepository;

    public function __construct(EmployeeRepository $repository, ReligionRepository $religionRepository, EducationRepository $educationRepository, EmployeeEducationRepository $employeeEducationRepository){
        $this->repository = $repository;
        $this->religionRepository = $religionRepository;
        $this->educationRepository = $educationRepository;
        $this->employeeEducationRepository = $employeeEducationRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modules.master.employee.view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $religions = $this->religionRepository->orderBy('name')->all();
        $educations = $this->educationRepository->all();
        return view('modules.master.employee.create', compact('religions', 'educations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeStore $request)
    {
        \Cloudinary::config(array( 
          "cloud_name" => config('cloudinary.cloud_name'), 
          "api_key" => config('cloudinary.api_key'), 
          "api_secret" => config('cloudinary.api_secret') 
        ));
        $employee = $this->repository->create($request->all());
        if ($request->hasFile('image_file')) {
          $upload = \Cloudinary\Uploader::upload($request->file('image_file')->getRealPath(), array("folder" => "employees/", "public_id" => $employee->id, "overwrite" => TRUE, "resource_type" => "image"));
          $this->repository->update(['image_path' => $upload['url']], $employee->id);
        }
        return redirect()->route('employees.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = $this->repository->with(['village', 'religion', 'employee_educations'])->find($id);
        return view('modules.master.employee.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $religions = $this->religionRepository->orderBy('name')->all();
        $educations = $this->educationRepository->orderBy('name')->all();
        $employee = $this->repository->find($id);
        $employee->district_id = Indonesia::findVillage($employee->village_id)->district_id;
        $employee->city_id = Indonesia::findDistrict($employee->district_id)->city_id;
        $employee->province_id = Indonesia::findCity($employee->city_id)->province_id;
        return view('modules.master.employee.edit', compact('id', 'employee', 'religions', 'educations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeUpdate $request, $id)
    {
        \Cloudinary::config(array( 
          "cloud_name" => config('cloudinary.cloud_name'), 
          "api_key" => config('cloudinary.api_key'), 
          "api_secret" => config('cloudinary.api_secret') 
        ));
        $employee = $this->repository->update($request->all(), $id);
        if ($request->hasFile('image_file')) {
          $upload = \Cloudinary\Uploader::upload($request->file('image_file')->getRealPath(), array("folder" => "employees/", "public_id" => $id, "overwrite" => TRUE, "resource_type" => "image"));
          $this->repository->update(['image_path' => $upload['url']], $id);
        }
        return redirect()->route('employees.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \Cloudinary::config(array( 
          "cloud_name" => config('cloudinary.cloud_name'), 
          "api_key" => config('cloudinary.api_key'), 
          "api_secret" => config('cloudinary.api_secret') 
        ));
        $this->repository->delete($id);
        return redirect()->route('employees.index');
    }

    public function datatable(Request $request)
    {
        return Datatables::of($this->repository->with(['village'])->all())
        ->editColumn('image_path', function ($student) {
            return "<span class='avatar' style='background-image: url(".$student->image_path.")'></span>";
        })
        ->editColumn('sex', function ($student) {
            return $student->sex == 'M' ? 'LK' : 'PR';
        })
        ->editColumn('birth_date', function ($employee) {
            return $employee->birth_date->format('d-m-Y');
        })
        ->editColumn('registered_at', function ($employee) {
            return $employee->registered_at->format('d-m-Y');
        })
        ->editColumn('created_at', function ($employee) {
            return $employee->created_at->diffForHumans();
        })
        ->editColumn('updated_at', function ($employee) {
            return $employee->updated_at->diffForHumans();
        })
        ->editColumn('active', function ($employee) {
            return $employee->active == '1' ? "<span class='text text-success'>Aktif</span>" : "<span class='text text-warning'>Nonaktif</span>";
        })
        ->addColumn('action', function ($employee) {
            $text = "";
            $text.= '<a href="'.route('employees.edit', ['id' => $employee->id]).'" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a>';
            $text.= "<form class='form-horizontal' style='display: inline;' method='POST' action='".route('employees.destroy', ['id' => $employee->id])."'><input type='hidden' name='_token' value='".csrf_token()."'> <input type='hidden' name='_method' value='DELETE'><button class='btn btn-sm btn-danger' type='submit'><i class='fas fa-trash'></i> Hapus</button></form><form>";
            return $text;
        })
        ->rawColumns(['image_path', 'active', 'action'])
        ->make(true);
    }
}
