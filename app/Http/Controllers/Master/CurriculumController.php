<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CurriculumStore;
use App\Http\Requests\CurriculumUpdate;
use Yajra\Datatables\Datatables;
use App\Repositories\CurriculumRepository;
use App\Repositories\GradeRepository;

class CurriculumController extends Controller
{
    protected $repository, $gradeRepository;

    public function __construct(CurriculumRepository $repository, GradeRepository $gradeRepository){
        $this->repository = $repository;
        $this->gradeRepository = $gradeRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $curriculums = $this->repository->all();
        return view('modules.master.curriculum.view', compact('curriculums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.master.curriculum.create');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CurriculumStore $request)
    {
        $curriculum = $this->repository->create($request->all());
        return redirect()->route('curriculums.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $curriculum = $this->repository->find($id);
        $grades = $this->gradeRepository->with([
            'lessons' => function ($query) use ($id) {
                $query->where('curriculum_id', $id)->orderBy('name');
            }
        ])->all();
        return view('modules.master.curriculum.show', compact('curriculum', 'grades'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $curriculum = $this->repository->find($id);
        return view('modules.master.curriculum.edit', compact('curriculum'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CurriculumUpdate $request, $id)
    {
        $curriculum = $this->repository->update($request->all(), $id);
        return redirect()->route('curriculums.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('curriculums.index');
    }

    public function datatable(Request $request)
    {
        return Datatables::of($this->repository->all())
        ->addColumn('action', function ($curriculum) {
            $text = "";
            $text.= '<a href="'.route('curriculums.edit', ['id' => $curriculum->id]).'" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a>';
            $text.= ' <a href="'.route('curriculums.show', ['id' => $curriculum->id]).'" class="btn btn-sm btn-info"><i class="fa fa-info"></i> Detail</a>';
            return $text;
        })
        ->rawColumns(['active', 'action'])
        ->make(true);
    }
}
