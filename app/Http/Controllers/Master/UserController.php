<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserStore;
use App\Http\Requests\UserUpdate;
use Yajra\Datatables\Datatables;
use Spatie\Permission\Models\Role;
use Hash;
use App\Repositories\UserRepository;

class UserController extends Controller
{
    protected $repository;

    public function __construct(UserRepository $repository){
        $this->repository = $repository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modules.master.user.view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::where('name', 'not like', 'superadmin')->get();
        return view('modules.master.user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStore $request)
    {
        $user = $this->repository->create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
        $user->assignRole($request->role_name);
        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->repository->find($id);
        return view('modules.master.user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdate $request, $id)
    {
        $this->repository->update($request->all(), $id);
        return redirect()->route('users.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('users.index');
    }

    public function datatable(Request $request)
    {
        return Datatables::of($this->repository->all())
        ->editColumn('created_at', function ($user) {
            return $user->created_at->diffForHumans();
        })
        ->editColumn('updated_at', function ($user) {
            return $user->updated_at->diffForHumans();
        })
        ->addColumn('roles', function ($user) {
            $text = '';
            foreach ($user->getRoleNames() as $key => $value) {
                $text = $text . '<span class="label label-info">'.$value.'</span> ';
            }
            return $text;
        })
        ->addColumn('action', function ($user) {
            return '<a href="'.route('users.edit', ['id' => $user->id]).'" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a>';
        })
        ->rawColumns(['roles', 'action'])
        ->make(true);
    }
}
