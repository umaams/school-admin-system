<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Http\Requests\ExtraCurricularStore;
use App\Http\Requests\ExtraCurricularUpdate;
use App\Repositories\ExtraCurricularRepository;

class ExtraCurricularController extends Controller
{
    protected $repository;

    public function __construct(ExtraCurricularRepository $repository){
        $this->repository = $repository;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modules.master.extracurricular.view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.master.extracurricular.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $extracurricular = $this->repository->create($request->all());
        return redirect()->route('extracurriculars.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $extracurricular = $this->repository->find($id);
        return view('modules.master.extracurricular.edit', compact('extracurricular'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $extracurricular = $this->repository->update($request->all(), $id);
        return redirect()->route('extracurriculars.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('extracurriculars.index');
    }

    public function datatable(Request $request)
    {
        return Datatables::of($this->repository->all())
        ->editColumn('created_at', function ($extracurricular) {
            return $extracurricular->created_at->diffForHumans();
        })
        ->editColumn('updated_at', function ($extracurricular) {
            return $extracurricular->updated_at->diffForHumans();
        })
        ->addColumn('action', function ($extracurricular) {
            $text = "";
            $text.= '<a href="'.route('extracurriculars.edit', ['id' => $extracurricular->id]).'" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a>';
            $text.= "<form class='form-horizontal' style='display: inline;' method='POST' action='".route('extracurriculars.destroy', ['id' => $extracurricular->id])."'><input type='hidden' name='_token' value='".csrf_token()."'> <input type='hidden' name='_method' value='DELETE'><button class='btn btn-sm btn-danger' type='submit'><i class='fas fa-trash'></i> Hapus</button></form><form>";
            return $text;
        })
        ->rawColumns(['active', 'action'])
        ->make(true);
    }
}
