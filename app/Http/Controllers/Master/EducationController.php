<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Master\Education;

class EducationController extends Controller
{
    public function showEducations()
    {
        $data['educations'] = Education::orderBy('id')->get();
        return response()->json(compact('data'));
    }
}
