<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\TeacherStore;
use App\Http\Requests\TeacherUpdate;
use Yajra\Datatables\Datatables;
use App\Repositories\TeacherRepository;
use App\Repositories\EmployeeRepository;

class TeacherController extends Controller
{
    protected $repository, $employeeRepository;

    public function __construct(TeacherRepository $repository, EmployeeRepository $employeeRepository){
        $this->repository = $repository;
        $this->employeeRepository = $employeeRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modules.master.teacher.view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = $this->employeeRepository->scopeQuery(function ($query) {
            return $query->whereDoesntHave('teacher');
        })->orderBy('name')->all();
        return view('modules.master.teacher.create', compact('employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeacherStore $request)
    {
        $teacher = $this->repository->create($request->all());
        return redirect()->route('teachers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $teacher = $this->repository->with(['employee'])->find($id);
        return view('modules.master.teacher.show', compact('teacher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employees = $this->employeeRepository->scopeQuery(function($query){
            return $query->whereDoesntHave('teacher');
        })->orderBy('name')->all();
        $teacher = $this->repository->find($id);
        return view('modules.master.teacher.edit', compact('id', 'teacher', 'employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TeacherUpdate $request, $id)
    {
        $teacher = $this->repository->update($request->all(), $id);
        return redirect()->route('teachers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('teachers.index');
    }

    public function datatable(Request $request)
    {
        return Datatables::of($this->repository->with(['employee'])->all())
        ->editColumn('employee.birth_date', function ($teacher) {
            return $teacher->employee->birth_date->format('d-m-Y');
        })
        ->editColumn('employee.registered_at', function ($teacher) {
            return $teacher->employee->registered_at->format('d-m-Y');
        })
        ->editColumn('active', function ($teacher) {
            return $teacher->active == '1' ? '<span class="text text-success">Aktif</span>' : '<span class="text text-warning">Nonaktif</span>';
        })
        ->editColumn('created_at', function ($teacher) {
            return $teacher->created_at->diffForHumans();
        })
        ->editColumn('updated_at', function ($teacher) {
            return $teacher->updated_at->diffForHumans();
        })
        ->addColumn('action', function ($teacher) {
            $text = "";
            $text.= '<a href="'.route('teachers.edit', ['id' => $teacher->id]).'" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a>';
            $text.= "<form class='form-horizontal' style='display: inline;' method='POST' action='".route('teachers.destroy', ['id' => $teacher->id])."'><input type='hidden' name='_token' value='".csrf_token()."'> <input type='hidden' name='_method' value='DELETE'><button class='btn btn-sm btn-danger' type='submit'><i class='fas fa-trash'></i> Hapus</button></form><form>";
            return $text;
        })
        ->rawColumns(['active', 'action'])
        ->make(true);
    }
}
