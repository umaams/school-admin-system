<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StudentStore;
use App\Http\Requests\StudentUpdate;
use Yajra\Datatables\Datatables;
use Indonesia;
use App\Repositories\StudentRepository;
use App\Repositories\BloodTypeRepository;
use App\Repositories\GradeRepository;
use App\Repositories\SchoolYearRepository;
use App\Repositories\ReligionRepository;
use App\Repositories\OccupationRepository;
use Illuminate\Support\Facades\Storage;

class StudentController extends Controller
{
    protected $repository, $bloodTypeRepository, $gradeRepository, $schoolYearRepository, $religionRepository, $occupationRepository;

    public function __construct(StudentRepository $repository, BloodTypeRepository $bloodTypeRepository, GradeRepository $gradeRepository, SchoolYearRepository $schoolYearRepository, ReligionRepository $religionRepository, OccupationRepository $occupationRepository){
        $this->repository = $repository;
        $this->bloodTypeRepository = $bloodTypeRepository;
        $this->gradeRepository = $gradeRepository;
        $this->schoolYearRepository = $schoolYearRepository;
        $this->religionRepository = $religionRepository;
        $this->occupationRepository = $occupationRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modules.master.student.view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $religions = $this->religionRepository->orderBy('name')->all();
        $occupations = $this->occupationRepository->orderBy('name')->all();
        $school_years = $this->schoolYearRepository->orderBy('name')->all();
        $grades = $this->gradeRepository->orderBy('id')->all();
        $blood_types = $this->bloodTypeRepository->orderBy('name')->all();
        return view('modules.master.student.create', compact('blood_types', 'grades', 'occupations', 'religions', 'school_years'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentStore $request)
    {
        \Cloudinary::config(array( 
          "cloud_name" => config('cloudinary.cloud_name'), 
          "api_key" => config('cloudinary.api_key'), 
          "api_secret" => config('cloudinary.api_secret') 
        ));
        $student = $this->repository->create($request->all());
        if ($request->hasFile('image_file')) {
          $upload = \Cloudinary\Uploader::upload($request->file('image_file')->getRealPath(), array("folder" => "students/", "public_id" => $student->id, "overwrite" => TRUE, "resource_type" => "image"));
          $this->repository->update(['image_path' => $upload['url']], $student->id);
        }
        return redirect()->route('students.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $student = $this->repository->find($id);
      return view('modules.master.student.show', compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blood_types = $this->bloodTypeRepository->orderBy('name')->all();
        $religions = $this->religionRepository->orderBy('name')->all();
        $occupations = $this->occupationRepository->orderBy('name')->all();
        $school_years = $this->schoolYearRepository->orderBy('name')->all();
        $grades = $this->gradeRepository->orderBy('id')->all();
        $student = $this->repository->find($id);
        $student->district_id = Indonesia::findVillage($student->village_id)->district_id;
        $student->city_id = Indonesia::findDistrict($student->district_id)->city_id;
        $student->province_id = Indonesia::findCity($student->city_id)->province_id;
        return view('modules.master.student.edit', compact('id', 'blood_types', 'student', 'grades', 'occupations', 'religions', 'school_years'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StudentUpdate $request, $id)
    {
        \Cloudinary::config(array( 
          "cloud_name" => config('cloudinary.cloud_name'), 
          "api_key" => config('cloudinary.api_key'), 
          "api_secret" => config('cloudinary.api_secret') 
        ));
        $this->repository->update($request->all(), $id);
        if ($request->hasFile('image_file')) {
          $upload = \Cloudinary\Uploader::upload($request->file('image_file')->getRealPath(), array("folder" => "students/", "public_id" => $id, "overwrite" => TRUE, "resource_type" => "image"));
          $this->repository->update(['image_path' => $upload['url']], $id);
        }
        return redirect()->route('students.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \Cloudinary::config(array( 
          "cloud_name" => config('cloudinary.cloud_name'), 
          "api_key" => config('cloudinary.api_key'), 
          "api_secret" => config('cloudinary.api_secret') 
        ));
        $this->repository->delete($id);
        return redirect()->route('students.index');
    }

    public function doesntHaveClasses(Request $request)
    {
        $students = $this->repository->whereDoesntHave('classes', function($query) {
            $query->where('school_year_id', $this->schoolYearRepository->findActiveId());
        })->orderBy('name')->all();
        return response()->json(compact('students'));
    }

    public function doesntHaveClassesDatatable(Request $request)
    {
      $query = $this->repository->with(['registered_school_year', 'religion', 'village'])->whereDoesntHave('classes', function($query) {
            $query->where('school_year_id', $this->schoolYearRepository->findActiveId());
        });
      if ($request->has('school_year_id')) {
        $query->whereHas('classes', function($query) use ($request) {
          $query->where('school_year_id', $request->get('school_year_id'));
          if ($request->has('class_id')) {
            $query->where('class_id', $request->get('class_id'));
          }
        });
      }
      return Datatables::of($query->orderBy('name')->all())
        ->editColumn('sex', function ($student) {
            return $student->sex == 'M' ? 'LK' : 'PR';
        })
        ->editColumn('birth_date', function ($student) {
            return $student->birth_date->format('d-m-Y');
        })
        ->addColumn('action', function ($student) {
            $text = "<input type='checkbox' name='student_id[]' value='".$student->id."'/>";
            return $text;
        })
        ->rawColumns(['action'])
        ->make(true);;
    }

    public function hasClasses(Request $request)
    {
        $data['students'] = $this->repository->with('classes')->whereHas('classes', function($query) {
            $query->where('school_year_id', $this->schoolYearRepository->findActiveId());
        })->orderBy('name')->scopeQuery(function ($query) use ($request) {
            $query->where('active', '1');
            if ($request->has('search')) $query->where('name', 'like', '%'.$request->get('search').'%');
            return $query->paginate(10);
        })->all();
        return response()->json(compact('data'));
    }

    public function doesntHaveExtracurriculars(Request $request)
    {
        $query = $this->repository->whereDoesntHave('extracurriculars', function($query) use ($request) {
            $query->where('school_year_id', $this->schoolYearRepository->findActiveId())->where('extracurricular_id', $request->get('extracurricular_id'));
        })->orderBy('name');
        return Datatables::of($query)
        ->editColumn('sex', function ($student) {
            return $student->sex == 'M' ? 'LK' : 'PR';
        })
        ->editColumn('birth_date', function ($student) {
            return $student->birth_date->format('d-m-Y');
        })
        ->editColumn('created_at', function ($student) {
            return $student->created_at->diffForHumans();
        })
        ->editColumn('updated_at', function ($student) {
            return $student->updated_at->diffForHumans();
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    public function datatable(Request $request)
    {
        return Datatables::of($this->repository->with(['registered_school_year', 'religion', 'village'])->all())
        ->editColumn('image_path', function ($student) {
            return "<span class='avatar' style='background-image: url(".$student->image_path.")'></span>";
        })
        ->editColumn('sex', function ($student) {
            return $student->sex == 'M' ? 'LK' : 'PR';
        })
        ->editColumn('birth_date', function ($student) {
            return $student->birth_date->format('d-m-Y');
        })
        ->editColumn('created_at', function ($student) {
            return $student->created_at->diffForHumans();
        })
        ->editColumn('updated_at', function ($student) {
            return $student->updated_at->diffForHumans();
        })
        ->addColumn('action', function ($student) {
            $text = "";
            $text.='<a href="'.route('students.show', ['id' => $student->id]).'" class="btn btn-sm btn-info"><i class="fa fa-eye"></i> Lihat</a>';
            $text.=' <a href="'.route('students.edit', ['id' => $student->id]).'" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a>';
            return $text;
        })
        ->rawColumns(['image_path', 'action'])
        ->make(true);
    }
}
