<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\EmployeeRepository;
use App\Repositories\StudentRepository;
use App\Repositories\TeacherRepository;
use App\Repositories\SchoolYearRepository;

class HomeController extends Controller
{
    protected $employeeRepository, $studentRepository, $teacherRepository, $schoolYearRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(EmployeeRepository $employeeRepository, StudentRepository $studentRepository, TeacherRepository $teacherRepository, SchoolYearRepository $schoolYearRepository)
    {
        $this->employeeRepository = $employeeRepository;
        $this->studentRepository = $studentRepository;
        $this->teacherRepository = $teacherRepository;
        $this->schoolYearRepository = $schoolYearRepository;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $studentCount = $this->studentRepository->whereHas('classes', function ($query) {
            $query->where('school_year_id', $this->schoolYearRepository->findActiveId());
        })->findByField('active', '1')->count();
        $employeeCount = $this->employeeRepository->findByField('active', '1')->count();
        $teacherCount = $this->teacherRepository->findByField('active', '1')->count();
        return view('modules/home', compact('employeeCount', 'studentCount', 'teacherCount'));
    }
}
