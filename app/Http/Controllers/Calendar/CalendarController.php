<?php

namespace App\Http\Controllers\Calendar;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CalendarUpdate;
use App\Repositories\DayWeekRepository;
use App\Repositories\SchoolYearCalendarRepository;
use App\Repositories\SchoolYearRepository;
use App\Repositories\EventRepository;
use Carbon\Carbon;

class CalendarController extends Controller
{
    protected $repository;
    protected $dayWeekRepository;
    protected $schoolYearRepository;
    protected $eventRepository;

    public function __construct(
        SchoolYearCalendarRepository $repository,
        DayWeekRepository $dayWeekRepository,
        SchoolYearRepository $schoolYearRepository,
        EventRepository $eventRepository
    ){
        $this->repository = $repository;
        $this->dayWeekRepository = $dayWeekRepository;
        $this->schoolYearRepository = $schoolYearRepository;
        $this->eventRepository = $eventRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $calendar = $this->repository->findByField('school_year_id', $this->schoolYearRepository->findActiveId())->first();
        return view('modules.calendar.view')->with(compact('calendar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $yearActiveId = $this->schoolYearRepository->findActiveId();
        $day_weeks = $this->dayWeekRepository->orderBy('id')->all();
        $calendar = $this->repository->findByField('school_year_id', $yearActiveId)->first();
        $holidays = $this->schoolYearRepository->find($yearActiveId)->holidays()->get()->map(function($item) {
            return $item->day_week_id;
        })->toArray();
        return view('modules.calendar.edit')->with(compact('day_weeks', 'calendar', 'holidays'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CalendarUpdate $request)
    {
        $yearActiveId = $this->schoolYearRepository->findActiveId();
        $this->schoolYearRepository->find($yearActiveId)->holidays()->delete();
		$attributes = array();
		foreach ($request->day_week_id as $value) {
			array_push($attributes, ['day_week_id' => $value]);
        }
        $this->schoolYearRepository->find($yearActiveId)->holidays()->createMany($attributes);
        // $this->eventRepository->deleteWhere(['event_category_id' => 4, 'visibility' => '0', 'school_year_id' => $yearActiveId]);
		// $date_start = Carbon::parse($request->semester_1_date_start);
		// $date_end = Carbon::parse($request->semester_2_date_end);
		// while ($date_start->lte($date_end)) {
		// 	if (in_array((($date_start->format('N') + 1) % 7 != 0) ? ($date_start->format('N') + 1) % 7 : 7, $request->day_week_id)) {
		// 		$this->eventRepository->create([
		// 			'name' => 'Hari Libur',
		// 			'school_year_id' => $yearActiveId,
		// 			'event_category_id' => 4,
		// 			'visibility' => '0',
		// 			'date_start' => $date_start->format('Y-m-d'),
		// 			'date_end' => $date_start->format('Y-m-d')
		// 		]);
		// 	}
		// 	$date_start = $date_start->addDay();
		// }
		$this->repository->updateOrCreate(['school_year_id' => $yearActiveId], $request->all());
        return redirect()->route('calendar.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
