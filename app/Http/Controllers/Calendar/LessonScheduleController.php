<?php

namespace App\Http\Controllers\Calendar;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Http\Requests\LessonScheduleStore;
use App\Http\Requests\LessonScheduleUpdate;
use App\Repositories\LessonScheduleRepository;
use App\Repositories\DayWeekRepository;
use App\Repositories\SchoolTimeRepository;
use App\Repositories\SchoolYearRepository;
use App\Repositories\ClassRepository;
use App\Repositories\LessonRepository;
use App\Repositories\TeacherRepository;

class LessonScheduleController extends Controller
{
    protected $repository, $dayWeekRepository, $schoolTimeRepository, $schoolYearRepository, $classRepository, $lessonRepository, $teacherRepository;

    public function __construct(LessonScheduleRepository $repository, SchoolYearRepository $schoolYearRepository, DayWeekRepository $dayWeekRepository, SchoolTimeRepository $schoolTimeRepository, ClassRepository $classRepository, LessonRepository $lessonRepository, TeacherRepository $teacherRepository){
        $this->repository = $repository;
        $this->schoolYearRepository = $schoolYearRepository;
        $this->dayWeekRepository = $dayWeekRepository;
        $this->schoolTimeRepository = $schoolTimeRepository;
        $this->classRepository = $classRepository;
        $this->lessonRepository = $lessonRepository;
        $this->teacherRepository = $teacherRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes = $this->classRepository->all();
        return view('modules.calendar.lesson_schedule.view')->with(compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $class = $this->classRepository->find($id);
        $dayWeeks = $this->dayWeekRepository->with('holiday')->scopeQuery(function ($query) {
            return $query->whereDoesntHave('holiday', function ($query) {
                return $query->where('school_year_id', $this->schoolYearRepository->findActiveId());
            });
        })->all();
        $schoolTimes = $this->schoolTimeRepository->all();
        $teachers = $this->teacherRepository->with('employee')->all();
        $lessons = $this->lessonRepository->all();
        return view('modules.calendar.lesson_schedule.create')->with(compact('class', 'dayWeeks', 'schoolTimes', 'teachers', 'lessons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, LessonScheduleStore $request)
    {
        $request->merge(['school_year_id' => $this->schoolYearRepository->findActiveId()]);
        $request->merge(['class_id' => $id]);
        $lesson_schedule = $this->repository->create($request->all());
        return redirect()->route('lesson_schedules.show', ['id' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $class = $this->classRepository->find($id);
        $dayWeeks = $this->dayWeekRepository->with('holiday')->scopeQuery(function ($query) {
            return $query->whereDoesntHave('holiday', function ($query) {
                return $query->where('school_year_id', $this->schoolYearRepository->findActiveId());
            });
        })->all();
        $schoolTimes = $this->schoolTimeRepository->all();
        $schoolTimes->map(function ($schoolTime, $i) use ($id) {
            $schoolTime->dayWeeks = $this->dayWeekRepository->with('holiday')->scopeQuery(function ($query) {
                return $query->whereDoesntHave('holiday', function ($query) {
                    return $query->where('school_year_id', $this->schoolYearRepository->findActiveId());
                });
            })->all();
            $schoolTime->dayWeeks->map(function ($dayWeek, $j) use ($id, $schoolTime) {
                $dayWeek->lessonSchedule = $this->repository->with(['lesson', 'teacher.employee'])->findWhere([
                    'school_year_id' => $this->schoolYearRepository->findActiveId(),
                    'class_id' => $id,
                    'school_time_id' => $schoolTime->id,
                    'day_week_id' => $dayWeek->id
                ])->first();
                return $dayWeek;
            });
            return $schoolTime;
        });
        return view('modules.calendar.lesson_schedule.show')->with(compact('class', 'dayWeeks', 'schoolTimes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dayWeeks = $this->dayWeekRepository->with('holiday')->scopeQuery(function ($query) {
            return $query->whereDoesntHave('holiday', function ($query) {
                return $query->where('school_year_id', $this->schoolYearRepository->findActiveId());
            });
        })->all();
        $schoolTimes = $this->schoolTimeRepository->all();
        $teachers = $this->teacherRepository->with('employee')->all();
        $lessons = $this->lessonRepository->all();
        $lesson_schedule = $this->repository->find($id);
        return view('modules.calendar.lesson_schedule.edit')->with(compact('dayWeeks', 'schoolTimes', 'teachers', 'lessons', 'lesson_schedule'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lesson_schedule = $this->repository->update($request->all(), $id);
        return redirect()->route('lesson_schedules.show', ['id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lesson_schedule = $this->repository->find($id);
        $class_id = $lesson_schedule->class_id;
        $this->repository->delete($id);
        return redirect()->route('lesson_schedules.show', ['id' => $class_id]);
    }

    public function search(Request $request)
    {
        $dayWeeks = $this->dayWeekRepository->all(['id','name']);
        $dayWeeks->map(function ($dayWeek, $index) use ($request) {
            $dayWeek->timeSchools = $this->schoolTimeRepository->all(['id', 'time_start', 'time_end'])->map(function ($timeSchool, $index) use ($request) {
                $timeSchool->schedule = $this->repository->all();
                return $timeSchool;
            });
            return $dayWeek;
        });
        return response()->json($dayWeeks);
    }

    public function datatable(Request $request)
    {
        $classes = $this->classRepository->with('grade')->withCount(['lesson_schedules' => function($query) {
            $query->where('school_year_id', $this->schoolYearRepository->findActiveId());
        }])->withCount(['lessons' => function($query) {
            $query->where('school_year_id', $this->schoolYearRepository->findActiveId());
        }])->withCount(['teachers' => function($query) {
            $query->where('school_year_id', $this->schoolYearRepository->findActiveId());
        }])->all();
        return Datatables::of($classes)
        ->editColumn('name', function ($class) {
            return "Kelas ".$class->name;
        })
        ->editColumn('grade.name', function ($class) {
            return $class->grade->name;
        })
        ->editColumn('lesson_schedules_count', function ($class) {
            return $class->lesson_schedules_count. " jam";
        })
        ->editColumn('lessons_count', function ($class) {
            return $class->lessons_count. " mata pelajaran";
        })
        ->addColumn('action', function ($class) {
            $text = "";
            $text.= ' <a href="'.route('lesson_schedules.show', ['id' => $class->id]).'" class="btn btn-sm btn-info"><i class="fa fa-info"></i> Detail Jadwal</a>';
            return $text;
        })
        ->rawColumns(['active', 'action'])
        ->make(true);
    }
}
