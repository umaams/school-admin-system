<?php

namespace App\Http\Controllers\Calendar;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\EventStore;
use App\Http\Requests\EventUpdate;
use Yajra\Datatables\Datatables;
use App\Repositories\EventRepository;
use App\Repositories\EventCategoryRepository;
use App\Repositories\SchoolYearRepository;
use App\Repositories\SchoolYearCalendarRepository;
use Carbon\Carbon;

class EventController extends Controller
{

    protected $repository;
    protected $schoolYearRepository;
    protected $schoolYearCalendarRepository;
    protected $eventCategoryRepository;

    public function __construct(EventRepository $repository, SchoolYearRepository $schoolYearRepository, EventCategoryRepository $eventCategoryRepository, SchoolYearCalendarRepository $schoolYearCalendarRepository){
        $this->repository = $repository;
        $this->schoolYearRepository = $schoolYearRepository;
        $this->schoolYearCalendarRepository = $schoolYearCalendarRepository;
        $this->eventCategoryRepository = $eventCategoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modules.calendar.event.view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $calendar = $this->schoolYearCalendarRepository->findByField('school_year_id', $this->schoolYearRepository->findActiveId())->first();
        $event_categories = $this->eventCategoryRepository->orderBy('name')->all();
        return view('modules.calendar.event.create', compact('event_categories', 'calendar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventStore $request)
    {
        $request->merge(['school_year_id' => $this->schoolYearRepository->findActiveId()]);
        $event = $this->repository->create($request->all());
        return redirect()->route('events.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event_categories = $this->eventCategoryRepository->orderBy('name')->all();
        $event = $this->repository->find($id);
        return view('modules.calendar.event.edit', compact('event', 'event_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EventUpdate $request, $id)
    {
        $event = $this->repository->update($request->all(), $id);
        return redirect()->route('events.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->route('events.index');
    }

    public function datatable(Request $request)
    {
        return Datatables::of($this->repository->with('event_category')->findWhere(['school_year_id' => $this->schoolYearRepository->findActiveId(), 'visibility' => '1']))
        ->editColumn('date_start', function ($event) {
            return $event->date_start->format('d-m-Y');
        })
        ->editColumn('date_end', function ($event) {
            return $event->date_end->format('d-m-Y');
        })
        ->editColumn('created_at', function ($event) {
            return $event->created_at->diffForHumans();
        })
        ->editColumn('updated_at', function ($event) {
            return $event->updated_at->diffForHumans();
        })
        ->addColumn('action', function ($event) {
            $text = "";
            $text.= '<a href="'.route('events.edit', ['id' => $event->id]).'" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a>';
            $text.= "<form class='form-horizontal' style='display: inline;' method='POST' action='".route('events.destroy', ['id' => $event->id])."'><input type='hidden' name='_token' value='".csrf_token()."'> <input type='hidden' name='_method' value='DELETE'><button class='btn btn-sm btn-danger' type='submit'><i class='fas fa-trash'></i> Hapus</button></form><form>";
            return $text;
        })
        ->rawColumns(['active', 'action'])
        ->make(true);
    }

    public function getOnActiveSchoolYear(Request $request)
    {
        $events = $this->repository->scopeQuery(function ($query) use ($request) {
            if ($request->has('isActiveDay')) {
                $query->whereHas('event_category', function ($query) use ($request) {
                    $query->where('active_day', $request->get('isActiveDay'));
                });
            }
            return $query;
        })->findByField('school_year_id', $this->schoolYearRepository->findActiveId());
        return response()->json(compact('events'));
    }

    public function getUpcoming(Request $request)
    {
        $events = $this->repository->scopeQuery(function ($query) use ($request) {
            $query->where(function ($query) {
                return $query->orWhereDate('date_start', '>=', date('Y-m-d'))->orWhereDate('date_end', '>=', date('Y-m-d'));
            })->where('date_start', '<=', Carbon::createFromFormat('Y-m-d', date('Y-m-d'))->addDays(7)->format('Y-m-d'));
            return $query;
        })->findByField('school_year_id', $this->schoolYearRepository->findActiveId());
        return response()->json(compact('events'));
    }
}
