<?php

namespace App\Http\Controllers\Report\StudentAttendance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\CarbonPeriod;
use Carbon\Carbon;
use App\Repositories\ClassRepository;
use App\Repositories\StudentAttendanceRepository;
use App\Repositories\EventRepository;
use App\Repositories\StudentPermitRepository;
use App\Repositories\SchoolYearRepository;
use App\Repositories\SchoolYearCalendarRepository;

class ReportClassController extends Controller
{
    protected $classRepository, $attendanceRepository, $eventRepository, $studentPermitRepository, $schoolYearRepository, $schoolYearCalendarRepository;

    public function __construct(ClassRepository $classRepository, StudentAttendanceRepository $attendanceRepository, EventRepository $eventRepository, StudentPermitRepository $studentPermitRepository, SchoolYearRepository $schoolYearRepository, SchoolYearCalendarRepository $schoolYearCalendarRepository){
        $this->classRepository = $classRepository;
        $this->attendanceRepository = $attendanceRepository;
        $this->eventRepository = $eventRepository;
        $this->studentPermitRepository = $studentPermitRepository;
        $this->schoolYearRepository = $schoolYearRepository;
        $this->schoolYearCalendarRepository = $schoolYearCalendarRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modules.report.student_attendance.class');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $year = $request->has('year') ? $request->get('year') : date("Y");
        $month = $request->has('month') ? $request->get('month') : date('m');
        $school_year_id = $request->has('school_year_id') ? $request->get('school_year_id') : $this->schoolYearRepository->findActiveId();
        $carbonDate = Carbon::createFromFormat('Y-m-d', $year.'-'.$month.'-01');
        $period = CarbonPeriod::create($carbonDate->format('Y-m-d'), '1 days', $carbonDate->format('Y-m-t'));

        $events = $this->eventRepository->scopeQuery(function ($query) use ($carbonDate) {
            return $query->where(function ($query) use ($carbonDate) {
                return $query->whereDate('date_start', '>=', $carbonDate->format('Y-m-d'))->whereDate('date_end', '<=', $carbonDate->format('Y-m-t'));
            });
        })->whereHas('event_category', function ($query) {
            return $query->where('holiday', '1');
        })->orderBy('date_start')->all();
        $schoolYearCalendar = $this->schoolYearCalendarRepository->findByField('school_year_id', $school_year_id)->first();

        $holidays = array();
        foreach ($events as $event) {
            $eventPeriods = CarbonPeriod::create($event->date_start->format('Y-m-d'), '1 days', $event->date_end->format('Y-m-d'));
            foreach ($eventPeriods as $eventPeriod) {
                array_push($holidays, $eventPeriod->format("Y-m-d"));
            }
        }

        $dates = array();
        foreach ($period as $key => $date) {
            $temp['date'] = $date->format("Y-m-d");
            $temp['holiday'] = in_array($date->format("Y-m-d"), $holidays) ? 1 : 0;
            if ($schoolYearCalendar) {
                if (($schoolYearCalendar->semester_1_date_start->lessThanOrEqualTo($date) && $schoolYearCalendar->semester_1_date_end->greaterThanOrEqualTo($date)) || ($schoolYearCalendar->semester_2_date_start->lessThanOrEqualTo($date) && $schoolYearCalendar->semester_2_date_end->greaterThanOrEqualTo($date))) {
                    $temp['active_day'] = 1;
                } else {
                    $temp['active_day'] = 0;
                }
            } else {
                $temp['active_day'] = 0;
            }
            $temp['active_day'] = $temp['holiday'] != 1 ? 1 : 0;
            $temp['present'] = 0;
            $temp['absence'] = $temp['active_day'] == 1 ? 1 : 0;
            $temp['sick'] = 0;
            $temp['permit'] = 0;
            array_push($dates, $temp);
        }
        $data['class'] = $this->classRepository->with(['students' => function($query) use ($request) {
            $query->where('school_year_id', $this->schoolYearRepository->findActiveId())->select('id', 'nis', 'name');
        }])->scopeQuery(function($query) use ($request) {
            return $query->where('id', $request->get('class_id'));
        })->all(['id', 'name'])->first();
        if ($data['class']) {
            $data['class']->students = $data['class']->students->map(function($student) use ($dates, $school_year_id, $carbonDate) {
                $attendances = $this->attendanceRepository->scopeQuery(function($query) use ($school_year_id, $student, $carbonDate) {
                    return $query->where('school_year_id', $school_year_id)->where('student_id', $student->id)->whereDate('att_date', '>=', $carbonDate->format('Y-m-d'))->whereDate('att_date', '<=', $carbonDate->format('Y-m-t'));
                })->all()->pluck('att_date')->toArray();

                $permits = $this->studentPermitRepository->scopeQuery(function($query) use ($school_year_id, $student, $carbonDate) {
                    return $query->where('school_year_id', $school_year_id)->where('student_id', $student->id)->where(function ($query) use ($carbonDate) {
                            return $query->orWhere(function ($query) use ($carbonDate) {
                                return $query->whereDate('date_start', '>=', $carbonDate->format('Y-m-d'))->whereDate('date_start', '<=', $carbonDate->format('Y-m-t'));
                            })->orWhere(function ($query) use ($carbonDate) {
                                return $query->whereDate('date_end', '>=', $carbonDate->format('Y-m-d'))->whereDate('date_end', '<=', $carbonDate->format('Y-m-t'));
                            });
                    });
                })->all();
                $student->attendances = collect($dates);
                $student->attendances = $student->attendances->map(function ($attendance) use ($attendances, $permits) {
                    if ($attendance['active_day']) {
                        if (in_array($attendance['date'], $attendances)) {
                            $attendance['present'] = 1;
                            $attendance['absence'] = 0;
                        } else {
                            $permit = $permits->first(function ($value, $key) use($attendance) {
                                return (strtotime($value->date_start) <= strtotime($attendance['date']) && strtotime($value->date_end) >= strtotime($attendance['date']));
                            });
                            //dd($permit);
                            if ($permit) {
                                if ($permit->permit_category_id == 1) {
                                    $attendance['sick'] = 1;
                                    $attendance['absence'] = 0;
                                } elseif ($permit->permit_category_id == 2) {
                                    $attendance['permit'] = 1;
                                    $attendance['absence'] = 0;
                                } else $attendance['absence'] = 1;
                            } else {
                                $attendance['absence'] = 1;
                            }
                        }
                    } else {
                        $attendance['present'] = 0;
                        $attendance['absence'] = 0;
                    }
                    
                    return $attendance;
                });
                $student->total_active_day = $student->attendances->sum('active_day');
                $student->total_present = $student->attendances->sum('present');
                $student->total_absence = $student->attendances->sum('absence');
                $student->total_sick = $student->attendances->sum('sick');
                $student->total_permit = $student->attendances->sum('permit');
                return $student;
            });
        }
        return response()->json(compact('data'));
    }
}
