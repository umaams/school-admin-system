<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\CarbonPeriod;
use App\Repositories\ClassRepository;
use App\Repositories\StudentAttendanceRepository;
use App\Repositories\EventRepository;
use App\Repositories\SchoolYearRepository;

class ReportStudentAttendanceController extends Controller
{
    protected $classRepository, $attendanceRepository, $eventRepository, $schoolYearRepository;

    public function __construct(ClassRepository $classRepository, StudentAttendanceRepository $attendanceRepository, EventRepository $eventRepository, SchoolYearRepository $schoolYearRepository){
        $this->classRepository = $classRepository;
        $this->attendanceRepository = $attendanceRepository;
        $this->eventRepository = $eventRepository;
        $this->schoolYearRepository = $schoolYearRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function class(Request $request)
    {
        $period = CarbonPeriod::create('2018-12-01', '1 days', '2018-12-31');
        $dates = array();
        foreach ($period as $key => $date) {
            $temp['date'] = $date->format("Y-m-d");
            $temp['active_day'] = $date->dayOfWeekIso != 1 ? 1 : 0;
            $temp['holiday'] = $date->dayOfWeekIso == 1 ? 1 : 0;
            $temp['present'] = 0;
            $temp['absence'] = $date->dayOfWeekIso == 1 ? 0 : 1;
            $temp['sick'] = 0;
            $temp['permit'] = 0;
            array_push($dates, $temp);
        }
        $data['class'] = $this->classRepository->with(['students' => function($query) {
            $query->where('school_year_id', $this->schoolYearRepository->findActiveId())->select('id', 'nis', 'name');
        }])->all(['id', 'name'])->first();
        $data['class']->students->map(function($student) use ($dates) {
            $student->attendances = $dates;
            $student->total_active_day = collect($student->attendances)->sum('active_day');
            $student->total_present = collect($student->attendances)->sum('present');
            $student->total_absence = collect($student->attendances)->sum('absence');
            $student->total_sick = collect($student->attendances)->sum('sick');
            $student->total_permit = collect($student->attendances)->sum('permit');
            return $student;
        });
        return response()->json(compact('data'));
    }
}
