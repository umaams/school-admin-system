<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Repositories\StudentRepository;
use App\Repositories\SchoolYearRepository;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ReportStudentExport;

class ReportStudentController extends Controller
{
    protected $schoolYearRepository, $studentRepository;

    public function __construct(SchoolYearRepository $schoolYearRepository, StudentRepository $studentRepository){
        $this->schoolYearRepository = $schoolYearRepository;
        $this->studentRepository = $studentRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('modules.report.student.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $school_year_id = $request->has('school_year_id') ? $request->school_year_id : $this->schoolYearRepository->findActiveId();
        $orderBy = $request->has('orderBy') ? $request->orderBy : 'nis';
        $school_years = $this->schoolYearRepository->all();
        $query = $this->studentRepository->with(['village', 'registered_school_year'])->whereHas('classes', function ($query) use ($school_year_id) {
            $query->where('school_year_id', $school_year_id);
        });
        $data['students'] = $query->orderBy($orderBy)->all();
        return response()->json(compact('data'));
    }

    public function getStudents(Request $request)
    {
        $school_year_id = $request->has('school_year_id') ? $request->school_year_id : $this->schoolYearRepository->findActiveId();
        $query = $this->studentRepository->with(['village', 'religion', 'registered_school_year'])->whereHas('classes', function ($query) use ($school_year_id) {
            $query->where('school_year_id', $school_year_id);
        });
        return $query;
    }

    public function datatable(Request $request)
    {
        $query = $this->getStudents($request)->all();

        return Datatables::of($query)
        ->editColumn('sex', function ($student) {
            return $student->sex == 'M' ? 'LK' : 'PR';
        })
        ->editColumn('birth_date', function ($class) {
            return $class->birth_date->format('d-m-Y');
        })
        ->rawColumns(['active', 'action'])
        ->make(true);
    }

    public function getExportParameters(Request $request)
    {
        $school_year_id = $request->has('school_year_id') ? $request->school_year_id : $this->schoolYearRepository->findActiveId();

        $parameters = [];
        $parameters['school_year_id'] = $this->schoolYearRepository->find($school_year_id)->name;
        return $parameters;
    }

    public function ExportExcel(Request $request)
    {
        $data = $this->getStudents($request)->all();
        $parameters = $this->getExportParameters($request);
        return Excel::download(new ReportStudentExport($data, $parameters), 'Report Data Siswa.xls');
    }

    public function ExportPdf(Request $request)
    {
        $data = $this->getStudents($request)->all();
        $parameters = $this->getExportParameters($request);
        return Excel::download(new ReportStudentExport($data, $parameters), 'Report Data Siswa.pdf', \Maatwebsite\Excel\Excel::DOMPDF);
    }
}
