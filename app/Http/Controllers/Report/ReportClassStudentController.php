<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\GradeRepository;
use App\Repositories\ClassRepository;
use App\Repositories\SchoolYearRepository;
use App\Repositories\StudentRepository;

class ReportClassStudentController extends Controller
{
    protected $gradeRepository, $classRepository, $schoolYearRepository, $studentRepository;

    public function __construct(GradeRepository $gradeRepository, ClassRepository $classRepository, SchoolYearRepository $schoolYearRepository, StudentRepository $studentRepository){
        $this->gradeRepository = $gradeRepository;
        $this->classRepository = $classRepository;
        $this->schoolYearRepository = $schoolYearRepository;
        $this->studentRepository = $studentRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $school_year_id = $request->has('school_year_id') ? $request->school_year_id : $this->schoolYearRepository->findActiveId();
        $orderBy = $request->has('orderBy') ? $request->orderBy : 'nis';
        $school_years = $this->schoolYearRepository->all();
        $query = $this->gradeRepository->with(['classes' => function ($query) use ($school_year_id, $orderBy) {
            $query->with(['guardian', 'students' => function ($query) use ($school_year_id, $orderBy) {
                $query->with(['village', 'registered_school_year'])->where('school_year_id', $school_year_id)->orderBy($orderBy);
            }]);
        }]);
        $grades = $query->orderBy('id')->all();
        return view('modules.report.class_student.index', compact('school_year_id', 'orderBy', 'school_years', 'grades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $school_year_id = $request->has('school_year_id') ? $request->school_year_id : $this->schoolYearRepository->findActiveId();
        $orderBy = $request->has('orderBy') ? $request->orderBy : 'nis';
        $school_years = $this->schoolYearRepository->all();
        $query = $this->gradeRepository->with(['classes' => function ($query) use ($school_year_id, $orderBy) {
            $query->with(['guardian', 'students' => function ($query) use ($school_year_id, $orderBy) {
                $query->with(['village', 'registered_school_year'])->where('school_year_id', $school_year_id)->orderBy($orderBy);
            }]);
        }]);
        $data['grades'] = $query->orderBy('id')->all();
        return response()->json(compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
