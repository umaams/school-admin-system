<?php

namespace App\Http\Controllers\Stat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ReligionRepository;
use App\Repositories\SchoolYearRepository;
use App\Repositories\OccupationRepository;
use App\Repositories\VillageRepository;
use App\Repositories\BloodTypeRepository;
use App\Repositories\StudentRepository;

class StudentController extends Controller
{
    protected $studentRepository, $religionRepository, $schoolYearRepository, $occupationRepository, $villageRepository, $bloodTypeRepository;

    public function __construct(StudentRepository $studentRepository, ReligionRepository $religionRepository, SchoolYearRepository $schoolYearRepository, OccupationRepository $occupationRepository, VillageRepository $villageRepository, BloodTypeRepository $bloodTypeRepository){
        $this->studentRepository = $studentRepository;
        $this->religionRepository = $religionRepository;
        $this->schoolYearRepository = $schoolYearRepository;
        $this->occupationRepository = $occupationRepository;
        $this->villageRepository = $villageRepository;
        $this->bloodTypeRepository = $bloodTypeRepository;
    }

    public function getReligions(Request $request)
    {
    	$religions = $this->religionRepository->whereHas('students', function($query) use ($request) {
    		$query->whereHas('classes', function ($query) use ($request) {
                $query->where('school_year_id', $request->has('school_year_id') ? $request->get('school_year_id') : $schoolYearRepository->findActiveId());
            })->where('active', '1');
    	})->withCount('students')->all();
    	return response()->json(compact('religions'));
    }

    public function getBloodTypes(Request $request)
    {
        $blood_types = $this->bloodTypeRepository->whereHas('students', function($query) use ($request) {
            $query->whereHas('classes', function ($query) use ($request) {
                $query->where('school_year_id', $request->has('school_year_id') ? $request->get('school_year_id') : $schoolYearRepository->findActiveId());
            })->where('active', '1');
        })->withCount('students')->all();
        return response()->json(compact('blood_types'));
    }

    public function getVillages(Request $request)
    {
        $villages = $this->villageRepository->whereHas('students', function($query) use ($request) {
            $query->whereHas('classes', function ($query) use ($request) {
                $query->where('school_year_id', $request->has('school_year_id') ? $request->get('school_year_id') : $schoolYearRepository->findActiveId());
            })->where('active', '1');
        })->withCount('students')->all();
        return response()->json(compact('villages'));
    }

    public function getGenders(Request $request)
    {
        
    }

    public function getFatherOccupations(Request $request)
    {
        $occupations = $this->occupationRepository->whereHas('father_students', function($query) use ($request) {
            $query->whereHas('classes', function ($query) use ($request) {
                $query->where('school_year_id', $request->has('school_year_id') ? $request->get('school_year_id') : $schoolYearRepository->findActiveId());
            })->where('active', '1');
        })->withCount('students')->all();
        return response()->json(compact('occupations'));
    }

    public function getMotherOccupations(Request $request)
    {
        $occupations = $this->occupationRepository->whereHas('mother_students', function($query) use ($request) {
            $query->whereHas('classes', function ($query) use ($request) {
                $query->where('school_year_id', $request->has('school_year_id') ? $request->get('school_year_id') : $schoolYearRepository->findActiveId());
            })->where('active', '1');
        })->withCount('students')->all();
        return response()->json(compact('occupations'));
    }

    public function getGuardianOccupations(Request $request)
    {
        $occupations = $this->occupationRepository->whereHas('guardian_students', function($query) use ($request) {
            $query->whereHas('classes', function ($query) use ($request) {
                $query->where('school_year_id', $request->has('school_year_id') ? $request->get('school_year_id') : $schoolYearRepository->findActiveId());
            })->where('active', '1');
        })->withCount('students')->all();
        return response()->json(compact('occupations'));
    }
}
