<?php

namespace App\Http\Controllers\Stat;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ReligionRepository;
use App\Repositories\VillageRepository;

class EmployeeController extends Controller
{
	protected $religionRepository, $villageRepository;

    public function __construct(ReligionRepository $religionRepository, VillageRepository $villageRepository){
        $this->religionRepository = $religionRepository;
        $this->villageRepository = $villageRepository;
    }

    public function getReligions(Request $request)
    {
    	$religions = $this->religionRepository->whereHas('employees', function($query) {
    		$query->where('active', '1');
    	})->withCount('employees')->all();
    	return response()->json(compact('religions'));
    }

    public function getVillages(Request $request)
    {
        $villages = $this->villageRepository->whereHas('employees', function($query) {
            $query->where('active', '1');
        })->withCount('employees')->all();
        return response()->json(compact('villages'));
    }
}
