<?php

namespace App\Http\Controllers\Classes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\LessonJournalEdit;
use App\Http\Requests\LessonJournalUpdate;
use App\Repositories\ClassRepository;
use App\Repositories\LessonJournalRepository;
use App\Repositories\LessonScheduleRepository;
use App\Repositories\SchoolYearRepository;
use App\Repositories\SchoolYearCalendarRepository;
use Carbon\Carbon;

class LessonJournalController extends Controller
{
    protected $repository, $classRepository, $lessonScheduleRepository, $schoolYearRepository, $schoolYearCalendarRepository;

    public function __construct(LessonJournalRepository $repository, ClassRepository $classRepository, LessonScheduleRepository $lessonScheduleRepository, SchoolYearRepository $schoolYearRepository, SchoolYearCalendarRepository $schoolYearCalendarRepository){
        $this->repository = $repository;
        $this->classRepository = $classRepository;
        $this->lessonScheduleRepository = $lessonScheduleRepository;
        $this->schoolYearRepository = $schoolYearRepository;
        $this->schoolYearCalendarRepository = $schoolYearCalendarRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $calendar = $this->schoolYearCalendarRepository->findByField('school_year_id', $this->schoolYearRepository->findActiveId())->first();
        $classes = $this->classRepository->findByField('active', '1');
        return view('modules.class.journal.view')->with(compact('classes', 'calendar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $lesson_schedules = $this->lessonScheduleRepository->with([
            'lesson_journal' => function ($query) use ($request) {
                $query->whereDate('journal_date', $request->journal_date);
            },
            'teacher' => function ($query) {
                $query->with(['employee' => function ($query) {
                    $query->select('id', 'name');
                }])->select('id', 'employee_id');
            },
            'lesson' => function ($query) {
                $query->select('id', 'name');
            },
            'school_time' => function ($query) {
                $query->select('id', 'time_start', 'time_end');
            }
        ])->orderBy('school_time_id')->findWhere([
            'school_year_id' => $this->schoolYearRepository->findActiveId(),
            'class_id' => $request->class_id,
            'day_week_id' => floatval(date('w', strtotime($request->journal_date))) + 1
        ]);
        return response()->json(compact('lesson_schedules'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(LessonJournalEdit $request)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LessonJournalUpdate $request)
    {
        $this->repository->updateOrCreate(['journal_date' => $request->journal_date, 'lesson_schedule_id' => $request->lesson_schedule_id], ['description' => $request->description]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return redirect()->back();
    }
}
