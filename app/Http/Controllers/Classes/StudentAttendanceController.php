<?php

namespace App\Http\Controllers\Classes;

use Illuminate\Http\Request;
use App\Http\Requests\AttendanceEdit;
use App\Http\Requests\StudentAttendanceUpdate;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Repositories\ClassRepository;
use App\Repositories\SchoolYearRepository;
use App\Repositories\SchoolYearCalendarRepository;
use App\Repositories\StudentAttendanceRepository;

class StudentAttendanceController extends Controller
{
    protected $repository, $classRepository, $schoolYearRepository, $schoolYearCalendarRepository;

    public function __construct(StudentAttendanceRepository $repository, ClassRepository $classRepository, SchoolYearRepository $schoolYearRepository, SchoolYearCalendarRepository $schoolYearCalendarRepository){
        $this->repository = $repository;
        $this->classRepository = $classRepository;
        $this->schoolYearRepository = $schoolYearRepository;
        $this->schoolYearCalendarRepository = $schoolYearCalendarRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $calendar = $this->schoolYearCalendarRepository->findByField('school_year_id', $this->schoolYearRepository->findActiveId())->first();
        $classes = $this->classRepository->findByField('active', '1');
        return view('modules.class.attendance.view')->with(compact('classes', 'calendar'));        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(AttendanceEdit $request)
    {
        $att_date = $request->att_date;
        $kelas = $this->classRepository->with([
            'students' => function ($query) use ($att_date) {
                $query->with([
                    'attendances' => function ($query) use ($att_date) {
                        $query->whereDate('att_date', $att_date);
                    },
                    'student_permits' => function ($query) use ($att_date) {
                        $query->with('permit_category')->whereDate('date_start', '<=', $att_date)->whereDate('date_end', '>=', $att_date);
                    }
                ])->where('school_year_id', $this->schoolYearRepository->findActiveId());
            }
        ])->findWhere(['active' => '1', 'id' => $request->class_id])->first();
        return response()->json(compact('kelas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(AttendanceEdit $request)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StudentAttendanceUpdate $request)
    {
        $att_date = Carbon::parse($request->att_date);
        $calendar = $this->schoolYearCalendarRepository->findByField('school_year_id', $this->schoolYearRepository->findActiveId())->first();
        $semester_id = '';
        if ($att_date->between($calendar->semester_1_date_start, $calendar->semester_1_date_end)) $semester_id = 1;
        if ($att_date->between($calendar->semester_2_date_start, $calendar->semester_2_date_end)) $semester_id = 2; 
        foreach ($request->students as $student) {
            $this->repository->deleteWhere(['att_date' => $request->att_date, 'student_id' => $student['student_id']]);
            if ($student['att_status'] == "1") {
                $this->repository->create([
                    'school_year_id' => $this->schoolYearRepository->findActiveId(),
                    'semester_id' => $semester_id,
                    'student_id' => $student['student_id'],
                    'att_date' => $request->att_date,
                    'log_date' => $request->att_date
                ]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
