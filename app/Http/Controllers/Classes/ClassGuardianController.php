<?php

namespace App\Http\Controllers\Classes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ClassRepository;
use App\Repositories\GradeRepository;
use App\Repositories\SchoolYearRepository;
use App\Repositories\EmployeeRepository;

class ClassGuardianController extends Controller
{
    protected $classRepository, $gradeRepository, $schoolYearRepository, $employeeRepository;

    public function __construct(ClassRepository $classRepository, GradeRepository $gradeRepository, SchoolYearRepository $schoolYearRepository, EmployeeRepository $employeeRepository){
        $this->classRepository = $classRepository;
        $this->gradeRepository = $gradeRepository;
        $this->schoolYearRepository = $schoolYearRepository;
        $this->employeeRepository = $employeeRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes = $this->classRepository->with([
            'grade',
            'guardian' => function ($query) {
                $query->where('school_year_id', $this->schoolYearRepository->findActiveId());
            }
        ])->orderBy('name')->findByField('active', '1');
        return view('modules.class.guardian.view', compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $class = $this->classRepository->with([
            'guardian' => function ($query) {
                $query->where('school_year_id', $this->schoolYearRepository->findActiveId());
            }
        ])->find($id);
        $employees = $this->employeeRepository->orderBy('name')->findByField('active', '1'); 
        return view('modules.class.guardian.edit', compact('class', 'employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $class = $this->classRepository->find($id);
        $class->guardian()->sync(array($request->employee_id => array('school_year_id' => $this->schoolYearRepository->findActiveId())));
        return redirect()->route('classes.guardians.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
