<?php

namespace App\Http\Controllers\Classes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ClassRepository;
use App\Repositories\SchoolYearRepository;
use App\Repositories\StudentRepository;
use Yajra\Datatables\Datatables;

class ClassStudentController extends Controller
{
    protected $classRepository, $schoolYearRepository, $studentRepository;

    public function __construct(ClassRepository $classRepository, SchoolYearRepository $schoolYearRepository, StudentRepository $studentRepository){
        $this->classRepository = $classRepository;
        $this->schoolYearRepository = $schoolYearRepository;
        $this->studentRepository = $studentRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes = $this->classRepository->with('grade')->withCount([
            'students' => function ($query) {
                $query->where('school_year_id', $this->schoolYearRepository->findActiveId());
            }
        ])->orderBy('grade_id')->orderBy('name')->findByField('active', '1');
        return view('modules.class.student.view', compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add($id, Request $request)
    {
        $school_years = $this->schoolYearRepository->all();
        $school_year_id = $request->get('school_year_id');
        $class = $this->classRepository->findWhere(['active' => '1', 'id' => $id])->first();
        $query = $this->studentRepository->with('registered_school_year')->whereDoesntHave('classes', function ($query) {
            $query->where('school_year_id', \App\Models\Master\SchoolYear::where('active', '1')->first()->id);
        })->orderBy('name');
        $clauses = array();
        $clauses['active'] = 1;
        if ($request->has('school_year_id')) {
            $clauses['registered_school_year_id'] = $school_year_id;
        }
        $students = $query->scopeQuery(function($query) use ($request, $school_year_id) {
            $query->where('active', '1');
            if ($request->has('school_year_id')) {
                $query->where('registered_school_year_id', $school_year_id);
            }
            return $query;
        })->paginate();
        return view('modules.class.student.add', compact('class', 'school_years', 'students', 'school_year_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {
        $class = $this->classRepository->find($id);
        $class->students()->attach($request->student_id, ['school_year_id' => $this->schoolYearRepository->findActiveId()]);
        return redirect()->route('classes.students.show', ['id' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $class = $this->classRepository->with([
            'students' => function ($query) {
                $query->with('village')->where('school_year_id', $this->schoolYearRepository->findActiveId());
            }
        ])->findWhere(['active' => '1', 'id' => $id])->first();
        return view('modules.class.student.show', compact('class'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $class = $this->classRepository->find($id);
        $class->students()->wherePivot('school_year_id', $this->schoolYearRepository->findActiveId())->detach($request->student_id);
        return redirect()->route('classes.students.show', ['id' => $id]);
    }

    public function datatable(Request $request)
    {
        $classes = $this->classRepository->with('grade')->withCount(['students' => function($query) {
            $query->where('school_year_id', $this->schoolYearRepository->findActiveId());
        }])->all();
        return Datatables::of($classes)
        ->editColumn('name', function ($class) {
            return "Kelas ".$class->name;
        })
        ->editColumn('grade.name', function ($class) {
            return $class->grade->name;
        })
        ->editColumn('students_count', function ($class) {
            return $class->students_count." siswa";
        })
        ->addColumn('action', function ($class) {
            $text = "";
            $text.= ' <a href="'.route('classes.students.show', ['id' => $class->id]).'" class="btn btn-sm btn-info"><i class="fa fa-info"></i> Detail Siswa</a>';
            return $text;
        })
        ->rawColumns(['action'])
        ->make(true);
    }

    public function show_datatable($id, Request $request)
    {
        return Datatables::of($this->studentRepository->with(['village'])->whereHas('classes', function($query) use($id) {
            $query->where('school_year_id', $this->schoolYearRepository->findActiveId())->where('class_id', $id);
        })->all())
        ->editColumn('sex', function ($student) {
            return $student->sex == 'M' ? 'LK' : 'PR';
        })
        ->editColumn('birth_date', function ($student) {
            return $student->birth_date->format('d-m-Y');
        })
        ->editColumn('created_at', function ($student) {
            return $student->created_at->diffForHumans();
        })
        ->editColumn('updated_at', function ($student) {
            return $student->updated_at->diffForHumans();
        })
        ->addColumn('action', function ($student) use ($id) {
            return "<form class='form-horizontal' style='display: inline;' method='POST' action='".route('classes.students.destroy', ['id' => $id])."'><input type='hidden' name='_token' value='".csrf_token()."'> <input type='hidden' name='_method' value='PUT'><input type='hidden' name='student_id[]' value='".$student->id."'/><button class='btn btn-sm btn-danger' type='submit'><i class='fas fa-trash'></i> Hapus</button></form><form>";
            return $text;
        })
        ->rawColumns(['action'])
        ->make(true);
    }
}
