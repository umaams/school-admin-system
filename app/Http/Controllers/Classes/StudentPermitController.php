<?php

namespace App\Http\Controllers\Classes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StudentPermitStore;
use App\Http\Requests\StudentPermitUpdate;
use Yajra\Datatables\Datatables;
use App\Repositories\StudentPermitRepository;
use App\Repositories\PermitCategoryRepository;
use App\Repositories\SchoolYearRepository;

class StudentPermitController extends Controller
{

    protected $repository, $permitCategoryRepository, $schoolYearRepository;

    public function __construct(StudentPermitRepository $repository, PermitCategoryRepository $permitCategoryRepository, SchoolYearRepository $schoolYearRepository){
        $this->repository = $repository;
        $this->permitCategoryRepository = $permitCategoryRepository;
        $this->schoolYearRepository = $schoolYearRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modules.class.attendance.permit.view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permit_categories = $this->permitCategoryRepository->orderBy('name')->all();
        return view('modules.class.attendance.permit.create', compact('permit_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentPermitStore $request)
    {
        $request->merge(['school_year_id' => $this->schoolYearRepository->findActiveId()]);
        $permit = $this->repository->create($request->all());
        if ($request->expectsJson()) return response()->json(compact('permit'));
        else return redirect()->route('student_permits.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $permit_categories = $this->permitCategoryRepository->orderBy('name')->all();
        $permit = $this->repository->with(['student', 'permit_category'])->find($id);
        if ($request->expectsJson()) return response()->json(array('student_permit' => $permit));
        else return view('modules.class.attendance.permit.edit', compact('permit', 'permit_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StudentPermitUpdate $request, $id)
    {
        $permit = $this->repository->update($request->all(), $id);
        if ($request->expectsJson()) return response()->json(compact('permit'));
        else return redirect()->route('student_permits.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $this->repository->delete($id);
        if ($request->expectsJson()) return response()->json([]);
        else return redirect()->route('student_permits.index');
    }

    public function datatable(Request $request)
    {
        return Datatables::of(
            $this->repository->with(['student', 'permit_category'])
            ->findWhere([
                'school_year_id' => $this->schoolYearRepository->findActiveId()
            ])
        )
        ->editColumn('date_start', function ($permit) {
            return $permit->date_start->format('d-m-Y');
        })
        ->editColumn('date_end', function ($permit) {
            return $permit->date_end->format('d-m-Y');
        })
        ->editColumn('created_at', function ($permit) {
            return $permit->created_at->diffForHumans();
        })
        ->editColumn('updated_at', function ($permit) {
            return $permit->updated_at->diffForHumans();
        })
        ->addColumn('action', function ($permit) {
            $text = "";
            $text.= '<a href="'.route('student_permits.edit', ['id' => $permit->id]).'" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Edit</a>';
            $text.= "<form class='form-horizontal' style='display: inline;' method='POST' action='".route('student_permits.destroy', ['id' => $permit->id])."'><input type='hidden' name='_token' value='".csrf_token()."'> <input type='hidden' name='_method' value='DELETE'><button class='btn btn-sm btn-danger' type='submit'><i class='fas fa-trash'></i> Hapus</button></form><form>";
            return $text;
        })
        ->rawColumns(['active', 'action'])
        ->make(true);
    }
}
