<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Master\SchoolYear;

class SchoolYearComposer
{
    public function compose(View $view)
    {
        $view->with('active_school_year', SchoolYear::where('active','1')->first());
    }
}