<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Master\Semester;

class SemesterComposer
{
    public function compose(View $view)
    {
        $view->with('active_semester', Semester::where('active','1')->first());
    }
}