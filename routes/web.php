<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
	Route::get('/home', 'HomeController@index')->name('home');

	Route::namespace('Master')->group(function () {
		Route::get('permit_categories', 'PermitCategoryController@index')->name('permit_categories.index');

		Route::get('educations', 'EducationController@showEducations');
		Route::get('provinces', 'IndonesiaController@showProvinces');
		Route::get('provinces/{id}/cities', 'IndonesiaController@showCities');
		Route::get('cities/{id}/districts', 'IndonesiaController@showDistricts');
		Route::get('districts/{id}/villages', 'IndonesiaController@showVillages');
		Route::resource('roles', 'RoleController');

		Route::get('curriculums/datatable', 'CurriculumController@datatable')->name('curriculums.datatable');
		Route::get('curriculums/{id}/add/{grade_id}', 'CurriculumDetailController@create')->name('curriculums.details.add');
		Route::put('curriculums/{id}/store/{grade_id}', 'CurriculumDetailController@store')->name('curriculums.details.store');
		Route::put('curriculums/{id}/update/{grade_id}', 'CurriculumDetailController@update')->name('curriculums.details.update');
		Route::put('curriculums/{id}/delete/{grade_id}', 'CurriculumDetailController@destroy')->name('curriculums.details.destroy');
		Route::resource('curriculums', 'CurriculumController');

		Route::get('classes/active', 'ClassController@getActive')->name('classes.active');
		Route::get('classes/datatable', 'ClassController@datatable')->name('classes.datatable');
		Route::resource('classes', 'ClassController');

		Route::get('extracurriculars/datatable', 'ExtraCurricularController@datatable')->name('extracurriculars.datatable');
		Route::resource('extracurriculars', 'ExtraCurricularController');

		Route::get('lesson_categories/datatable', 'LessonCategoryController@datatable')->name('lesson_categories.datatable');
		Route::resource('lesson_categories', 'LessonCategoryController');
		Route::get('lessons/datatable', 'LessonController@datatable')->name('lessons.datatable');
		Route::resource('lessons', 'LessonController');

		Route::get('school_times/datatable', 'SchoolTimeController@datatable')->name('school_times.datatable');
		Route::resource('school_times', 'SchoolTimeController');
		Route::get('students/hasclass', 'StudentController@hasClasses')->name('students.hasclass');
		Route::get('students/hasclass/none/datatable', 'StudentController@doesntHaveClassesDatatable')->name('students.hasclass.none.datatable');
		Route::get('students/datatable', 'StudentController@datatable')->name('students.datatable');
		Route::resource('students', 'StudentController');
		Route::get('employees/datatable', 'EmployeeController@datatable')->name('employees.datatable');
		Route::resource('employees', 'EmployeeController');

		Route::get('school_years/{id}/classes', 'SchoolYearController@getClasses')->name('school_years.classes');
		Route::get('school_years', 'SchoolYearController@index')->name('school_years.index');

		Route::get('teachers/datatable', 'TeacherController@datatable')->name('teachers.datatable');
		Route::resource('teachers', 'TeacherController');
		Route::get('users/datatable', 'UserController@datatable')->name('users.datatable');
		Route::resource('users', 'UserController');
	});

	Route::namespace('Classes')->group(function () {
		Route::get('attendances', 'StudentAttendanceController@index')->name('attendances.index');
		Route::get('attendances/show', 'StudentAttendanceController@show')->name('attendances.show');
		Route::post('attendances/update', 'StudentAttendanceController@update')->name('attendances.update');

		Route::get('lesson_journals', 'LessonJournalController@index')->name('lesson_journals.index');
		Route::get('lesson_journals/show', 'LessonJournalController@show')->name('lesson_journals.show');
		Route::post('lesson_journals/update', 'LessonJournalController@update')->name('lesson_journals.update');

		Route::get('student_permits/datatable', 'StudentPermitController@datatable')->name('student_permits.datatable');
		Route::resource('student_permits', 'StudentPermitController');

		Route::get('class_guardians', 'ClassGuardianController@index')->name('classes.guardians.index');
		Route::get('class_guardians/{id}/edit', 'ClassGuardianController@edit')->name('classes.guardians.edit');
		Route::put('class_guardians/{id}/update', 'ClassGuardianController@update')->name('classes.guardians.update');
		Route::get('class_students', 'ClassStudentController@index')->name('classes.students.index');
		Route::get('class_students/datatable', 'ClassStudentController@datatable')->name('classes.students.datatable');
		Route::get('class_students/{id}/students', 'ClassStudentController@show')->name('classes.students.show');
		Route::get('class_students/{id}/students/datatable', 'ClassStudentController@show_datatable')->name('classes.students.show.datatable');
		Route::get('class_students/{id}/add', 'ClassStudentController@add')->name('classes.students.add');
		Route::put('class_students/{id}/store', 'ClassStudentController@store')->name('classes.students.store');
		Route::put('class_students/{id}/remove', 'ClassStudentController@destroy')->name('classes.students.destroy');
	});

	Route::namespace('Calendar')->group(function () {
		Route::get('calendar_academic', 'CalendarController@index')->name('calendar.index');
		Route::get('calendar_academic/setting', 'CalendarController@edit')->name('calendar.edit');
		Route::post('calendar_academic/setting', 'CalendarController@update')->name('calendar.update');

		Route::get('events/school_year/active', 'EventController@getOnActiveSchoolYear')->name('events.show.school_year.active')->middleware('calendar.set');
		Route::get('events/datatable', 'EventController@datatable')->name('events.datatable')->middleware('calendar.set');
		Route::get('events/upcoming', 'EventController@getUpcoming')->name('events.upcoming');
		Route::resource('events', 'EventController')->middleware('calendar.set');

		Route::get('lesson_schedules/datatable', 'LessonScheduleController@datatable')->name('lesson_schedules.datatable');
		Route::get('lesson_schedules/{id}/schedules', 'LessonScheduleController@show')->name('lesson_schedules.show');
		Route::get('lesson_schedules/{id}/schedules/create', 'LessonScheduleController@create')->name('lesson_schedules.add');
		Route::post('lesson_schedules/{id}/schedules/store', 'LessonScheduleController@store')->name('lesson_schedules.store');
		Route::get('lesson_schedules/{id}', 'LessonScheduleController@edit')->name('lesson_schedules.edit');
		Route::patch('lesson_schedules/{id}', 'LessonScheduleController@update')->name('lesson_schedules.update');
		Route::delete('lesson_schedules/{id}', 'LessonScheduleController@destroy')->name('lesson_schedules.delete');
		Route::get('lesson_schedules', 'LessonScheduleController@index')->name('lesson_schedules.index');
	});

	Route::prefix('reports')->group(function () {
		Route::get('students', 'Report\ReportStudentController@index')->name('reports.students');
		Route::get('students/datatable', 'Report\ReportStudentController@datatable')->name('reports.students.datatable');
		Route::get('students/export/pdf', 'Report\ReportStudentController@exportPdf')->name('reports.students.export.pdf');
		Route::get('students/export/xls', 'Report\ReportStudentController@exportExcel')->name('reports.students.export.xls');
		Route::get('students/show', 'Report\ReportStudentController@show')->name('reports.students.show');
		Route::get('class_students', 'Report\ReportClassStudentController@index')->name('reports.class_students');
		Route::get('class_students/show', 'Report\ReportClassStudentController@show')->name('reports.class_students.show');
		Route::get('student_attendances/class', 'Report\StudentAttendance\ReportClassController@index')->name('reports.student_attendances.class');
		Route::get('student_attendances/class/show', 'Report\StudentAttendance\ReportClassController@show')->name('reports.student_attendances.class.show');

		Route::prefix('stats')->group(function () {
			Route::get('employees', 'Report\ReportStudentController@index')->name('reports.stats.students');
			Route::get('students', 'Report\ReportStudentController@index')->name('reports.stats.students');
		});
	});

	Route::prefix('stats')->group(function () {
		Route::prefix('employee')->group(function () {
			Route::get('religions', 'Stat\EmployeeController@getReligions')->name('stats.employee.religions');
		});
		Route::prefix('student')->group(function () {
			Route::get('religions', 'Stat\StudentController@getReligions')->name('stats.student.religions');
		});
	});

	Route::prefix('settings')->group(function () {
		Route::prefix('general')->group(function () {
			Route::get('/', 'Setting\SettingController@index')->name('settings.index');
			Route::put('/', 'Setting\SettingController@update')->name('settings.update');
		});
	});
});
