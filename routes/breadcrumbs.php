<?php 

Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('home'));
});

Breadcrumbs::for('class', function ($trail) {
    $trail->parent('home');
    $trail->push('Kelas', route('classes.index'));
});

Breadcrumbs::for('class create', function ($trail) {
    $trail->parent('class');
    $trail->push('Tambah', route('classes.create'));
});

Breadcrumbs::for('class edit', function ($trail, $class) {
    $trail->parent('class');
    $trail->push($class->name, route('classes.edit', $class->id));
    $trail->push('Edit', route('classes.edit', $class->id));
});

Breadcrumbs::for('attendance', function ($trail) {
    $trail->parent('home');
    $trail->push('Absensi Siswa', route('attendances.index'));
});

Breadcrumbs::for('attendance edit', function ($trail, $class) {
    $trail->parent('attendance');
    $trail->push('Kelas '.$class->name, route('attendances.edit'));
    $trail->push('Edit', route('attendances.edit'));
});

Breadcrumbs::for('class guardian', function ($trail) {
    $trail->parent('home');
    $trail->push('Wali Kelas', route('classes.guardians.index'));
});

Breadcrumbs::for('class guardian edit', function ($trail, $class) {
    $trail->parent('class guardian');
    $trail->push($class->name, route('classes.guardians.edit', $class->id));
    $trail->push('Edit', route('classes.guardians.edit', $class->id));
});

Breadcrumbs::for('class student', function ($trail) {
    $trail->parent('home');
    $trail->push('Rombongan Kelas', route('classes.students.index'));
});

Breadcrumbs::for('class student show', function ($trail, $class) {
    $trail->parent('class student');
    $trail->push($class->name, route('classes.students.show', $class->id));
    $trail->push('Detail', route('classes.students.show', $class->id));
});

Breadcrumbs::for('class student add', function ($trail, $class) {
    $trail->parent('class student');
    $trail->push($class->name, route('classes.students.show', $class->id));
    $trail->push('Tambah', route('classes.students.add', $class->id));
});

Breadcrumbs::for('curriculum', function ($trail) {
    $trail->parent('home');
    $trail->push('Kurikulum', route('curriculums.index'));
});

Breadcrumbs::for('curriculum create', function ($trail) {
    $trail->parent('curriculum');
    $trail->push('Tambah', route('curriculums.create'));
});

Breadcrumbs::for('curriculum edit', function ($trail, $curriculum) {
    $trail->parent('curriculum');
    $trail->push($curriculum->name, route('curriculums.edit', $curriculum->id));
    $trail->push('Edit', route('curriculums.edit', $curriculum->id));
});

Breadcrumbs::for('curriculum detail', function ($trail, $curriculum) {
    $trail->parent('curriculum');
    $trail->push($curriculum->name, route('curriculums.edit', $curriculum->id));
    $trail->push('Detail', route('curriculums.show', $curriculum->id));
});

Breadcrumbs::for('curriculum detail add', function ($trail, $curriculum, $grade) {
    $trail->parent('curriculum');
    $trail->push($curriculum->name, route('curriculums.edit', $curriculum->id));
    $trail->push('Tambah', route('curriculums.details.add', ['id' => $curriculum->id, 'grade_id' => $grade->id]));
});

Breadcrumbs::for('employee', function ($trail) {
    $trail->parent('home');
    $trail->push('Pegawai', route('employees.index'));
});

Breadcrumbs::for('employee create', function ($trail) {
    $trail->parent('employee');
    $trail->push('Tambah', route('employees.create'));
});

Breadcrumbs::for('employee edit', function ($trail, $employee) {
    $trail->parent('employee');
    $trail->push($employee->name, route('employees.edit', $employee->id));
    $trail->push('Edit', route('employees.edit', $employee->id));
});

Breadcrumbs::for('lesson_category', function ($trail) {
    $trail->parent('home');
    $trail->push('Kelompok Mata Pelajaran', route('lesson_categories.index'));
});

Breadcrumbs::for('lesson_category create', function ($trail) {
    $trail->parent('lesson_category');
    $trail->push('Tambah', route('lesson_categories.create'));
});

Breadcrumbs::for('lesson_category edit', function ($trail, $lesson_category) {
    $trail->parent('lesson_category');
    $trail->push($lesson_category->name, route('lesson_categories.edit', $lesson_category->id));
    $trail->push('Edit', route('lesson_categories.edit', $lesson_category->id));
});

Breadcrumbs::for('lesson', function ($trail) {
    $trail->parent('home');
    $trail->push('Mata Pelajaran', route('lessons.index'));
});

Breadcrumbs::for('lesson create', function ($trail) {
    $trail->parent('lesson');
    $trail->push('Tambah', route('lessons.create'));
});

Breadcrumbs::for('lesson edit', function ($trail, $lesson) {
    $trail->parent('lesson');
    $trail->push($lesson->name, route('lessons.edit', $lesson->id));
    $trail->push('Edit', route('lessons.edit', $lesson->id));
});

Breadcrumbs::for('school_time', function ($trail) {
    $trail->parent('home');
    $trail->push('Jam Pelajaran', route('school_times.index'));
});

Breadcrumbs::for('school_time create', function ($trail) {
    $trail->parent('school_time');
    $trail->push('Tambah', route('school_times.create'));
});

Breadcrumbs::for('school_time edit', function ($trail, $school_time) {
    $trail->parent('school_time');
    $trail->push($school_time->id, route('school_times.edit', $school_time->id));
    $trail->push('Edit', route('school_times.edit', $school_time->id));
});

Breadcrumbs::for('student', function ($trail) {
    $trail->parent('home');
    $trail->push('Siswa', route('students.index'));
});

Breadcrumbs::for('student create', function ($trail) {
    $trail->parent('student');
    $trail->push('Tambah', route('students.create'));
});

Breadcrumbs::for('student edit', function ($trail, $student) {
    $trail->parent('student');
    $trail->push($student->name, route('students.edit', $student->id));
    $trail->push('Edit', route('students.edit', $student->id));
});

Breadcrumbs::for('teacher', function ($trail) {
    $trail->parent('home');
    $trail->push('Guru', route('teachers.index'));
});

Breadcrumbs::for('teacher create', function ($trail) {
    $trail->parent('teacher');
    $trail->push('Tambah', route('teachers.create'));
});

Breadcrumbs::for('teacher edit', function ($trail, $teacher) {
    $trail->parent('teacher');
    $trail->push($teacher->name, route('teachers.edit', $teacher->id));
    $trail->push('Edit', route('teachers.edit', $teacher->id));
});

Breadcrumbs::for('user', function ($trail) {
    $trail->parent('home');
    $trail->push('User', route('users.index'));
});

Breadcrumbs::for('user create', function ($trail) {
    $trail->parent('user');
    $trail->push('Tambah', route('users.create'));
});

Breadcrumbs::for('user edit', function ($trail, $user) {
    $trail->parent('user');
    $trail->push($user->name, route('users.edit', $user->id));
    $trail->push('Edit', route('users.edit', $user->id));
});

Breadcrumbs::for('calendar', function ($trail) {
    $trail->parent('home');
    $trail->push('Kalender Akademik', route('calendar.index'));
});

Breadcrumbs::for('calendar setting', function ($trail) {
    $trail->parent('calendar');
    $trail->push('Setting', route('calendar.edit'));
});

Breadcrumbs::for('event', function ($trail) {
    $trail->parent('calendar');
    $trail->push('Agenda', route('events.index'));
});

Breadcrumbs::for('event create', function ($trail) {
    $trail->parent('event');
    $trail->push('Tambah', route('events.create'));
});

Breadcrumbs::for('event edit', function ($trail, $event) {
    $trail->parent('event');
    $trail->push($event->name, route('events.edit', $event->id));
    $trail->push('Edit', route('events.edit', $event->id));
});

Breadcrumbs::for('lesson_schedule', function ($trail) {
    $trail->parent('home');
    $trail->push('Jadwal Pelajaran', route('lesson_schedules.index'));
});

Breadcrumbs::for('lesson_schedule show', function ($trail, $class) {
    $trail->parent('lesson_schedule');
    $trail->push("Kelas ".$class->name, route('lesson_schedules.show', $class->id));
    $trail->push('Detail', route('lesson_schedules.show', $class->id));
});

Breadcrumbs::for('lesson_schedule add', function ($trail, $class) {
    $trail->parent('lesson_schedule');
    $trail->push("Kelas ".$class->name, route('lesson_schedules.show', $class->id));
    $trail->push('Tambah', route('lesson_schedules.add', $class->id));
});

Breadcrumbs::for('lesson_schedule edit', function ($trail, $lesson_schedule) {
    $trail->parent('lesson_schedule');
    $trail->push("Edit", route('lesson_schedules.edit', $lesson_schedule->id));
});

Breadcrumbs::for('lesson_journal', function ($trail) {
    $trail->parent('home');
    $trail->push('Jurnal Pelajaran', route('lesson_journals.index'));
});

Breadcrumbs::for('lesson_journal edit', function ($trail, $date) {
    $trail->parent('lesson_journal');
    $trail->push($date, route('lesson_journals.edit', $date));
    $trail->push('Edit', route('lesson_journals.edit', $date));
});

Breadcrumbs::for('student_permit', function ($trail) {
    $trail->parent('attendance');
    $trail->push('Ijin Siswa', route('student_permits.index'));
});

Breadcrumbs::for('student_permit create', function ($trail) {
    $trail->parent('student_permit');
    $trail->push('Buat Ijin Siswa', route('student_permits.create'));
});

Breadcrumbs::for('student_permit edit', function ($trail, $permit) {
    $trail->parent('student_permit');
    $trail->push($permit->id, route('student_permits.edit', $permit->id));
    $trail->push('Edit', route('student_permits.edit', $permit->id));
});

Breadcrumbs::for('report student', function ($trail) {
    $trail->parent('home');
    $trail->push('Report Siswa', route('reports.students'));
});

Breadcrumbs::for('setting general', function ($trail) {
    $trail->parent('home');
    $trail->push('Setting', route('settings.index'));
    $trail->push('General', route('settings.index'));
});